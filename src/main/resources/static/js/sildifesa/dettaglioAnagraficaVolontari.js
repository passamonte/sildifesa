$(document).ready(function () {
    if (anag.caDocumentoIdentita != null && anag.caDocumentoIdentita.caTipoDocumentoIdentita != null) {
        var trovato = (tipoDocumenti.find(function (value, index) {
            return value.id === anag.caDocumentoIdentita.caTipoDocumentoIdentita.id;
        })).nome;
        $('#tipoDoc').append(trovato);
    } else {
        $('#tipoDoc').append("Nessun valore inserito");
    }
    if (anag.caAnagraficaTitolostudios != null && anag.caAnagraficaTitolostudios.length > 0) {
        if (anag.caAnagraficaTitolostudios.length > 1) {
            $('#titoloStudio').append("Laurea");
        } else {
            livelloStudio.forEach(function (value, index) {
                if (anag.caAnagraficaTitolostudios[0].suTitoloStudio.livello === value.livello) {
                    $('#titoloStudio').append(value.nome);
                }
            });
        }
    }
});


