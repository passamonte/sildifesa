
function ajaxComuneSedeLegale(sezione) {
    $('#comuneSedeLegaleId').val("");
    var comune = $('#comuneSedeLegaleInput').val();
    var digit = comune.length;
    if (digit === 0) {
        $("#suggesstion-box").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxComuni + "?comune=" + comune,
        success: function (data) {
            $("#suggesstion-box").prop("hidden", false);
            var ajaxCall = "";
            listaCodiciComune = [];
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(comune.toLocaleLowerCase());
                    ajaxCall += "<div id='" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaScelta(this.id,\"" + sezione + "\")'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + ' (' + data[i].provincia + ')' + "</div>"
                    listaCodiciComune.push({id: data[i].id, codice: data[i].codiceFisco});
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#suggesstion-box").html(ajaxCall);
        }
    });
}

function assegnaScelta(id, sezione) {
    if (id === "no") {
        return;
    }
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#comuneSedeLegaleInput').val(stringa);
    $('#' + id + '').val(id);
    $('#suggesstion-box').html("");
    $("#suggesstion-box").prop("hidden", true);
    $('#comuneSedeLegaleId').val(id);
    if (sezione !== 'comuneSedeLegale') {
        return;
    }
    var cod = listaCodiciComune.find(function (oggetto) {
        return (oggetto.id + "") === id ? oggetto.codice : undefined;
    });

    $('#comuneSedeLegaleCodice').val(cod.codice);
}

function chiudiComune() {
    $("#suggesstion-box").prop("hidden", true);
}



$(function () {
    $('#file').change(function () {
        $('#hiddenFile').val("");
        $('#pFile').html("");
    });
});
