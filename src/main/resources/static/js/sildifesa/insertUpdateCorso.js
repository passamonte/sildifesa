$(function () {
    $('#file').change(function () {
        $('#hiddenFile').val("");
        $('#pFile').html("");
    });
});

function ajaxComune() {
    $('#comuneId').val("");
    var comune = $('#comune').val();
    var digit = comune.length;
    if (digit === 0) {
        $("#ajaxComune").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxComuni + "?comune=" + comune,
        success: function (data) {
            $("#ajaxComune").prop("hidden", false);
            var ajaxCall = "";
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(comune.toLocaleLowerCase());
                    ajaxCall += "<div id='dom" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaScelta(this.id)'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + ' (' + data[i].provincia + ')' + "</div>"
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#ajaxComune").html(ajaxCall);
        }
    });
}

function assegnaScelta(id) {
    if (id === "no") {
        return;
    }
    var idValue = id.replace("dom", "");
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#comune').val(stringa);
    $('#' + id + '').val(idValue);
    $('#ajaxComune').html("");
    $("#ajaxComune").prop("hidden", true);
    $('#comuneId').val(idValue);


}

function chiudiComune() {
    $("#ajaxComune").prop("hidden", true);
}

function ajaxAzienda() {
    $('#aziendaId').val("");
    var azienda = $('#azienda').val();
    var digit = azienda.length;
    if (digit === 0) {
        $("#ajaxAzienda").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxAzienda + "?azienda=" + azienda,
        success: function (data) {
            $("#ajaxAzienda").prop("hidden", false);
            var ajaxCall = "";
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].ragioneSociale !== null && data[i].ragioneSociale !== undefined) {
                    var partenza = data[i].ragioneSociale.toLowerCase().indexOf(azienda.toLocaleLowerCase());
                    ajaxCall += "<div id='azi" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaSceltaAzienda(this.id)'>"
                        + data[i].ragioneSociale.substring(0, partenza) + "<b>" + data[i].ragioneSociale.substring(partenza, partenza + digit) + "</b>" + data[i].ragioneSociale.substring(partenza + digit) + "</div>"
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='noA' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#ajaxAzienda").html(ajaxCall);
        }
    });
}

function assegnaSceltaAzienda(id) {
    if (id === "noA") {
        return;
    }
    var idValue = id.replace("azi", "");
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#azienda').val(stringa);
    $('#' + id + '').val(idValue);
    $('#ajaxAzienda').html("");
    $("#ajaxAzienda").prop("hidden", true);
    $('#aziendaId').val(idValue);


}

function chiudiAzienda() {
    $("#ajaxAzienda").prop("hidden", true);
}
$(document).ready(function () {
    if ($('#livelloTitolo').val() !== "") {
        $('#livelloTitolo').change();
    }
});

function cancellaFile(){
    $('#hiddenFile').val(null);
    $('#hiddenFlag').val(false);
    $('#file').val(null);
    $('#pFile').html("");
}
