var index = 0;
var bottoneElimina = "<span data-container='body' data-toggle='popover' data-trigger='hover' data-placement='right' data-html='true'\n" +
    "      title='Elimina Elemento'> <a class='nounderline' id='bottoneEliminaTitolo'> <svg class='icon icon-danger icon-m'><use\n" +
    "        th:href='@{/svg/sprite.svg#it-delete}'></use> </svg> </a> </span>"
var livello = $("#livelloTitolo").val();
var listaCodiciComune;
$(function () {
    $("#livelloTitolo").change(function () {
        $('#aggiungiTitolo').replaceWith("<div class='bootstrap-select-wrapper'><label for='titoloStudios' class='labelChange'>Titolo Studio</label><select  title='titolo di studio' class='form-control change changeSelect' id='titoloStudios' name='caAnagraficaTitolostudios[0].suTitoloStudio.id' data-live-search='true' data-live-search-placeholder='Cerca Titolo Di Studio'>");
        livello = $(this).val();
        livello > 40 ? $('#bottoneAggiungiTitolo').prop('disabled', false) : $('#bottoneAggiungiTitolo').prop('disabled', true);

        if (livello < 50) {
            $("#fatherDivRowTitolo").each(function () {
                $(this).find(".cloned").remove();
                index = 0;
            })


        }
        if (livello >= 30) {
            $.ajax({
                type: 'GET',
                url: addressAjax,
                success: function (data) {
                    var slctSubcat = $('#titoloStudios'), option = new Option();
                    slctSubcat.empty();
                    var flag = false;
                    for (var i = 0; i < data.length; i++) {
                        flag = false;
                        var res = data[i].nome.substring(3, data[i].nome.length);
                        for (var j = 0; titoli != null && j < titoli.length && flag === false; j++) {
                            if (titoli[j].suTitoloStudio.id != null && titoli[j].suTitoloStudio.id.toString() === data[i].id.toString()) {
                                flag = true;
                            }
                        }
                        if (flag) {
                            option = option + "<option selected='selected' value='" + data[i].id + "'>" + res + "</option>";
                        } else {
                            option = option + "<option value='" + data[i].id + "'>" + res + "</option>";
                        }
                    }
                    $('#titoloStudios').append(option);
                    $('#titoloStudios').append("</select></div>")

                },
                error: function () {
                    alert("error");
                }
            });
        } else {
            $.ajax({
                type: 'GET',
                url: addressAjaxLower + "?livello=" + livello,
                success: function (data) {
                    var slctSubcat = $('#titoloStudios'), option = new Option();
                    slctSubcat.empty();
                    for (var i = 0; i < data.length; i++) {
                        var res = data[i].nome.substring(3, data[i].nome.length);
                        option = option + "<option value='" + data[i].id + "'>" + res + "</option>";
                    }
                    $('#titoloStudios').append(option);
                    $('#titoloStudios').append("</select></div>")

                },
                error: function () {
                    alert("error");
                }
            });
        }
        if (livello === "0") {

            $('#anntoTitolo').prop("disabled", true);
            $('#istituto').prop("disabled", true);
        } else {
            $('#anntoTitolo').prop("disabled", false);
            $('#istituto').prop("disabled", false);
        }
    });
});
$(function () {
    $("#bottoneEliminaTitolo").click(function () {
        if (index > 0) {
            $('div[id^="divRowTitolo"]:last').remove();
            index -= 1;
            if (index == 0)
                $('#bottoneEliminaTitolo').prop('disabled', true);
        } else {
            $('#bottoneEliminaTitolo').prop('disabled', true);
        }
    });
});
var $klon;
var $divLast;

function clonaRiga(indice) {
    indice > 0 ? $('#bottoneEliminaTitolo').prop('disabled', false) : '';
    var nuovoid = 'divRowTitolo' + indice;
    // Prende i div il quale id inizia per "..."
    var $div = $('#divRowTitolo');
    $divLast = $('div[id^="divRowTitolo"]:last');

    // Clona elemento (DIV in questo caso) e aggiunge un indice che si incrementa ogni volta che viene richiamata la funzione
    $klon = $div.clone().prop('id', 'divRowTitolo' + indice);

    $klon.addClass("cloned");
    $klon.find(".labelChange").each(function () {
        $(this).prop("for", $(this).prop("for") + indice);
    })
    $klon.find(".change").each(function () {
        $(this).prop("id", $(this).prop("id") + indice);

    })
    $klon.find(".changeSelect").prop("name", "caAnagraficaTitolostudios[" + indice + "].suTitoloStudio.id")
    $klon.find(".changeInputAnno").prop("name", "caAnagraficaTitolostudios[" + indice + "].annoConseguimento")
    $klon.find(".changeInputIstituto").prop("name", "caAnagraficaTitolostudios[" + indice + "].istitutoConseguimento")


    $(bottoneElimina).appendTo($klon);

    if (livello >= 60 && indice > 0) {
        $.ajax({
            type: 'GET',
            url: addressAjaxLaurea,
            success: function (data) {
                var option = "";
                $klon.find(".changeSelect").empty();
                var slctSubcat = $klon.find(".changeSelect"), option = new Option();
                slctSubcat.empty();
                for (var i = 0; i < data.length; i++) {
                    var res = data[i].nome.substring(3, data[i].nome.length);
                    option = option + "<option value='" + data[i].id + "'>" + res + "</option>";
                }
                $klon.find(".changeSelect").append(option);
                // $divLast.after($klon.appendTo("#fatherDivRowTitolo"));
            },
            error: function () {
                alert("error");
            }
        });

    }
    // Append del clone
    $divLast.after($klon.appendTo("#fatherDivRowTitolo"));
}

$(function () {
    $("#bottoneAggiungiTitolo").click(function () {
        index++;
        clonaRiga(index);

    });
});

function copiaDomicilio() {
    $('#capDomicilioLabel').addClass('active');
    $('#indirizzoDomicilioLabel').addClass('active');
    $('#comuneDomLabel').addClass('active');
    $('#indirizzoDomicilio').val($('#indirizzoRes').val());
    $('#capDomicilio').val($('#capResidenza').val());
    $('#comuneDom').val($('#comuneRes').val());
    $('#comuneDomicilioId').val($('#comuneResidenzaId').val());

}

function nonValido(nome, cognome, sesso, luogo, data) {
    return nome === null || nome === undefined || nome === "" || cognome === null || cognome === undefined || cognome === "" ||
        sesso === null || sesso === undefined || sesso === "" || luogo === null || luogo === undefined || luogo === "" ||
        data === null || data === undefined || data === "";
}

function calcolaCodiceFiscale() {
    var sesso = $('#sesso').val();
    var data = $('#date1').val();
    data = data.match(/^\s*(\d+).(\d+).(\d+)/);
    var luogo = $('#comuneNascitaCodice').val();
    var nome = $('#nome').val();
    var cognome = $('#cognome').val();
    var erroreCod;
    if (nonValido(nome, cognome, sesso, luogo, data)) {
        erroreCod = "Compilare Nome, Cognome, Sesso, Luogo e Data di nascita per calcolare il Codice Fiscale";
        $('#errore').html(erroreCod);
        $("#erroreDiv").prop("hidden", false);
        return;
    }

    var codicefiscale = CFisc.calcola_codice(
        nome,
        cognome,
        sesso,
        data[1], data[2], data[3],
        luogo
    );
    $('#CF').val(codicefiscale);
    $("#erroreDiv").prop("hidden", true);
    $('#errore').html("");
}

function ajaxComune(sezione) {
    $('#comuneNascitaId').val("");
    var comune = $('#comuneNascitaInput').val();
    var digit = comune.length;
    if (digit === 0) {
        $("#suggesstion-box").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxComuni + "?comune=" + comune,
        success: function (data) {
            $("#suggesstion-box").prop("hidden", false);
            var ajaxCall = "";
            listaCodiciComune = [];
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(comune.toLocaleLowerCase());
                    ajaxCall += "<div id='" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaScelta(this.id,\"" + sezione + "\")'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + ' (' + data[i].provincia + ')' + "</div>"
                    listaCodiciComune.push({id: data[i].id, codice: data[i].codiceFisco});
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#suggesstion-box").html(ajaxCall);
        }
    });
}

function assegnaScelta(id, sezione) {
    if (id === "no") {
        return;
    }
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#comuneNascitaInput').val(stringa);
    $('#' + id + '').val(id);
    $('#suggesstion-box').html("");
    $("#suggesstion-box").prop("hidden", true);
    $('#comuneNascitaId').val(id);
    if (sezione !== 'nascita') {
        return;
    }
    var cod = listaCodiciComune.find(function (oggetto) {
        return (oggetto.id + "") === id ? oggetto.codice : undefined;
    });

    $('#comuneNascitaCodice').val(cod.codice);
}

function chiudi() {
    $("#suggesstion-box").prop("hidden", true);
}

function ajaxComuneResidenza() {
    $('#comuneResidenzaId').val("");
    var comune = $('#comuneRes').val();
    var digit = comune.length;
    if (digit === 0) {
        $("#ajaxResidenza").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxComuni + "?comune=" + comune,
        success: function (data) {
            $("#ajaxResidenza").prop("hidden", false);
            var ajaxCall = "";
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(comune.toLocaleLowerCase());
                    ajaxCall += "<div id='res" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaSceltaResidenza(this.id)'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + ' (' + data[i].provincia + ')' + "</div>"
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#ajaxResidenza").html(ajaxCall);
        }
    });
}

function assegnaSceltaResidenza(id) {
    if (id === "no") {
        return;
    }
    var idValue = id.replace("res", "");
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#comuneRes').val(stringa);
    $('#' + id + '').val(idValue);
    $('#ajaxResidenza').html("");
    $("#ajaxResidenza").prop("hidden", true);
    $('#comuneResidenzaId').val(idValue);


}

function chiudiResidenza() {
    $("#ajaxResidenza").prop("hidden", true);
}

function ajaxComuneDomicilio() {
    $('#comuneDomicilioId').val("");
    var comune = $('#comuneDom').val();
    var digit = comune.length;
    if (digit === 0) {
        $("#ajaxDomicilio").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxComuni + "?comune=" + comune,
        success: function (data) {
            $("#ajaxDomicilio").prop("hidden", false);
            var ajaxCall = "";
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(comune.toLocaleLowerCase());
                    ajaxCall += "<div id='dom" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaSceltaDomicilio(this.id)'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + ' (' + data[i].provincia + ')' + "</div>"
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#ajaxDomicilio").html(ajaxCall);
        }
    });
}

function assegnaSceltaDomicilio(id) {
    if (id === "no") {
        return;
    }
    var idValue = id.replace("dom", "");
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#comuneDom').val(stringa);
    $('#' + id + '').val(idValue);
    $('#ajaxDomicilio').html("");
    $("#ajaxDomicilio").prop("hidden", true);
    $('#comuneDomicilioId').val(idValue);


}

function chiudiDomicilio() {
    $("#ajaxDomicilio").prop("hidden", true);
}

$(function () {
    $("#forzaArmata").change(function () {
        var forzaArmata = $('#forzaArmata').val();
        $('#armaDiv').replaceWith("<div id='armaDiv' class='form-group bootstrap-select-wrapper'><label for='arma' class='labelChange'>Arma</label><select th:field='*{arma}' title='Seleziona una Forza Armata' class='form-control change changeSelect' id='arma' name='arma'>");
        $.ajax({
            type: 'GET',
            url: addressAjaxArma + "?forzaArmata=" + forzaArmata,
            success: function (data) {
                var slctSubcat = $('#arma'), option = new Option();
                slctSubcat.empty();
                option = option + "<option value='null'>Selezionare Arma</option>";
                for (var i = 0; i < data.length; i++) {
                    var res = data[i].nome;
                    option = option + '<option value="' + res + '">' + data[i].nome + '</option>';
                }
                $('#arma').append(option);
                $('#arma').append("</select></div>");
            },
            error: function () {
                alert("error");
            }
        });
    });
});

$(function () {
    $("#forzaArmata").change(function () {
        var forzaArmata = $('#forzaArmata').val();
        $('#gradoDiv').replaceWith("<div id='gradoDiv' class='form-group bootstrap-select-wrapper'><label for='gradoMilitare' class='labelChange'>Grado</label><select th:field='*{gradoMilitare}' title='Seleziona una Grado' class='form-control change changeSelect' id='gradoMilitare' name='gradoMilitare'>");
        $.ajax({
            type: 'GET',
            url: addressAjaxGrado + "?forzaArmata=" + forzaArmata,
            success: function (data) {
                var slctSubcat = $('#gradoMilitare'), option = new Option();
                slctSubcat.empty();
                option = option + "<option value='null'>Selezionare un grado</option>";
                for (var i = 0; i < data.length; i++) {
                    var res = data[i].nome;
                    option = option + "<option value='" + data[i].nome + "'>" + res + "</option>";
                }
                $('#gradoMilitare').append(option);
                $('#gradoMilitare').append("</select></div>")

            },
            error: function () {
                alert("error");
            }
        });
    });
});
$(document).ready(function () {
    if ($('#livelloTitolo').val() !== "") {
        $('#livelloTitolo').change();
    }
    if (anag.caAnagraficaTitolostudios != null && anag.caAnagraficaTitolostudios.length > 0) {
        if (anag.caAnagraficaTitolostudios.length > 1) {
            $('#livelloTitolo').val(60);
        } else {
            switch (anag.caAnagraficaTitolostudios[0].suTitoloStudio.livello) {
                case 0:
                    $('#livelloTitolo').val(0);
                    break;
                case 10:
                    $('#livelloTitolo').val(10);
                    break;
                case 20:
                    $('#livelloTitolo').val(20);
                    break;
                case 30:
                    $('#livelloTitolo').val(30);
            }
        }
        $('#livelloTitolo').change();
    }
    if ($('#forzaArmata').val() !== "") {
        var forzaArmata = $('#forzaArmata').val();
        $('#armaDiv').replaceWith("<div id='armaDiv' class='form-group bootstrap-select-wrapper'><label for='arma' class='labelChange'>Arma</label><select th:field='*{arma}' title='Seleziona una Forza Armata' class='form-control change changeSelect' id='arma' name='arma'>");
        $.ajax({
            type: 'GET',
            url: addressAjaxArma + "?forzaArmata=" + forzaArmata,
            success: function (data) {
                var slctSubcat = $('#arma'), option = new Option();
                slctSubcat.empty();
                option = option + "<option value='null'>Selezionare Arma</option>";
                for (var i = 0; i < data.length; i++) {
                    var res = data[i].nome;
                    option = option + '<option value="' + res + '">' + data[i].nome + '</option>';
                }
                $('#arma').append(option);
                $('#arma').append("</select></div>");
                if (anag.arma != null) {
                    $('#arma').val(anag.arma);
                }
            }
        });
        var forzaArmata = $('#forzaArmata').val();
        $('#gradoDiv').replaceWith("<div id='gradoDiv' class='form-group bootstrap-select-wrapper'><label for='gradoMilitare' class='labelChange'>Grado</label><select th:field='*{gradoMilitare}' title='Seleziona una Grado' class='form-control change changeSelect' id='gradoMilitare' name='gradoMilitare'>");
        $.ajax({
            type: 'GET',
            url: addressAjaxGrado + "?forzaArmata=" + forzaArmata,
            success: function (data) {
                var slctSubcat = $('#gradoMilitare'), option = new Option();
                slctSubcat.empty();
                option = option + "<option value='null'>Selezionare un grado</option>";
                for (var i = 0; i < data.length; i++) {
                    var res = data[i].nome;
                    option = option + "<option value='" + data[i].nome + "'>" + res + "</option>";
                }
                $('#gradoMilitare').append(option);
                $('#gradoMilitare').append("</select></div>");
                $('#gradoMilitare').val(anag.gradoMilitare);
            }
        });
    }
    if(anag.caAnagraficaTitolostudios != null) {
        var titoliStudio = anag.caAnagraficaTitolostudios.filter(function (item) {
            if (item.suTitoloStudio.livello !== 30) {
                return item;
            }
        });
    }
        if (livello >= 60) {
            $.ajax({
                type: 'GET',
                url: addressAjaxLaurea,
                success: function (data) {
                    for (var k = 0; k < titoliStudio.length; k++) {
                        index++;
                        var indice = index;
                        indice > 0 ? $('#bottoneEliminaTitolo').prop('disabled', false) : '';
                        var nuovoid = 'divRowTitolo' + indice;
                        // Prende i div il quale id inizia per "..."
                        var $div = $('#divRowTitolo');
                        $divLast = $('div[id^="divRowTitolo"]:last');

                        // Clona elemento (DIV in questo caso) e aggiunge un indice che si incrementa ogni volta che viene richiamata la funzione
                        $klon = $div.clone().prop('id', 'divRowTitolo' + indice);

                        $klon.addClass("cloned");
                        $klon.find(".labelChange").each(function () {
                            $(this).prop("for", $(this).prop("for") + indice);
                        });
                        $klon.find(".change").each(function () {
                            $(this).prop("id", $(this).prop("id") + indice);
                        });
                        var t = k+1;
                        $klon.find(".changeSelect").prop("name", "caAnagraficaTitolostudios[" + indice + "].suTitoloStudio.id");
                        $klon.find(".changeInputAnno").prop("name", "caAnagraficaTitolostudios[" + indice + "].annoConseguimento");
                        $klon.find(".changeInputIstituto").prop("name", "caAnagraficaTitolostudios[" + indice + "].istitutoConseguimento");
                        $klon.find(".changeHidden").prop("name", "caAnagraficaTitolostudios[" + indice + "].id");
                        $(bottoneElimina).appendTo($klon);
                        var option = "";
                        $klon.find(".changeSelect").empty();
                        var slctSubcat = $klon.find(".changeSelect"), option = new Option();
                        slctSubcat.empty();
                        for (var i = 0; i < data.length; i++) {
                            var res = data[i].nome.substring(3, data[i].nome.length);
                            option = option + "<option value='" + data[i].id + "'>" + res + "</option>";
                        }
                        $klon.find(".changeSelect").append(option);

                        $divLast.after($klon.appendTo("#fatherDivRowTitolo"));
                        $('#istituto' + t).val(titoliStudio[k].istitutoConseguimento);
                        $('#annoTitolo' + t).val(titoliStudio[k].annoConseguimento);
                        $('#titoloStudios' + t).val(titoliStudio[k].suTitoloStudio.id);

                        $('#studio'+t).val(titoliStudio[k].id);
                    }
                    if (liv === true){
                        $('#divRowTitolo1').remove();
                    }
                },
                error: function () {
                    alert("error");
                }
            });
    }
});


$(function () {
    $('#file').change(function () {
        $('#hiddenFile').val("");
        $('#pFile').html("");
    });
});


function ajaxIncarico() {
    $('#incaricoId').val("");
    var forzaArmata = $('#forzaArmata').val();
    if (forzaArmata == null || forzaArmata === "") {
        var htmlError = "<div id='no2' class='mouse-over-ajax graph graph-hover p-2' >"
            + "<b>Seleziona prima una forza armata</b></div>"
        $("#ajaxIncarico").html(htmlError);
        $("#ajaxIncarico").prop("hidden", false);
        return;
    }
    var incarico = $('#incaricoInput').val();

    var digit = incarico.length;
    if (digit === 0) {
        $("#ajaxIncarico").prop("hidden", true);
        return;
    }
    if (digit < 2) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: addressAjaxIncarico + "?incarico=" + incarico + "&forzaArmata=" + forzaArmata,
        success: function (data) {
            $("#ajaxIncarico").prop("hidden", false);
            var ajaxCall = "";
            for (var i = 0; i < Math.min(data.length, 7); i++) {
                if (data[i].nome !== null && data[i].nome !== undefined) {
                    var partenza = data[i].nome.toLowerCase().indexOf(incarico.toLocaleLowerCase());
                    ajaxCall += "<div id='inc" + data[i].id + "' class='mouse-over-ajax graph graph-hover p-2' onmousedown='assegnaSceltaIncarico(this.id)'>"
                        + data[i].nome.substring(0, partenza) + "<b>" + data[i].nome.substring(partenza, partenza + digit) + "</b>" + data[i].nome.substring(partenza + digit) + "</div>"
                }
            }
            if (data.length < 1) {
                ajaxCall += "<div id='no2' class='mouse-over-ajax graph graph-hover p-2' >"
                    + "<b>Nessun risultato trovato</b></div>"
            }
            $("#ajaxIncarico").html(ajaxCall);
        }
    });
}

function assegnaSceltaIncarico(id) {
    if (id === "no") {
        return;
    }
    var idValue = id.replace("inc", "");
    var stringa = document.getElementById(id).innerHTML;
    stringa = stringa.replace("<b>", "");
    stringa = stringa.replace("</b>", "");
    $('#incaricoInput').val(stringa);
    $('#' + id + '').val(idValue);
    $('#ajaxIncarico').html("");
    $("#ajaxIncarico").prop("hidden", true);
    $('#incaricoId').val(idValue);


}

function chiudiIncarico() {
    $("#ajaxIncarico").prop("hidden", true);
}
