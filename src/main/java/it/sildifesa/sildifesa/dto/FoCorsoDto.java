package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.fo.FoCorso;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class FoCorsoDto {

	private Long id;

	@NotNull(message = "Cap obbligatorio")
	private String capSede;

	private String compenso;

	private String competenzeRichieste;

	private Integer creditiFormativi;

	@NotNull(message = "Data Inizio obbligatoria")
	private Date dataInizio;

	private String descrizione;

	@NotNull(message = "Durata obbligatoria")
	private String durata;

	private CaAnagraficaAziendaDto caAnagraficaAzienda;

	private String fascia;

	@NotNull(message = "Indirizzo obbligatorio")
	private String indirizzoSede;

	private Boolean interno;

	private String livelloContratto;

	@Valid
	private SuComuneDto localitaSedeCorso;

	@NotNull(message = "Numero Posti obbligatorio")
	private Integer numeroPosti;

	private Boolean perAutoimpiego;

	private Boolean perCooperativa;

	private Boolean perVolontari;

	private String riferimento;

	private String tipologiaContratto;

	private String tipoRapportoLavoro;

	@NotNull(message = "Tirocinio obbligatorio")
	private Boolean tirocinio;

	@NotNull(message = "Titolo obbligatorio")
	private String titolo;

	private Integer titoloStudioRichiesto;

	private Boolean attivo;

	private FoDocumentoCorsoDto foDocumentoCorso;

	@NotNull(message = "Data Fine obbligatoria")
	private Date dataFine;

	@NotNull(message = "Data Fine Candidatura obbligatoria")
	private Date dataInizioCandidatura;

	@NotNull(message = "Data Fine Candidatura obbligatoria")
	private Date dataFineCandidatura;

	private Integer annoFinanziario;

	private String sezioneTerritorialeCoordinatrice;

	private String certificazioni;

	private String benefit;

	private String telefonoReferente;

	@Email(message = "Inserire email valida")
	private String emailReferente;

	private Set<FoCorsoAnagraficaDto> foCorsoAnagraficaList;

	public FoCorsoDto() {
	}

	public FoCorsoDto(Long id, String capSede, String compenso, String competenzeRichieste, Integer creditiFormativi, Date dataInizio, String descrizione, String durata, CaAnagraficaAziendaDto ente, String fascia, String indirizzoSede, Boolean interno, String livelloContratto, SuComuneDto localitaSedeCorso, Integer numeroPosti, Boolean perAutoimpiego, Boolean perCooperativa, Boolean perVolontari, String riferimento, String tipologiaContratto, String tipoRapportoLavoro, Boolean tirocinio, String titolo, Integer titoloStudioRichiesto, Boolean attivo, FoDocumentoCorsoDto foDocumentoCorso,
					  Date dataFine, Date dataInizioCandidatura, Date dataFineCandidatura, Integer annoFinanziario, String sezioneTerritorialeCoordinatrice, String certificazioni, String benefit, String telefonoReferente, String emailReferente, Set<FoCorsoAnagraficaDto> foCorsoAnagraficaList) {
		this.id = id;
		this.capSede = capSede;
		this.compenso = compenso;
		this.competenzeRichieste = competenzeRichieste;
		this.creditiFormativi = creditiFormativi;
		this.dataInizio = dataInizio;
		this.descrizione = descrizione;
		this.durata = durata;
		this.caAnagraficaAzienda = ente;
		this.fascia = fascia;
		this.indirizzoSede = indirizzoSede;
		this.interno = interno;
		this.livelloContratto = livelloContratto;
		this.localitaSedeCorso = localitaSedeCorso;
		this.numeroPosti = numeroPosti;
		this.perAutoimpiego = perAutoimpiego;
		this.perCooperativa = perCooperativa;
		this.perVolontari = perVolontari;
		this.riferimento = riferimento;
		this.tipologiaContratto = tipologiaContratto;
		this.tipoRapportoLavoro = tipoRapportoLavoro;
		this.tirocinio = tirocinio;
		this.titolo = titolo;
		this.titoloStudioRichiesto = titoloStudioRichiesto;
		this.attivo = attivo;
		this.foDocumentoCorso = foDocumentoCorso;
		this.dataFine = dataFine;
		this.dataInizioCandidatura = dataInizioCandidatura;
		this.dataFineCandidatura = dataFineCandidatura;
		this.annoFinanziario = annoFinanziario;
		this.sezioneTerritorialeCoordinatrice = sezioneTerritorialeCoordinatrice;
		this.certificazioni = certificazioni;
		this.benefit = benefit;
		this.telefonoReferente = telefonoReferente;
		this.emailReferente = emailReferente;
		this.foCorsoAnagraficaList = foCorsoAnagraficaList;
	}

	public FoCorsoDto(Long id, String capSede, String compenso, String competenzeRichieste, Integer creditiFormativi, Date dataInizio, String descrizione, String durata, CaAnagraficaAziendaDto ente, String fascia, String indirizzoSede, Boolean interno, String livelloContratto, SuComuneDto localitaSedeCorso, Integer numeroPosti, Boolean perAutoimpiego, Boolean perCooperativa, Boolean perVolontari, String riferimento, String tipologiaContratto, String tipoRapportoLavoro, Boolean tirocinio, String titolo, Integer titoloStudioRichiesto, Boolean attivo,
					  Date dataFine, Date dataInizioCandidatura, Date dataFineCandidatura, Integer annoFinanziario, String sezioneTerritorialeCoordinatrice, String certificazioni, String benefit, String telefonoReferente, String emailReferente) {
		this.id = id;
		this.capSede = capSede;
		this.compenso = compenso;
		this.competenzeRichieste = competenzeRichieste;
		this.creditiFormativi = creditiFormativi;
		this.dataInizio = dataInizio;
		this.descrizione = descrizione;
		this.durata = durata;
		this.caAnagraficaAzienda = ente;
		this.fascia = fascia;
		this.indirizzoSede = indirizzoSede;
		this.interno = interno;
		this.livelloContratto = livelloContratto;
		this.localitaSedeCorso = localitaSedeCorso;
		this.numeroPosti = numeroPosti;
		this.perAutoimpiego = perAutoimpiego;
		this.perCooperativa = perCooperativa;
		this.perVolontari = perVolontari;
		this.riferimento = riferimento;
		this.tipologiaContratto = tipologiaContratto;
		this.tipoRapportoLavoro = tipoRapportoLavoro;
		this.tirocinio = tirocinio;
		this.titolo = titolo;
		this.titoloStudioRichiesto = titoloStudioRichiesto;
		this.attivo = attivo;
		this.dataFine = dataFine;
		this.dataInizioCandidatura = dataInizioCandidatura;
		this.dataFineCandidatura = dataFineCandidatura;
		this.annoFinanziario = annoFinanziario;
		this.sezioneTerritorialeCoordinatrice = sezioneTerritorialeCoordinatrice;
		this.certificazioni = certificazioni;
		this.benefit = benefit;
		this.telefonoReferente = telefonoReferente;
		this.emailReferente = emailReferente;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCapSede() {
		return capSede;
	}

	public void setCapSede(String capSede) {
		this.capSede = capSede;
	}

	public String getCompenso() {
		return compenso;
	}

	public void setCompenso(String compenso) {
		this.compenso = compenso;
	}

	public String getCompetenzeRichieste() {
		return competenzeRichieste;
	}

	public void setCompetenzeRichieste(String competenzeRichieste) {
		this.competenzeRichieste = competenzeRichieste;
	}

	public Integer getCreditiFormativi() {
		return creditiFormativi;
	}

	public void setCreditiFormativi(Integer creditiFormativi) {
		this.creditiFormativi = creditiFormativi;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getDurata() {
		return durata;
	}

	public void setDurata(String durata) {
		this.durata = durata;
	}

	public CaAnagraficaAziendaDto getCaAnagraficaAzienda() {
		return caAnagraficaAzienda;
	}

	public void setCaAnagraficaAzienda(CaAnagraficaAziendaDto caAnagraficaAzienda) {
		this.caAnagraficaAzienda = caAnagraficaAzienda;
	}

	public String getFascia() {
		return fascia;
	}

	public void setFascia(String fascia) {
		this.fascia = fascia;
	}

	public String getIndirizzoSede() {
		return indirizzoSede;
	}

	public void setIndirizzoSede(String indirizzoSede) {
		this.indirizzoSede = indirizzoSede;
	}

	public Boolean getInterno() {
		return interno;
	}

	public void setInterno(Boolean interno) {
		this.interno = interno;
	}

	public String getLivelloContratto() {
		return livelloContratto;
	}

	public void setLivelloContratto(String livelloContratto) {
		this.livelloContratto = livelloContratto;
	}

	public SuComuneDto getLocalitaSedeCorso() {
		return localitaSedeCorso;
	}

	public void setLocalitaSedeCorso(SuComuneDto localitaSedeCorso) {
		this.localitaSedeCorso = localitaSedeCorso;
	}

	public Boolean getAttivo() {
		return attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public Integer getNumeroPosti() {
		return numeroPosti;
	}

	public void setNumeroPosti(Integer numeroPosti) {
		this.numeroPosti = numeroPosti;
	}

	public Boolean getPerAutoimpiego() {
		return perAutoimpiego;
	}

	public void setPerAutoimpiego(Boolean perAutoimpiego) {
		this.perAutoimpiego = perAutoimpiego;
	}

	public Boolean getPerCooperativa() {
		return perCooperativa;
	}

	public void setPerCooperativa(Boolean perCooperativa) {
		this.perCooperativa = perCooperativa;
	}

	public Boolean getPerVolontari() {
		return perVolontari;
	}

	public void setPerVolontari(Boolean perVolontari) {
		this.perVolontari = perVolontari;
	}

	public String getRiferimento() {
		return riferimento;
	}

	public void setRiferimento(String riferimento) {
		this.riferimento = riferimento;
	}

	public String getTipologiaContratto() {
		return tipologiaContratto;
	}

	public void setTipologiaContratto(String tipologiaContratto) {
		this.tipologiaContratto = tipologiaContratto;
	}

	public String getTipoRapportoLavoro() {
		return tipoRapportoLavoro;
	}

	public void setTipoRapportoLavoro(String tipoRapportoLavoro) {
		this.tipoRapportoLavoro = tipoRapportoLavoro;
	}

	public Boolean getTirocinio() {
		return tirocinio;
	}

	public void setTirocinio(Boolean tirocinio) {
		this.tirocinio = tirocinio;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public Integer getTitoloStudioRichiesto() {
		return titoloStudioRichiesto;
	}

	public void setTitoloStudioRichiesto(Integer titoloStudioRichiesto) {
		this.titoloStudioRichiesto = titoloStudioRichiesto;
	}

	public FoDocumentoCorsoDto getFoDocumentoCorso() {
		return foDocumentoCorso;
	}

	public void setFoDocumentoCorso(FoDocumentoCorsoDto foDocumentoCorso) {
		this.foDocumentoCorso = foDocumentoCorso;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Date getDataInizioCandidatura() {
		return dataInizioCandidatura;
	}

	public void setDataInizioCandidatura(Date dataInizioCandidatura) {
		this.dataInizioCandidatura = dataInizioCandidatura;
	}

	public Date getDataFineCandidatura() {
		return dataFineCandidatura;
	}

	public void setDataFineCandidatura(Date dataFineCandidatura) {
		this.dataFineCandidatura = dataFineCandidatura;
	}

	public Integer getAnnoFinanziario() {
		return annoFinanziario;
	}

	public void setAnnoFinanziario(Integer annoFinanziario) {
		this.annoFinanziario = annoFinanziario;
	}

	public String getSezioneTerritorialeCoordinatrice() {
		return sezioneTerritorialeCoordinatrice;
	}

	public void setSezioneTerritorialeCoordinatrice(String sezioneTerritorialeCoordinatrice) {
		this.sezioneTerritorialeCoordinatrice = sezioneTerritorialeCoordinatrice;
	}

	public String getCertificazioni() {
		return certificazioni;
	}

	public void setCertificazioni(String certificazioni) {
		this.certificazioni = certificazioni;
	}

	public String getBenefit() {
		return benefit;
	}

	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}

	public String getTelefonoReferente() {
		return telefonoReferente;
	}

	public void setTelefonoReferente(String telefonoReferente) {
		this.telefonoReferente = telefonoReferente;
	}

	public String getEmailReferente() {
		return emailReferente;
	}

	public void setEmailReferente(String emailReferente) {
		this.emailReferente = emailReferente;
	}

	public Set<FoCorsoAnagraficaDto> getFoCorsoAnagraficaList() {
		return foCorsoAnagraficaList;
	}

	public void setFoCorsoAnagraficaList(Set<FoCorsoAnagraficaDto> foCorsoAnagraficaList) {
		this.foCorsoAnagraficaList = foCorsoAnagraficaList;
	}

	public static FoCorsoDto fromModelToDto(FoCorso model){
		return new FoCorsoDto(model.getId(), model.getCapSede(), model.getCompenso(), model.getCompetenzeRichieste(), model.getCreditiFormativi(), model.getDataInizio(), model.getDescrizione(), model.getDurata(), CaAnagraficaAziendaDto.fromModelToDto(model.getCaAnagraficaAzienda()), model.getFascia(), model.getIndirizzoSede(), model.getInterno(), model.getLivelloContratto(), SuComuneDto.fromModelToDtoSenzaRelazioni(model.getLocalitaSedeCorso(),true), model.getNumeroPosti(), model.getPerAutoimpiego(), model.getPerCooperativa(), model.getPerVolontari(), model.getRiferimento(), model.getTipologiaContratto(), model.getTipoRapportoLavoro(), model.getTirocinio(), model.getTitolo(), model.getTitoloStudioRichiesto(), model.getAttivo(), FoDocumentoCorsoDto.fromModelToDto(model.getFoDocumentoCorso()), model.getDataFine(), model.getDataInizioCandidatura(), model.getDataFineCandidatura(), model.getAnnoFinanziario(), model.getSezioneTerritorialeCoordinatrice(), model.getCertificazioni(), model.getBenefit(), model.getTelefonoReferente(), model.getEmailReferente(), FoCorsoAnagraficaDto.fromSetModelToSetDto(model.getFoCorsoAnagraficaList()));
	}
	public static FoCorsoDto fromModelToDtoSenzaDocumento(FoCorso model){
		return new FoCorsoDto(model.getId(), model.getCapSede(), model.getCompenso(), model.getCompetenzeRichieste(), model.getCreditiFormativi(), model.getDataInizio(), model.getDescrizione(), model.getDurata(), CaAnagraficaAziendaDto.fromModelToDto(model.getCaAnagraficaAzienda()), model.getFascia(), model.getIndirizzoSede(), model.getInterno(), model.getLivelloContratto(), SuComuneDto.fromModelToDtoSenzaRelazioni(model.getLocalitaSedeCorso(),true), model.getNumeroPosti(), model.getPerAutoimpiego(), model.getPerCooperativa(), model.getPerVolontari(), model.getRiferimento(), model.getTipologiaContratto(), model.getTipoRapportoLavoro(), model.getTirocinio(), model.getTitolo(), model.getTitoloStudioRichiesto(), model.getAttivo(), model.getDataFine(), model.getDataInizioCandidatura(), model.getDataFineCandidatura(), model.getAnnoFinanziario(), model.getSezioneTerritorialeCoordinatrice(), model.getCertificazioni(), model.getBenefit(), model.getTelefonoReferente(), model.getEmailReferente());
	}

	public static FoCorso fromDtoToModel(FoCorsoDto dto){
		return new FoCorso(dto.getId(), dto.getCapSede(), dto.getCompenso(), dto.getCompetenzeRichieste(), dto.getCreditiFormativi(), dto.getDataInizio(), dto.getDescrizione(), dto.getDurata(), CaAnagraficaAziendaDto.fromDtoToModel(dto.getCaAnagraficaAzienda()), dto.getFascia(), dto.getIndirizzoSede(), dto.getInterno(), dto.getLivelloContratto(), SuComuneDto.fromDtoToModelSenzaRelazioni(dto.getLocalitaSedeCorso()), dto.getNumeroPosti(), dto.getPerAutoimpiego(), dto.getPerCooperativa(), dto.getPerVolontari(), dto.getRiferimento(), dto.getTipologiaContratto(), dto.getTipoRapportoLavoro(), dto.getTirocinio(), dto.getTitolo(), dto.getTitoloStudioRichiesto(), dto.getAttivo(), FoDocumentoCorsoDto.fromDtoToModel(dto.getFoDocumentoCorso()), dto.getDataFine(), dto.getDataInizioCandidatura(), dto.getDataFineCandidatura(), dto.getAnnoFinanziario(), dto.getSezioneTerritorialeCoordinatrice(), dto.getCertificazioni(), dto.getBenefit(), dto.getTelefonoReferente(), dto.getEmailReferente(), FoCorsoAnagraficaDto.fromSetDtoToSetModel(dto.getFoCorsoAnagraficaList()));
	}
}
