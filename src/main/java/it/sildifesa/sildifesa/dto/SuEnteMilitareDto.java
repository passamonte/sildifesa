package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.su.SuEnteMilitare;

import java.math.BigInteger;
import java.util.List;


public class SuEnteMilitareDto {

    private Long id;

    private String cap;


    private String codiceIntermediarioBCNL;


    private String codiceSige;


    private BigInteger comandoRFC;


    private String email;


    private String fa;


    private String fax;


    private String firma;


    private String indirizzo;


    private String indtel;


    private String intestazione;


    private BigInteger localita;


    private String nome;


    private byte sedeCentrale;


    private String telefono;


    private String tipoEnte;


    //bi-directional many-to-one association to CaAnagrafica
    private List<CaAnagraficaDto> caAnagraficas;

    public SuEnteMilitareDto() {
    }

    public SuEnteMilitareDto(Long id, String cap, String codiceIntermediarioBCNL, String codiceSige, BigInteger comandoRFC, String email, String fa, String fax, String firma, String indirizzo, String indtel, String intestazione, BigInteger localita, String nome, byte sedeCentrale, String telefono, String tipoEnte) {
        this.id = id;
        this.cap = cap;
        this.codiceIntermediarioBCNL = codiceIntermediarioBCNL;
        this.codiceSige = codiceSige;
        this.comandoRFC = comandoRFC;
        this.email = email;
        this.fa = fa;
        this.fax = fax;
        this.firma = firma;
        this.indirizzo = indirizzo;
        this.indtel = indtel;
        this.intestazione = intestazione;
        this.localita = localita;
        this.nome = nome;
        this.sedeCentrale = sedeCentrale;
        this.telefono = telefono;
        this.tipoEnte = tipoEnte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getCodiceIntermediarioBCNL() {
        return codiceIntermediarioBCNL;
    }

    public void setCodiceIntermediarioBCNL(String codiceIntermediarioBCNL) {
        this.codiceIntermediarioBCNL = codiceIntermediarioBCNL;
    }

    public String getCodiceSige() {
        return codiceSige;
    }

    public void setCodiceSige(String codiceSige) {
        this.codiceSige = codiceSige;
    }

    public BigInteger getComandoRFC() {
        return comandoRFC;
    }

    public void setComandoRFC(BigInteger comandoRFC) {
        this.comandoRFC = comandoRFC;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFa() {
        return fa;
    }

    public void setFa(String fa) {
        this.fa = fa;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getIndtel() {
        return indtel;
    }

    public void setIndtel(String indtel) {
        this.indtel = indtel;
    }

    public String getIntestazione() {
        return intestazione;
    }

    public void setIntestazione(String intestazione) {
        this.intestazione = intestazione;
    }

    public BigInteger getLocalita() {
        return localita;
    }

    public void setLocalita(BigInteger localita) {
        this.localita = localita;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public byte getSedeCentrale() {
        return sedeCentrale;
    }

    public void setSedeCentrale(byte sedeCentrale) {
        this.sedeCentrale = sedeCentrale;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoEnte() {
        return tipoEnte;
    }

    public void setTipoEnte(String tipoEnte) {
        this.tipoEnte = tipoEnte;
    }

    public List<CaAnagraficaDto> getCaAnagraficas() {
        return this.caAnagraficas;
    }

    public void setCaAnagraficas(List<CaAnagraficaDto> caAnagraficas) {
        this.caAnagraficas = caAnagraficas;
    }

    public CaAnagraficaDto addCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().add(caAnagrafica);
        caAnagrafica.setSuEntemilitare(this);

        return caAnagrafica;
    }

    public CaAnagraficaDto removeCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().remove(caAnagrafica);
        caAnagrafica.setSuEntemilitare(null);

        return caAnagrafica;
    }

    public static SuEnteMilitareDto fromModelToDtoSenzaRelazioni(SuEnteMilitare ente){
        if (ente == null){
            return null;
        }
        return new SuEnteMilitareDto(ente.getId(), ente.getCap(), ente.getCodiceIntermediarioBCNL(), ente.getCodiceSige(), ente.getComandoRFC(), ente.getEmail(), ente.getFa(), ente.getFax(), ente.getFirma(), ente.getIndirizzo(), ente.getIndtel(), ente.getIntestazione(), ente.getLocalita(), ente.getNome(), ente.getSedeCentrale(), ente.getTelefono(), ente.getTipoEnte());
    }

    public static SuEnteMilitare fromDtoToModelSenzaRelazioni(SuEnteMilitareDto ente){
        if (ente == null){
            return null;
        }
        return new SuEnteMilitare(ente.getId(), ente.getCap(), ente.getCodiceIntermediarioBCNL(), ente.getCodiceSige(), ente.getComandoRFC(), ente.getEmail(), ente.getFa(), ente.getFax(), ente.getFirma(), ente.getIndirizzo(), ente.getIndtel(), ente.getIntestazione(), ente.getLocalita(), ente.getNome(), ente.getSedeCentrale(), ente.getTelefono(), ente.getTipoEnte());
    }
}
