package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

public class CaAnagraficaAziendaDto {


    private Long id;

    @NotEmpty(message = "Ragione Sociale obbligatoria")
    private String ragioneSociale;

    @Length(max = 5)
    @NotNull(message = "Cap comune residenza obbligatorio")
    private String capSedeLegale;

    @NotEmpty(message = "P. IVA / C.F. Obbligatorio")
    private String partitaIva;

    @NotEmpty(message = " Attenzione il campo e-mail è obbligatorio")
    @Email(message = "Attenzione inserire una e-mail valida")
    private String email;

    @NotEmpty(message = " Attenzione il campo PEC è obbligatorio")
    @Email(message = "Attenzione inserire una PEC valida")
    private String emailPec;

    @NotEmpty(message = " Attenzione il campo Indirizzo Sede Legale è obbligatorio")
    private String indirizzoSedeLegale;

    @NotNull(message = "Numero di telefono obbligatorio")
    private String numeroTelefono;

    private String fax;

    private String descrizioneAttivita;

    private String formaGiuridica;

    private String sitoWeb;

    private String numeroRea;

    private String statoAttivita;

    private Boolean attivo;

    @NotEmpty(message = "Attenzione il Nome del Referente è obbligatorio")
    private String nomeReferente;

    private Date dataAttoDiCostituzione;

    @Valid
    private SuComuneDto comuneSedeLegale;

    @Valid
    private CaTipoAziendaDto tipologiaAzienda;

    private CaDocumentoAziendaDto caDocumentoAzienda;

    public CaAnagraficaAziendaDto() {
    }

    public CaAnagraficaAziendaDto(Long id, String ragioneSociale, String capSedeLegale, String partitaIva, String email, String emailPec, String indirizzoSedeLegale, String numeroTelefono, String fax, String descrizioneAttivita, String formaGiuridica, String sitoWeb, String numeroRea, String statoAttivita, Boolean attivo, String nomeReferente, Date dataAttoDiCostituzione, SuComuneDto comuneSedeLegale, CaTipoAziendaDto tipologiaAzienda, CaDocumentoAziendaDto caDocumentoAzienda) {
        this.id = id;
        this.ragioneSociale = ragioneSociale;
        this.capSedeLegale = capSedeLegale;
        this.partitaIva = partitaIva;
        this.email = email;
        this.emailPec = emailPec;
        this.indirizzoSedeLegale = indirizzoSedeLegale;
        this.numeroTelefono = numeroTelefono;
        this.fax = fax;
        this.descrizioneAttivita = descrizioneAttivita;
        this.formaGiuridica = formaGiuridica;
        this.sitoWeb = sitoWeb;
        this.numeroRea = numeroRea;
        this.statoAttivita = statoAttivita;
        this.attivo = attivo;
        this.nomeReferente = nomeReferente;
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
        this.comuneSedeLegale = comuneSedeLegale;
        this.tipologiaAzienda = tipologiaAzienda;
        this.caDocumentoAzienda = caDocumentoAzienda;
    }

    public CaAnagraficaAziendaDto(Long id, String ragioneSociale, String capSedeLegale, String partitaIva, String email, String emailPec, String indirizzoSedeLegale, String numeroTelefono, String fax, String descrizioneAttivita, String formaGiuridica, String sitoWeb, String numeroRea, String statoAttivita, Boolean attivo, String nomeReferente, Date dataAttoDiCostituzione, SuComuneDto comuneSedeLegale) {
        this.id = id;
        this.ragioneSociale = ragioneSociale;
        this.capSedeLegale = capSedeLegale;
        this.partitaIva = partitaIva;
        this.email = email;
        this.emailPec = emailPec;
        this.indirizzoSedeLegale = indirizzoSedeLegale;
        this.numeroTelefono = numeroTelefono;
        this.fax = fax;
        this.descrizioneAttivita = descrizioneAttivita;
        this.formaGiuridica = formaGiuridica;
        this.sitoWeb = sitoWeb;
        this.numeroRea = numeroRea;
        this.statoAttivita = statoAttivita;
        this.attivo = attivo;
        this.nomeReferente = nomeReferente;
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
        this.comuneSedeLegale = comuneSedeLegale;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getCapSedeLegale() {
        return capSedeLegale;
    }

    public void setCapSedeLegale(String capSedeLegale) {
        this.capSedeLegale = capSedeLegale;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailPec() {
        return emailPec;
    }

    public void setEmailPec(String emailPec) {
        this.emailPec = emailPec;
    }

    public String getIndirizzoSedeLegale() {
        return indirizzoSedeLegale;
    }

    public void setIndirizzoSedeLegale(String indirizzoSedeLegale) {
        this.indirizzoSedeLegale = indirizzoSedeLegale;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDescrizioneAttivita() {
        return descrizioneAttivita;
    }

    public void setDescrizioneAttivita(String descrizioneAttivita) {
        this.descrizioneAttivita = descrizioneAttivita;
    }

    public String getFormaGiuridica() {
        return formaGiuridica;
    }

    public void setFormaGiuridica(String formaGiuridica) {
        this.formaGiuridica = formaGiuridica;
    }

    public String getSitoWeb() {
        return sitoWeb;
    }

    public void setSitoWeb(String sitoWeb) {
        this.sitoWeb = sitoWeb;
    }

    public String getNumeroRea() {
        return numeroRea;
    }

    public void setNumeroRea(String numeroRea) {
        this.numeroRea = numeroRea;
    }

    public String getStatoAttivita() {
        return statoAttivita;
    }

    public void setStatoAttivita(String statoAttivita) {
        this.statoAttivita = statoAttivita;
    }

    public String getNomeReferente() {
        return nomeReferente;
    }

    public void setNomeReferente(String nomeReferente) {
        this.nomeReferente = nomeReferente;
    }

    public Date getDataAttoDiCostituzione() {
        return dataAttoDiCostituzione;
    }

    public void setDataAttoDiCostituzione(Date dataAttoDiCostituzione) {
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
    }

    public SuComuneDto getComuneSedeLegale() {
        return comuneSedeLegale;
    }

    public void setComuneSedeLegale(SuComuneDto comuneSedeLegale) {
        this.comuneSedeLegale = comuneSedeLegale;
    }

    public CaTipoAziendaDto getTipologiaAzienda() {
        return tipologiaAzienda;
    }

    public void setTipologiaAzienda(CaTipoAziendaDto tipologiaAzienda) {
        this.tipologiaAzienda = tipologiaAzienda;
    }

    public CaDocumentoAziendaDto getCaDocumentoAzienda() {
        return caDocumentoAzienda;
    }

    public void setCaDocumentoAzienda(CaDocumentoAziendaDto caDocumentoAzienda) {
        this.caDocumentoAzienda = caDocumentoAzienda;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public static CaAnagraficaAziendaDto fromModelToDtoConRel(CaAnagraficaAzienda model) {
        return new CaAnagraficaAziendaDto(model.getId(), model.getRagioneSociale(), model.getCapSedeLegale(), model.getPartitaIva(), model.getEmail(), model.getEmailPec(), model.getIndirizzoSedeLegale(), model.getNumeroTelefono(), model.getFax(), model.getDescrizioneAttivita(), model.getFormaGiuridica(), model.getSitoWeb(), model.getNumeroRea(), model.getStatoAttivita(), model.getAttivo(), model.getNomeReferente(), model.getDataAttoDiCostituzione(), SuComuneDto.fromModelToDtoSenzaRelazioni(model.getComuneSedeLegale(), false), CaTipoAziendaDto.fromModelToDto(model.getTipologiaAzienda()), CaDocumentoAziendaDto.fromModelToDto(model.getCaDocumentoAzienda()));
    }

    public static CaAnagraficaAziendaDto fromModelToDto(CaAnagraficaAzienda model) {
        return Objects.isNull(model) ? null : new CaAnagraficaAziendaDto(model.getId(), model.getRagioneSociale(), model.getCapSedeLegale(), model.getPartitaIva(), model.getEmail(), model.getEmailPec(), model.getIndirizzoSedeLegale(), model.getNumeroTelefono(), model.getFax(), model.getDescrizioneAttivita(), model.getFormaGiuridica(), model.getSitoWeb(), model.getNumeroRea(), model.getStatoAttivita(), model.getAttivo(), model.getNomeReferente(), model.getDataAttoDiCostituzione(), SuComuneDto.fromModelToDtoSenzaRelazioni(model.getComuneSedeLegale(), true));
    }

    public static CaAnagraficaAzienda fromDtoToModel(CaAnagraficaAziendaDto dto) {
        return new CaAnagraficaAzienda(dto.getId(), dto.getRagioneSociale(), dto.getCapSedeLegale(), dto.getPartitaIva(), dto.getEmail(), dto.getEmailPec(), dto.getIndirizzoSedeLegale(), dto.getNumeroTelefono(), dto.getFax(), dto.getDescrizioneAttivita(), dto.getFormaGiuridica(), dto.getSitoWeb(), dto.getNumeroRea(), dto.getStatoAttivita(), dto.getAttivo(), dto.getNomeReferente(), dto.getDataAttoDiCostituzione());
    }
}
