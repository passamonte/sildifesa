package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.lst.LstTipoContratto;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class LstTipoContrattoDto {


    private Long id;

    private String nome;

    private Set<LaOffertaDto> laOfferta;


    public LstTipoContrattoDto() {
    }

    public LstTipoContrattoDto(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Set<LaOffertaDto> getLaOfferta() {
        return laOfferta;
    }

    public void setLaOfferta(Set<LaOffertaDto> laOfferta) {
        this.laOfferta = laOfferta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public static LstTipoContrattoDto fromModelToDto(LstTipoContratto model){
        return Objects.isNull(model)?null:new LstTipoContrattoDto(model.getId(), model.getNome());
    }

    public static LstTipoContratto fromDtoToModel(LstTipoContrattoDto model){
        return Objects.isNull(model)?null:new LstTipoContratto(model.getId(), model.getNome());
    }

    public static List<LstTipoContrattoDto> fromSetModelToSetDto(List<LstTipoContratto> models){
        return Objects.isNull(models)?null:models.stream().map(LstTipoContrattoDto::fromModelToDto).collect(Collectors.toList());
    }

    public static Set<LstTipoContrattoDto> fromSetSModelToSetSDto(Set<LstTipoContratto> models){
        return Objects.isNull(models)?null:models.stream().map(LstTipoContrattoDto::fromModelToDto).collect(Collectors.toSet());
    }

    public static Set<LstTipoContratto> fromSetSDtoToSetSModel(Set<LstTipoContrattoDto> dtos){
        return Objects.isNull(dtos)?null:dtos.stream().map(LstTipoContrattoDto::fromDtoToModel).collect(Collectors.toSet());
    }
}
