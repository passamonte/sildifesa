package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.rp.RpCodificaLivelloTitolo;

import javax.persistence.*;

public class RpCodificaLivelloTitoloDto {

    private Long id;

    private Integer livello;

    private String nome;

    public RpCodificaLivelloTitoloDto(Long id, Integer livello, String nome) {
        this.id = id;
        this.livello = livello;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLivello() {
        return livello;
    }

    public void setLivello(Integer livello) {
        this.livello = livello;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public static RpCodificaLivelloTitoloDto fromModelToDto(RpCodificaLivelloTitolo model){
        return new RpCodificaLivelloTitoloDto(model.getId(), model.getLivello(),model.getNome());
    }
}
