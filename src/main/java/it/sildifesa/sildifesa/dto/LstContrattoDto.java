package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.lst.LstContratto;

import java.io.Serializable;
import java.util.Objects;


public class LstContrattoDto {

	private Long id;

	private String nome;

	private String livello;

	public LstContrattoDto() {
	}

	public LstContrattoDto(Long id, String nome, String livello) {
		this.id = id;
		this.nome = nome;
		this.livello = livello;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLivello() {
		return livello;
	}

	public void setLivello(String livello) {
		this.livello = livello;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static LstContrattoDto fromModelToDto(LstContratto model){
		return Objects.isNull(model)?null:new LstContrattoDto(model.getId(), model.getNome(), model.getLivello());
	}

	public static LstContratto fromDtoToModel(LstContrattoDto dto){
		return Objects.isNull(dto)?null:new LstContratto(dto.getId(), dto.getNome(), dto.getLivello());
	}
}
