package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.ca.CaTipoDocumentoIdentita;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

public class CaTipoDocumentoIdentitaDto {

    @NotNull(message = "Tipo documento obbligatorio")
    private Long id;

    private String nome;

    private List<CaDocumentoIdentitaDto> caDocumentoIdentita;

    public CaTipoDocumentoIdentitaDto() {
    }

    public CaTipoDocumentoIdentitaDto(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<CaDocumentoIdentitaDto> getCaDocumentoIdentita() {
        return caDocumentoIdentita;
    }

    public void setCaDocumentoIdentita(List<CaDocumentoIdentitaDto> caDocumentoIdentita) {
        this.caDocumentoIdentita = caDocumentoIdentita;
    }

    public static CaTipoDocumentoIdentitaDto fromModelToDto(CaTipoDocumentoIdentita tdi){
        return (Objects.isNull(tdi) ? null : new CaTipoDocumentoIdentitaDto(tdi.getId(), tdi.getNome()));
    }

    public static CaTipoDocumentoIdentita fromDtoToModel(CaTipoDocumentoIdentitaDto tdi){
        return (Objects.isNull(tdi) ? null : new CaTipoDocumentoIdentita(tdi.getId(), tdi.getNome()));
    }
}
