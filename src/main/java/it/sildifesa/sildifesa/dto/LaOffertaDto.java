package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.la.LaOfferta;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

public class LaOffertaDto {


    private Long id;

    private Integer anno;

    private String areaFunzionale;

    private String areaFunzionaleLettera;

    private Boolean chiusa;


    private String compenso;

    private Boolean concorso;

    private Boolean conteggiaRiserva;

    private String cpi;

    @NotNull(message = "Data obbligatoria")
    private Date data;

    private Date dataFine;

    @NotNull(message = "Titolo Offerta obbligatorio")
    private String descrizione;

    private String descrizioneAttivita;

    @NotNull(message = "Descrizione Offerta obbligatorio")
    private String descrizioneofferta;

    private Boolean diffusione;

    private CaAnagraficaAziendaDto azienda;

    private String livelloContratto;

    @Valid
    private SuComuneDto localitaSedeOfferta;

    @NotNull(message = "Competenze obbligatorie")
    private String competenzeRichieste;

    private String note;

    @NotNull(message = "Numero Posti obbligatorio")
    private Integer numeroPosti;

    private Integer numeroPostiRiservati;

    private String numeroRif;

    private Boolean previstaRiserva;

    private SuProfessioneDto professione;

    private String regioneRif;

    private Boolean rilievo;

    private Boolean rilievoMassDir;

    private Double riservaDiLegge;

    private String rispostarilievo;

    private String rispostarilievomassdir;

    private String sede;

    private LstContrattoDto settoreContratto;

    private String tipologiaRapporto;

    private Boolean tipologiaRif;

    private String tipoOffertaLavoro;

    @NotNull(message = "Titolo Studio richiesto obbligatorio")
    private Integer titoloStudioRichiesto;

    private Boolean attivo;

    private String cccnlApplicato;

    private Integer compensoMensile;

    private Set<LstTipoContrattoDto> laOffertaTipoContratti;

    public LaOffertaDto() {
    }

    public LaOffertaDto(Long id, Integer anno, String areaFunzionale, String areaFunzionaleLettera, Boolean chiusa, String compenso, Boolean concorso, Boolean conteggiaRiserva, String cpi, Date data, Date dataFine, String descrizione, String descrizioneAttivita, String descrizioneofferta, Boolean diffusione, CaAnagraficaAziendaDto azienda, String livelloContratto, SuComuneDto localitaSedeOfferta, String competenzeRichieste, String note, Integer numeroPosti, Integer numeroPostiRiservati, String numeroRif, Boolean previstaRiserva, SuProfessioneDto professione, String regioneRif, Boolean rilievo, Boolean rilievoMassDir, Double riservaDiLegge, String rispostarilievo, String rispostarilievomassdir, String sede, LstContrattoDto settoreContratto, String tipologiaRapporto, Boolean tipologiaRif, String tipoOffertaLavoro, Integer titoloStudioRichiesto, Boolean attivo, String cccnlApplicato, Integer compensoMensile, Set<LstTipoContrattoDto> laOffertaTipoContratti) {
        this.id = id;
        this.anno = anno;
        this.areaFunzionale = areaFunzionale;
        this.areaFunzionaleLettera = areaFunzionaleLettera;
        this.chiusa = chiusa;
        this.compenso = compenso;
        this.concorso = concorso;
        this.conteggiaRiserva = conteggiaRiserva;
        this.cpi = cpi;
        this.data = data;
        this.dataFine = dataFine;
        this.descrizione = descrizione;
        this.descrizioneAttivita = descrizioneAttivita;
        this.descrizioneofferta = descrizioneofferta;
        this.diffusione = diffusione;
        this.azienda = azienda;
        this.livelloContratto = livelloContratto;
        this.localitaSedeOfferta = localitaSedeOfferta;
        this.competenzeRichieste = competenzeRichieste;
        this.note = note;
        this.numeroPosti = numeroPosti;
        this.numeroPostiRiservati = numeroPostiRiservati;
        this.numeroRif = numeroRif;
        this.previstaRiserva = previstaRiserva;
        this.professione = professione;
        this.regioneRif = regioneRif;
        this.rilievo = rilievo;
        this.rilievoMassDir = rilievoMassDir;
        this.riservaDiLegge = riservaDiLegge;
        this.rispostarilievo = rispostarilievo;
        this.rispostarilievomassdir = rispostarilievomassdir;
        this.sede = sede;
        this.settoreContratto = settoreContratto;
        this.tipologiaRapporto = tipologiaRapporto;
        this.tipologiaRif = tipologiaRif;
        this.tipoOffertaLavoro = tipoOffertaLavoro;
        this.titoloStudioRichiesto = titoloStudioRichiesto;
        this.attivo = attivo;
        this.cccnlApplicato = cccnlApplicato;
        this.compensoMensile = compensoMensile;
        this.laOffertaTipoContratti = laOffertaTipoContratti;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public String getAreaFunzionale() {
        return areaFunzionale;
    }

    public void setAreaFunzionale(String areaFunzionale) {
        this.areaFunzionale = areaFunzionale;
    }

    public String getAreaFunzionaleLettera() {
        return areaFunzionaleLettera;
    }

    public void setAreaFunzionaleLettera(String areaFunzionaleLettera) {
        this.areaFunzionaleLettera = areaFunzionaleLettera;
    }

    public Boolean getChiusa() {
        return chiusa;
    }

    public void setChiusa(Boolean chiusa) {
        this.chiusa = chiusa;
    }

    public String getCompenso() {
        return compenso;
    }

    public void setCompenso(String compenso) {
        this.compenso = compenso;
    }

    public Boolean getConcorso() {
        return concorso;
    }

    public void setConcorso(Boolean concorso) {
        this.concorso = concorso;
    }

    public Boolean getConteggiaRiserva() {
        return conteggiaRiserva;
    }

    public void setConteggiaRiserva(Boolean conteggiaRiserva) {
        this.conteggiaRiserva = conteggiaRiserva;
    }

    public String getCpi() {
        return cpi;
    }

    public void setCpi(String cpi) {
        this.cpi = cpi;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getDescrizioneAttivita() {
        return descrizioneAttivita;
    }

    public void setDescrizioneAttivita(String descrizioneAttivita) {
        this.descrizioneAttivita = descrizioneAttivita;
    }

    public String getDescrizioneofferta() {
        return descrizioneofferta;
    }

    public void setDescrizioneofferta(String descrizioneofferta) {
        this.descrizioneofferta = descrizioneofferta;
    }

    public Boolean getDiffusione() {
        return diffusione;
    }

    public void setDiffusione(Boolean diffusione) {
        this.diffusione = diffusione;
    }

    public CaAnagraficaAziendaDto getAzienda() {
        return azienda;
    }

    public void setAzienda(CaAnagraficaAziendaDto azienda) {
        this.azienda = azienda;
    }

    public String getLivelloContratto() {
        return livelloContratto;
    }

    public void setLivelloContratto(String livelloContratto) {
        this.livelloContratto = livelloContratto;
    }

    public SuComuneDto getLocalitaSedeOfferta() {
        return localitaSedeOfferta;
    }

    public void setLocalitaSedeOfferta(SuComuneDto localitaSedeOfferta) {
        this.localitaSedeOfferta = localitaSedeOfferta;
    }

    public String getCompetenzeRichieste() {
        return competenzeRichieste;
    }

    public void setCompetenzeRichieste(String competenzeRichieste) {
        this.competenzeRichieste = competenzeRichieste;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getNumeroPosti() {
        return numeroPosti;
    }

    public void setNumeroPosti(Integer numeroPosti) {
        this.numeroPosti = numeroPosti;
    }

    public Integer getNumeroPostiRiservati() {
        return numeroPostiRiservati;
    }

    public void setNumeroPostiRiservati(Integer numeroPostiRiservati) {
        this.numeroPostiRiservati = numeroPostiRiservati;
    }

    public String getNumeroRif() {
        return numeroRif;
    }

    public void setNumeroRif(String numeroRif) {
        this.numeroRif = numeroRif;
    }

    public Boolean getPrevistaRiserva() {
        return previstaRiserva;
    }

    public void setPrevistaRiserva(Boolean previstaRiserva) {
        this.previstaRiserva = previstaRiserva;
    }

    public SuProfessioneDto getProfessione() {
        return professione;
    }

    public void setProfessione(SuProfessioneDto professione) {
        this.professione = professione;
    }

    public String getRegioneRif() {
        return regioneRif;
    }

    public void setRegioneRif(String regioneRif) {
        this.regioneRif = regioneRif;
    }

    public Boolean getRilievo() {
        return rilievo;
    }

    public void setRilievo(Boolean rilievo) {
        this.rilievo = rilievo;
    }

    public Boolean getRilievoMassDir() {
        return rilievoMassDir;
    }

    public void setRilievoMassDir(Boolean rilievoMassDir) {
        this.rilievoMassDir = rilievoMassDir;
    }

    public Double getRiservaDiLegge() {
        return riservaDiLegge;
    }

    public void setRiservaDiLegge(Double riservaDiLegge) {
        this.riservaDiLegge = riservaDiLegge;
    }

    public String getRispostarilievo() {
        return rispostarilievo;
    }

    public void setRispostarilievo(String rispostarilievo) {
        this.rispostarilievo = rispostarilievo;
    }

    public String getRispostarilievomassdir() {
        return rispostarilievomassdir;
    }

    public void setRispostarilievomassdir(String rispostarilievomassdir) {
        this.rispostarilievomassdir = rispostarilievomassdir;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public LstContrattoDto getSettoreContratto() {
        return settoreContratto;
    }

    public void setSettoreContratto(LstContrattoDto settoreContratto) {
        this.settoreContratto = settoreContratto;
    }

    public String getTipologiaRapporto() {
        return tipologiaRapporto;
    }

    public void setTipologiaRapporto(String tipologiaRapporto) {
        this.tipologiaRapporto = tipologiaRapporto;
    }

    public Boolean getTipologiaRif() {
        return tipologiaRif;
    }

    public void setTipologiaRif(Boolean tipologiaRif) {
        this.tipologiaRif = tipologiaRif;
    }

    public String getTipoOffertaLavoro() {
        return tipoOffertaLavoro;
    }

    public void setTipoOffertaLavoro(String tipoOffertaLavoro) {
        this.tipoOffertaLavoro = tipoOffertaLavoro;
    }

    public Integer getTitoloStudioRichiesto() {
        return titoloStudioRichiesto;
    }

    public void setTitoloStudioRichiesto(Integer titoloStudioRichiesto) {
        this.titoloStudioRichiesto = titoloStudioRichiesto;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public String getCccnlApplicato() {
        return cccnlApplicato;
    }

    public void setCccnlApplicato(String cccnlApplicato) {
        this.cccnlApplicato = cccnlApplicato;
    }

    public Integer getCompensoMensile() {
        return compensoMensile;
    }

    public void setCompensoMensile(Integer compensoMensile) {
        this.compensoMensile = compensoMensile;
    }

    public Set<LstTipoContrattoDto> getLaOffertaTipoContratti() {
        return laOffertaTipoContratti;
    }

    public void setLaOffertaTipoContratti(Set<LstTipoContrattoDto> laOffertaTipoContratti) {
        this.laOffertaTipoContratti = laOffertaTipoContratti;
    }

    public static LaOffertaDto fromModelToDto(LaOfferta model) {
        return Objects.isNull(model) ? null : new LaOffertaDto(model.getId(), model.getAnno(), model.getAreaFunzionale(), model.getAreaFunzionaleLettera(), model.getChiusa(), model.getCompenso(), model.getConcorso(), model.getConteggiaRiserva(), model.getCpi(), model.getData(), model.getDataFine(), model.getDescrizione(), model.getDescrizioneAttivita(), model.getDescrizioneofferta(), model.getDiffusione(), CaAnagraficaAziendaDto.fromModelToDto(model.getAzienda()), model.getLivelloContratto(), SuComuneDto.fromModelToDtoSenzaRelazioni(model.getLocalitaSedeOfferta(), true), model.getCompetenzeRichieste(), model.getNote(), model.getNumeroPosti(), model.getNumeroPostiRiservati(), model.getNumeroRif(), model.getPrevistaRiserva(), SuProfessioneDto.fromModelToDto(model.getProfessione()), model.getRegioneRif(), model.getRilievo(), model.getRilievoMassDir(), model.getRiservaDiLegge(), model.getRispostarilievo(), model.getRispostarilievomassdir(), model.getSede(), LstContrattoDto.fromModelToDto(model.getSettoreContratto()), model.getTipologiaRapporto(), model.getTipologiaRif(), model.getTipoOffertaLavoro(), model.getTitoloStudioRichiesto(), model.getAttivo(), model.getCccnlApplicato(), model.getCompensoMensile(), LstTipoContrattoDto.fromSetSModelToSetSDto(model.getLaOffertaTipoContratti()));
    }

    public static LaOfferta fromDtoToModel(LaOffertaDto dto) {
        return Objects.isNull(dto) ? null : new LaOfferta(dto.getId(), dto.getAnno(), dto.getAreaFunzionale(), dto.getAreaFunzionaleLettera(), dto.getChiusa(), dto.getCompenso(), dto.getConcorso(), dto.getConteggiaRiserva(), dto.getCpi(), dto.getData(), dto.getDataFine(), dto.getDescrizione(), dto.getDescrizioneAttivita(), dto.getDescrizioneofferta(), dto.getDiffusione(), CaAnagraficaAziendaDto.fromDtoToModel(dto.getAzienda()), dto.getLivelloContratto(), SuComuneDto.fromDtoToModelSenzaRelazioni(dto.getLocalitaSedeOfferta()), dto.getCompetenzeRichieste(), dto.getNote(), dto.getNumeroPosti(), dto.getNumeroPostiRiservati(), dto.getNumeroRif(), dto.getPrevistaRiserva(), SuProfessioneDto.fromDtoToModel(dto.getProfessione()), dto.getRegioneRif(), dto.getRilievo(), dto.getRilievoMassDir(), dto.getRiservaDiLegge(), dto.getRispostarilievo(), dto.getRispostarilievomassdir(), dto.getSede(), LstContrattoDto.fromDtoToModel(dto.getSettoreContratto()), dto.getTipologiaRapporto(), dto.getTipologiaRif(), dto.getTipoOffertaLavoro(), dto.getTitoloStudioRichiesto(), dto.getAttivo(), dto.getCccnlApplicato(), dto.getCompensoMensile(), LstTipoContrattoDto.fromSetSDtoToSetSModel(dto.getLaOffertaTipoContratti()));
    }


    @Override
    public String toString() {
        return "LaOffertaDto{" +
                "id=" + id +
                ", anno=" + anno +
                ", areaFunzionale='" + areaFunzionale + '\'' +
                ", areaFunzionaleLettera='" + areaFunzionaleLettera + '\'' +
                ", chiusa=" + chiusa +
                ", compenso='" + compenso + '\'' +
                ", concorso=" + concorso +
                ", conteggiaRiserva=" + conteggiaRiserva +
                ", cpi='" + cpi + '\'' +
                ", data=" + data +
                ", dataFine=" + dataFine +
                ", descrizione='" + descrizione + '\'' +
                ", descrizioneAttivita='" + descrizioneAttivita + '\'' +
                ", descrizioneofferta='" + descrizioneofferta + '\'' +
                ", diffusione=" + diffusione +
                ", azienda=" + azienda +
                ", livelloContratto='" + livelloContratto + '\'' +
                ", localitaSedeOfferta=" + localitaSedeOfferta +
                ", competenzeRichieste='" + competenzeRichieste + '\'' +
                ", note='" + note + '\'' +
                ", numeroPosti=" + numeroPosti +
                ", numeroPostiRiservati=" + numeroPostiRiservati +
                ", numeroRif='" + numeroRif + '\'' +
                ", previstaRiserva=" + previstaRiserva +
                ", professione=" + professione +
                ", regioneRif='" + regioneRif + '\'' +
                ", rilievo=" + rilievo +
                ", rilievoMassDir=" + rilievoMassDir +
                ", riservaDiLegge=" + riservaDiLegge +
                ", rispostarilievo='" + rispostarilievo + '\'' +
                ", rispostarilievomassdir='" + rispostarilievomassdir + '\'' +
                ", sede='" + sede + '\'' +
                ", settoreContratto=" + settoreContratto +
                ", tipologiaRapporto='" + tipologiaRapporto + '\'' +
                ", tipologiaRif=" + tipologiaRif +
                ", tipoOffertaLavoro='" + tipoOffertaLavoro + '\'' +
                ", titoloStudioRichiesto=" + titoloStudioRichiesto +
                ", attivo=" + attivo +
                ", cccnlApplicato='" + cccnlApplicato + '\'' +
                ", compensoMensile=" + compensoMensile +
                ", laOffertaTipoContratti=" + laOffertaTipoContratti +
                '}';
    }
}
