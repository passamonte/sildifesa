package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.ca.CaDocumentoAzienda;

import javax.persistence.Lob;
import java.util.Date;


public class CaDocumentoAziendaDto {

    Long id;

    @Lob
    private byte[] documentoAziendaFile;

    private String nomeDocumentoAziendaFile;

    private String estensioneDocumentoIdentitaFile;

    private Date dataIscrizioneCameraCommercio;

    private CaAnagraficaAzienda caAnagraficaAzienda;


    public CaDocumentoAziendaDto() {
    }

    public CaDocumentoAziendaDto(Long id, byte[] documentoAziendaFile, String nomeDocumentoAziendaFile, String estensioneDocumentoIdentitaFile, Date dataIscrizioneCameraCommercio) {
        this.id = id;
        this.documentoAziendaFile = documentoAziendaFile;
        this.nomeDocumentoAziendaFile = nomeDocumentoAziendaFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
        this.dataIscrizioneCameraCommercio = dataIscrizioneCameraCommercio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getDocumentoAziendaFile() {
        return documentoAziendaFile;
    }

    public void setDocumentoAziendaFile(byte[] documentoAziendaFile) {
        this.documentoAziendaFile = documentoAziendaFile;
    }

    public String getNomeDocumentoAziendaFile() {
        return nomeDocumentoAziendaFile;
    }

    public void setNomeDocumentoAziendaFile(String nomeDocumentoAziendaFile) {
        this.nomeDocumentoAziendaFile = nomeDocumentoAziendaFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public Date getDataIscrizioneCameraCommercio() {
        return dataIscrizioneCameraCommercio;
    }

    public void setDataIscrizioneCameraCommercio(Date dataIscrizioneCameraCommercio) {
        this.dataIscrizioneCameraCommercio = dataIscrizioneCameraCommercio;
    }

    public static CaDocumentoAziendaDto fromModelToDto(CaDocumentoAzienda doc) {
        if (doc == null){
            return null;
        }
        return new CaDocumentoAziendaDto(doc.getId(), doc.getDocumentoAziendaFile(), doc.getNomeDocumentoAziendaFile()
                , doc.getEstensioneDocumentoIdentitaFile(), doc.getDataIscrizioneCameraCommercio());
    }

    public static CaDocumentoAziendaDto fromModelToDto(CaDocumentoAziendaDto model){
        return new CaDocumentoAziendaDto(model.getId(), model.getDocumentoAziendaFile(), model.getNomeDocumentoAziendaFile(), model.getEstensioneDocumentoIdentitaFile(), model.getDataIscrizioneCameraCommercio());
    }
}
