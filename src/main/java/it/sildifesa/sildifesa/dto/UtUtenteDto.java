package it.sildifesa.sildifesa.dto;

import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

public class UtUtenteDto {


    private String password;


    private String passwordConfirm;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
