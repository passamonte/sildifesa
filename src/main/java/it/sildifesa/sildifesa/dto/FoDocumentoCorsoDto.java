package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.fo.FoDocumentoCorso;

import javax.persistence.Lob;


public class FoDocumentoCorsoDto {

    Long id;

    @Lob
    private byte[] documentoCorsoFile;

    private String nomeDocumentoCorsoFile;

    private String estensioneDocumentoIdentitaFile;

    private FoCorsoDto foCorso;


    public FoDocumentoCorsoDto() {
    }

    public FoDocumentoCorsoDto(Long id, byte[] documentoCorsoFile, String nomeDocumentoCorsoFile, String estensioneDocumentoIdentitaFile) {
        this.id = id;
        this.documentoCorsoFile = documentoCorsoFile;
        this.nomeDocumentoCorsoFile = nomeDocumentoCorsoFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getDocumentoCorsoFile() {
        return documentoCorsoFile;
    }

    public void setDocumentoCorsoFile(byte[] documentoCorsoFile) {
        this.documentoCorsoFile = documentoCorsoFile;
    }

    public String getNomeDocumentoCorsoFile() {
        return nomeDocumentoCorsoFile;
    }

    public void setNomeDocumentoCorsoFile(String nomeDocumentoCorsoFile) {
        this.nomeDocumentoCorsoFile = nomeDocumentoCorsoFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public static FoDocumentoCorso fromDtoToModel(FoDocumentoCorsoDto dto) {
        if (dto == null){
            return null;
        }
        return new FoDocumentoCorso(dto.getId(), dto.getDocumentoCorsoFile(), dto.getNomeDocumentoCorsoFile(), dto.getEstensioneDocumentoIdentitaFile());
    }

    public static FoDocumentoCorsoDto fromModelToDto(FoDocumentoCorso model) {
        if (model == null){
            return null;
        }
        return new FoDocumentoCorsoDto(model.getId(), model.getDocumentoCorsoFile(), model.getNomeDocumentoCorsoFile(), model.getEstensioneDocumentoIdentitaFile());
    }
}
