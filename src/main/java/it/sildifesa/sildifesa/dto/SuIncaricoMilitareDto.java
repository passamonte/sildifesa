package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.su.SuIncaricoMilitare;

import java.math.BigInteger;
import java.util.List;


public class SuIncaricoMilitareDto {

    private Long id;

    private String codice;


    private String descrizioneBreve;


    private String forzaArmata;


    private String nome;

    private BigInteger transcodificaProfessione;


    private BigInteger transcodificaSettore;


    //bi-directional many-to-one association to CaAnagrafica
    private List<CaAnagraficaDto> caAnagraficas;

    public SuIncaricoMilitareDto() {
    }

    public SuIncaricoMilitareDto(Long id, String codice, String descrizioneBreve, String forzaArmata, String nome, BigInteger transcodificaProfessione, BigInteger transcodificaSettore) {
        this.id = id;
        this.codice = codice;
        this.descrizioneBreve = descrizioneBreve;
        this.forzaArmata = forzaArmata;
        this.nome = nome;
        this.transcodificaProfessione = transcodificaProfessione;
        this.transcodificaSettore = transcodificaSettore;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getDescrizioneBreve() {
        return descrizioneBreve;
    }

    public void setDescrizioneBreve(String descrizioneBreve) {
        this.descrizioneBreve = descrizioneBreve;
    }

    public String getForzaArmata() {
        return forzaArmata;
    }

    public void setForzaArmata(String forzaArmata) {
        this.forzaArmata = forzaArmata;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigInteger getTranscodificaProfessione() {
        return transcodificaProfessione;
    }

    public void setTranscodificaProfessione(BigInteger transcodificaProfessione) {
        this.transcodificaProfessione = transcodificaProfessione;
    }

    public BigInteger getTranscodificaSettore() {
        return transcodificaSettore;
    }

    public void setTranscodificaSettore(BigInteger transcodificaSettore) {
        this.transcodificaSettore = transcodificaSettore;
    }

    public List<CaAnagraficaDto> getCaAnagraficas() {
        return this.caAnagraficas;
    }

    public void setCaAnagraficas(List<CaAnagraficaDto> caAnagraficas) {
        this.caAnagraficas = caAnagraficas;
    }

    public CaAnagraficaDto addCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().add(caAnagrafica);
        caAnagrafica.setSuIncaricomilitare(this);

        return caAnagrafica;
    }

    public CaAnagraficaDto removeCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().remove(caAnagrafica);
        caAnagrafica.setSuIncaricomilitare(null);

        return caAnagrafica;
    }

    public static SuIncaricoMilitareDto fromModelToDtoSenzaRelazioni(SuIncaricoMilitare incarico){
        if (incarico == null){
            return null;
        }
        return new SuIncaricoMilitareDto(incarico.getId(), incarico.getCodice(), incarico.getDescrizioneBreve(), incarico.getForzaArmata(), incarico.getNome(), incarico.getTranscodificaProfessione(), incarico.getTranscodificaSettore());
    }

    public static SuIncaricoMilitare fromDtoToModelSenzaRelazioni(SuIncaricoMilitareDto incarico){
        if (incarico == null){
            return null;
        }
        return new SuIncaricoMilitare(incarico.getId(), incarico.getCodice(), incarico.getDescrizioneBreve(), incarico.getForzaArmata(), incarico.getNome(), incarico.getTranscodificaProfessione(), incarico.getTranscodificaSettore());
    }

}
