package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.fo.FoCorsoAnagrafica;
import org.hibernate.LazyInitializationException;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class FoCorsoAnagraficaDto {

    private Long id;

    private CaAnagrafica caAnagrafica;

    private FoCorsoDto foCorso;

    public FoCorsoAnagraficaDto() {
    }

    public FoCorsoAnagraficaDto(Long id, CaAnagrafica caAnagrafica) {
        this.id = id;
        this.caAnagrafica = caAnagrafica;
    }

    public FoCorsoAnagraficaDto(Long id, CaAnagrafica caAnagrafica, FoCorsoDto foCorso) {
        this.id = id;
        this.caAnagrafica = caAnagrafica;
        this.foCorso = foCorso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CaAnagrafica getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagrafica caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public FoCorsoDto getFoCorso() {
        return foCorso;
    }

    public void setFoCorso(FoCorsoDto foCorso) {
        this.foCorso = foCorso;
    }

    public static FoCorsoAnagraficaDto fromModelToDto(FoCorsoAnagrafica model) {
        try {
            return Objects.isNull(model) ? null : new FoCorsoAnagraficaDto(model.getId(), model.getCaAnagrafica());
        } catch (LazyInitializationException e) {
            return null;
        }
    }

    public static FoCorsoAnagrafica fromDtoToModel(FoCorsoAnagraficaDto dto) {
        return Objects.isNull(dto) ? null : new FoCorsoAnagrafica(dto.getId(), dto.getCaAnagrafica());
    }

    public static Set<FoCorsoAnagraficaDto> fromSetModelToSetDto(Set<FoCorsoAnagrafica> models){
        try {
            return models.stream().map(FoCorsoAnagraficaDto::fromModelToDto).collect(Collectors.toSet());
        } catch (LazyInitializationException e) {
            return null;
        }
    }

    public static Set<FoCorsoAnagrafica> fromSetDtoToSetModel(Set<FoCorsoAnagraficaDto> dtos){
        return Objects.isNull(dtos)?null:dtos.stream().map(FoCorsoAnagraficaDto::fromDtoToModel).collect(Collectors.toSet());
    }

}
