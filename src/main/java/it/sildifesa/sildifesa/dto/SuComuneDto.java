package it.sildifesa.sildifesa.dto;


import it.sildifesa.sildifesa.models.su.SuComune;
import org.hibernate.LazyInitializationException;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SuComuneDto {

    @NotNull(message = "Comune obbligatorio")
    private Long id;

    private String cap;


    private String codiceCatastale;


    private String codiceFisco;


    private String codiceIstat;


    private String codiceRegioneBNL;


    private String nazione;


    private String nome;


    private String prefisso;


    private String provincia;


    private String provinciaEstesa;


    private String regione;

    private List<CaAnagraficaDto> caAnagraficas1;

    private List<CaAnagraficaDto> caAnagraficas2;

    public SuComuneDto() {
    }

    public SuComuneDto(Long id, String cap, String codiceCatastale, String codiceFisco, String codiceIstat, String codiceRegioneBNL, String nazione, String nome, String prefisso, String provincia, String provinciaEstesa, String regione) {
        this.id = id;
        this.cap = cap;
        this.codiceCatastale = codiceCatastale;
        this.codiceFisco = codiceFisco;
        this.codiceIstat = codiceIstat;
        this.codiceRegioneBNL = codiceRegioneBNL;
        this.nazione = nazione;
        this.nome = nome;
        this.prefisso = prefisso;
        this.provincia = provincia;
        this.provinciaEstesa = provinciaEstesa;
        this.regione = regione;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getCodiceCatastale() {
        return codiceCatastale;
    }

    public void setCodiceCatastale(String codiceCatastale) {
        this.codiceCatastale = codiceCatastale;
    }

    public String getCodiceFisco() {
        return codiceFisco;
    }

    public void setCodiceFisco(String codiceFisco) {
        this.codiceFisco = codiceFisco;
    }

    public String getCodiceIstat() {
        return codiceIstat;
    }

    public void setCodiceIstat(String codiceIstat) {
        this.codiceIstat = codiceIstat;
    }

    public String getCodiceRegioneBNL() {
        return codiceRegioneBNL;
    }

    public void setCodiceRegioneBNL(String codiceRegioneBNL) {
        this.codiceRegioneBNL = codiceRegioneBNL;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPrefisso() {
        return prefisso;
    }

    public void setPrefisso(String prefisso) {
        this.prefisso = prefisso;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getProvinciaEstesa() {
        return provinciaEstesa;
    }

    public void setProvinciaEstesa(String provinciaEstesa) {
        this.provinciaEstesa = provinciaEstesa;
    }

    public String getRegione() {
        return regione;
    }

    public void setRegione(String regione) {
        this.regione = regione;
    }

    public List<CaAnagraficaDto> getCaAnagraficas1() {
        return caAnagraficas1;
    }

    public void setCaAnagraficas1(List<CaAnagraficaDto> caAnagraficas1) {
        this.caAnagraficas1 = caAnagraficas1;
    }

    public CaAnagraficaDto addCaAnagraficas1(CaAnagraficaDto caAnagraficas1) {
        getCaAnagraficas1().add(caAnagraficas1);
        caAnagraficas1.setSuComune1(this);

        return caAnagraficas1;
    }

    public CaAnagraficaDto removeCaAnagraficas1(CaAnagraficaDto caAnagraficas1) {
        getCaAnagraficas1().remove(caAnagraficas1);
        caAnagraficas1.setSuComune1(null);

        return caAnagraficas1;
    }

    public List<CaAnagraficaDto> getCaAnagraficas2() {
        return this.caAnagraficas2;
    }

    public void setCaAnagraficas2(List<CaAnagraficaDto> caAnagraficas2) {
        this.caAnagraficas2 = caAnagraficas2;
    }

    public CaAnagraficaDto addCaAnagraficas2(CaAnagraficaDto caAnagraficas2) {
        getCaAnagraficas2().add(caAnagraficas2);
        caAnagraficas2.setSuComune2(this);

        return caAnagraficas2;
    }

    public CaAnagraficaDto removeCaAnagraficas2(CaAnagraficaDto caAnagraficas2) {
        getCaAnagraficas2().remove(caAnagraficas2);
        caAnagraficas2.setSuComune2(null);

        return caAnagraficas2;
    }

    public static SuComuneDto fromModelToDtoSenzaRelazioni(SuComune comune, boolean flagProvincia){
        try {
            if (comune == null) {
                return null;
            }
            String com = comune.getNome();
            if (flagProvincia) {
                com += " (" + comune.getProvincia() + ")";
            }

            return new SuComuneDto(comune.getId(), comune.getCap(), comune.getCodiceCatastale(), comune.getCodiceFisco(), comune.getCodiceIstat(), comune.getCodiceRegioneBNL(), comune.getNazione(), com, comune.getPrefisso(), comune.getProvincia(), comune.getProvinciaEstesa(), comune.getRegione());
        } catch (LazyInitializationException e) {
            return null;
        }
    }
    public static SuComuneDto fromModelToDtoSenzaRelazioniNoFlag(SuComune comune){
        String com = "";
        return new SuComuneDto(comune.getId(), comune.getCap(), comune.getCodiceCatastale(), comune.getCodiceFisco(), comune.getCodiceIstat(), comune.getCodiceRegioneBNL(), comune.getNazione(), com, comune.getPrefisso(), comune.getProvincia(), comune.getProvinciaEstesa(), comune.getRegione());
    }

    public static SuComune fromDtoToModelSenzaRelazioni(SuComuneDto comune){
        try {
            return new SuComune(comune.getId(), comune.getCap(), comune.getCodiceCatastale(), comune.getCodiceFisco(), comune.getCodiceIstat(), comune.getCodiceRegioneBNL(), comune.getNazione(), comune.getNome(), comune.getPrefisso(), comune.getProvincia(), comune.getProvinciaEstesa(), comune.getRegione());

        } catch (LazyInitializationException e) {
            return null;
        }
    }


}

