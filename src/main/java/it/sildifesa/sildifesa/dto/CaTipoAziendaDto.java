package it.sildifesa.sildifesa.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CaTipoAziendaDto {

    @NotNull(message = "Forma Giuridica obbligatoria")
    private Long id;

    private String nomeTipologia;

    private String nomeTipologiaBreve;

    @JsonIgnore
    private List<CaAnagraficaAzienda> azienda;

    public CaTipoAziendaDto() {
    }

    public CaTipoAziendaDto(Long id, String nomeTipologia, String nomeTipologiaBreve) {
        this.id = id;
        this.nomeTipologia = nomeTipologia;
        this.nomeTipologiaBreve = nomeTipologiaBreve;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeTipologia() {
        return nomeTipologia;
    }

    public void setNomeTipologia(String nomeTipologia) {
        this.nomeTipologia = nomeTipologia;
    }

    public List<CaAnagraficaAzienda> getAzienda() {
        return azienda;
    }

    public void setAzienda(List<CaAnagraficaAzienda> azienda) {
        this.azienda = azienda;
    }

    public String getNomeTipologiaBreve() {
        return nomeTipologiaBreve;
    }

    public void setNomeTipologiaBreve(String nomeTipologiaBreve) {
        this.nomeTipologiaBreve = nomeTipologiaBreve;
    }

    public static CaTipoAziendaDto fromModelToDto(CaTipoAzienda model){
        if (model == null){
            return null;
        }
        return new CaTipoAziendaDto(model.getId(), model.getNomeTipologia(), model.getNomeTipologiaBreve());
    }
}
