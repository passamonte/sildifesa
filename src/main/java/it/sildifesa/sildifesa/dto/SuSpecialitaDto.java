package it.sildifesa.sildifesa.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.su.SuSpecialita;
import org.hibernate.LazyInitializationException;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SuSpecialitaDto {

    private Long id;

    private String forzaArmata;

    @NotNull(message = "Specialita obbligatoria")
    private String nome;

    @JsonIgnore
    private List<CaAnagraficaDto> caAnagraficas;

    public SuSpecialitaDto() {
    }

    public SuSpecialitaDto(Long id, String forzaArmata, String nome) {
        this.id = id;
        this.forzaArmata = forzaArmata;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getForzaArmata() {
        return forzaArmata;
    }

    public void setForzaArmata(String forzaArmata) {
        this.forzaArmata = forzaArmata;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<CaAnagraficaDto> getCaAnagraficas() {
        return caAnagraficas;
    }

    public void setCaAnagraficas(List<CaAnagraficaDto> caAnagraficas) {
        this.caAnagraficas = caAnagraficas;
    }

    public CaAnagraficaDto addCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().add(caAnagrafica);
        caAnagrafica.setSuSpecialita(this);

        return caAnagrafica;
    }

    public CaAnagraficaDto removeCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        getCaAnagraficas().remove(caAnagrafica);
        caAnagrafica.setSuSpecialita(null);

        return caAnagrafica;
    }

    public static SuSpecialitaDto fromModelToDtoSenzaRelazioni(SuSpecialita spec) {
        try {
            if (spec == null) {
                return null;
            }
            return new SuSpecialitaDto(spec.getId(), spec.getForzaArmata(), spec.getNome());
        }catch (LazyInitializationException e){
            return null;
        }
    }

    public static SuSpecialita fromDtoToModelSenzaRelazioni(SuSpecialitaDto spec) {
        if (spec == null){
            return null;
        }
        return new SuSpecialita(spec.getId(), spec.getForzaArmata(), spec.getNome());
    }

}
