package it.sildifesa.sildifesa.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import it.sildifesa.sildifesa.models.su.SuProfessione;
import org.hibernate.LazyInitializationException;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class SuProfessioneDto {

    private Long id;

    private String codice;

    private String nome;

    private BigInteger parent;

    @Valid
    @JsonIgnore
    private List<CaAnagraficaDto> caAnagrafiche;

    @Valid
    @JsonIgnore
    private List<LaOfferta> offerte;

    public SuProfessioneDto() {
    }

    public SuProfessioneDto(Long id){
        this.id = id;
    }

    public SuProfessioneDto(Long id, String codice, String nome, BigInteger parent, List<LaOfferta> offerte) {
        this.id = id;
        this.codice = codice;
        this.nome = nome;
        this.parent = parent;
        this.offerte = offerte;
    }

    public SuProfessioneDto(Long id, String codice, String nome, BigInteger parent) {
        this.id = id;
        this.codice = codice;
        this.nome = nome;
        this.parent = parent;
        this.offerte = offerte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigInteger getParent() {
        return parent;
    }

    public void setParent(BigInteger parent) {
        this.parent = parent;
    }

    public List<CaAnagraficaDto> getCaAnagrafiche() { return caAnagrafiche; }

    public void setCaAnagrafiche(List<CaAnagraficaDto> caAnagrafiche) { this.caAnagrafiche = caAnagrafiche; }

    public List<LaOfferta> getOfferte() { return offerte; }

    public void setOfferte(List<LaOfferta> offerte) { this.offerte = offerte; }

    public static SuProfessioneDto fromModelToDto (SuProfessione model) {
        return Objects.isNull(model)?null:new SuProfessioneDto(model.getId(), model.getCodice(),
                model.getNome(), model.getParent(),
                model.getOfferte());
    }

    public static SuProfessioneDto fromModelToDtoBase (SuProfessione model) {
        return Objects.isNull(model)?null:new SuProfessioneDto(model.getId(), model.getCodice(),
                model.getNome(), model.getParent());
    }

    public static Set<SuProfessioneDto> fromListModelToListDto (Set<SuProfessione> listaModel) {
        try {
            Set<SuProfessioneDto> lista = new HashSet<>();
            listaModel.forEach(item -> lista.add(fromModelToDtoBase(item)));
            return lista;
        } catch (LazyInitializationException e) {
            return null;
        }
    }

    public static SuProfessione fromDtoToModel (SuProfessioneDto dto) {
        List<CaAnagrafica> ca = new ArrayList<>();
        if(dto.getCaAnagrafiche()!= null)
         ca = CaAnagraficaDto.fromListDtoToListModel(dto.getCaAnagrafiche());

        return new SuProfessione(dto.getId(), dto.getCodice(),
                dto.getNome(), dto.getParent(),
                ca,
                dto.getOfferte());
    }

    public static Set<SuProfessione> fromListDtoToListModel (Set<SuProfessioneDto> listaDto) {
        return Objects.isNull(listaDto)?null:listaDto.stream().map(SuProfessioneDto::fromDtoToModel).collect(Collectors.toSet());
    }

}
