package it.sildifesa.sildifesa.dto;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;
import it.sildifesa.sildifesa.models.su.SuTitoloStudio;
import org.hibernate.LazyInitializationException;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CaAnagraficaTitoloDiStudioDto {

    private Long id;

    @NotNull(message = "Anno titolo di studio obbligatorio")
    private Integer annoConseguimento;

    @NotNull(message = "Istituto conseguimento è obbligatorio")
    private String istitutoConseguimento;

    private CaAnagraficaDto caAnagrafica;

    private SuTitoloStudioDto suTitoloStudio;

    public CaAnagraficaTitoloDiStudioDto() {
    }

    public CaAnagraficaTitoloDiStudioDto(Long id, Integer annoConseguimento, String istitutoConseguimento, SuTitoloStudioDto suTitoloStudio) {
        this.id = id;
        this.annoConseguimento = annoConseguimento;
        this.istitutoConseguimento = istitutoConseguimento;
        this.suTitoloStudio = suTitoloStudio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnnoConseguimento() {
        return annoConseguimento;
    }

    public void setAnnoConseguimento(Integer annoConseguimento) {
        this.annoConseguimento = annoConseguimento;
    }

    public String getIstitutoConseguimento() {
        return istitutoConseguimento;
    }

    public void setIstitutoConseguimento(String istitutoConseguimento) {
        this.istitutoConseguimento = istitutoConseguimento;
    }

    public CaAnagraficaDto getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public SuTitoloStudioDto getSuTitoloStudio() {
        return suTitoloStudio;
    }

    public void setSuTitoloStudio(SuTitoloStudioDto suTitoloStudio) {
        this.suTitoloStudio = suTitoloStudio;
    }

    public static CaAnagraficaTitoloDiStudioDto fromModelToDto(CaAnagraficaTitoloDiStudio ats) {
        try {
            return new CaAnagraficaTitoloDiStudioDto(ats.getId(), ats.getAnnoConseguimento(), ats.getIstitutoConseguimento(), SuTitoloStudioDto.fromModelToDto(ats.getSuTitoloStudio()));
        } catch (LazyInitializationException e) {
            return null;
        }
    }

    public static List<CaAnagraficaTitoloDiStudioDto> fromListModelToListDto(List<CaAnagraficaTitoloDiStudio> listaModel){
        try {
            List<CaAnagraficaTitoloDiStudioDto> lista = new ArrayList<>();
            listaModel.forEach(item -> lista.add(fromModelToDto(item)));
            return lista;
        }catch (LazyInitializationException e){
            return null;
        }
    }

    public static CaAnagraficaTitoloDiStudio fromModelToDto(CaAnagraficaTitoloDiStudioDto ats){
        return new CaAnagraficaTitoloDiStudio(ats.getId(), ats.getAnnoConseguimento(), ats.getIstitutoConseguimento(), SuTitoloStudioDto.fromDtoToModel(ats.getSuTitoloStudio()));
    }

    public static List<CaAnagraficaTitoloDiStudio> fromListDtoToListModel(List<CaAnagraficaTitoloDiStudioDto> listaModel){
        List<CaAnagraficaTitoloDiStudio> lista = new ArrayList<>();
        listaModel.forEach(item -> lista.add(fromModelToDto(item)));
        return lista;
    }
}
