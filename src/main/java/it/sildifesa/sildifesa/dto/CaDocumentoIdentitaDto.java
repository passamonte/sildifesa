package it.sildifesa.sildifesa.dto;


import it.sildifesa.sildifesa.models.ca.CaDocumentoIdentita;
import org.hibernate.LazyInitializationException;

import javax.persistence.Lob;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class CaDocumentoIdentitaDto {

    Long id;

    @NotNull(message = "numero documento d'identità obbligatorio")
    private String numDocumentoIdentita;

    @Lob
    private byte[] documentoIdentitaFile;

    private String nomeDocumentoIdentitaFile;

    private String estensioneDocumentoIdentitaFile;

    private CaAnagraficaDto caAnagrafica;

    @Valid
    private CaTipoDocumentoIdentitaDto caTipoDocumentoIdentita;

    public CaDocumentoIdentitaDto() {
    }

    public CaDocumentoIdentitaDto(String numDocumentoIdentita, byte[] documentoIdentitaFile, String nomeDocumentoIdentitaFile, String estensioneDocumentoIdentitaFile) {
        this.numDocumentoIdentita = numDocumentoIdentita;
        this.documentoIdentitaFile = documentoIdentitaFile;
        this.nomeDocumentoIdentitaFile = nomeDocumentoIdentitaFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public CaDocumentoIdentitaDto(Long id, String numDocumentoIdentita, byte[] documentoIdentitaFile, String nomeDocumentoIdentitaFile, String estensioneDocumentoIdentitaFile, CaTipoDocumentoIdentitaDto caTipoDocumentoIdentita) {
        this.id = id;
        this.numDocumentoIdentita = numDocumentoIdentita;
        this.documentoIdentitaFile = documentoIdentitaFile;
        this.nomeDocumentoIdentitaFile = nomeDocumentoIdentitaFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
        this.caTipoDocumentoIdentita = caTipoDocumentoIdentita;
    }

    public Long getId() {
        return id;
    }

    public CaAnagraficaDto getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagraficaDto caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumDocumentoIdentita() {
        return numDocumentoIdentita;
    }

    public void setNumDocumentoIdentita(String numDocumentoIdentita) {
        this.numDocumentoIdentita = numDocumentoIdentita;
    }

    public byte[] getDocumentoIdentitaFile() {
        return documentoIdentitaFile;
    }

    public void setDocumentoIdentitaFile(byte[] documentoIdentitaFile) {
        this.documentoIdentitaFile = documentoIdentitaFile;
    }

    public String getNomeDocumentoIdentitaFile() {
        return nomeDocumentoIdentitaFile;
    }

    public void setNomeDocumentoIdentitaFile(String nomeDocumentoIdentitaFile) {
        this.nomeDocumentoIdentitaFile = nomeDocumentoIdentitaFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public CaTipoDocumentoIdentitaDto getCaTipoDocumentoIdentita() {
        return caTipoDocumentoIdentita;
    }

    public void setCaTipoDocumentoIdentita(CaTipoDocumentoIdentitaDto caTipoDocumentoIdentita) {
        this.caTipoDocumentoIdentita = caTipoDocumentoIdentita;
    }

    public static CaDocumentoIdentitaDto fromModelToDtoSenzaRelazioni(CaDocumentoIdentita doc){
        try {
            if (doc == null) {
                return null;
            }
            return new CaDocumentoIdentitaDto(doc.getId(), doc.getNumDocumentoIdentita(), doc.getDocumentoIdentitaFile(), doc.getNomeDocumentoIdentitaFile(), doc.getEstensioneDocumentoIdentitaFile(), CaTipoDocumentoIdentitaDto.fromModelToDto(doc.getCaTipoDocumentoIdentita()));
        } catch (LazyInitializationException e) {
            return null;
        }
    }

    public static CaDocumentoIdentita fromDtoToModelSenzaRelazioni(CaDocumentoIdentitaDto doc){
        if (doc == null){
            return null;
        }
        return new CaDocumentoIdentita(doc.getId(), doc.getNumDocumentoIdentita(), doc.getDocumentoIdentitaFile(), doc.getNomeDocumentoIdentitaFile(), doc.getEstensioneDocumentoIdentitaFile(), CaTipoDocumentoIdentitaDto.fromDtoToModel(doc.getCaTipoDocumentoIdentita()));
    }
}
