package it.sildifesa.sildifesa.models.lst;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * The persistent class for the lst_distrettomilitare database table.
 * 
 */
@Entity
@Table(name="lst_distrettomilitare")
@NamedQuery(name="LstDistrettomilitare.findAll", query="SELECT l FROM LstDistrettoMilitare l")
public class LstDistrettoMilitare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Nome Distretto Militare obbligatorio")
	private String nome;

	public LstDistrettoMilitare() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}