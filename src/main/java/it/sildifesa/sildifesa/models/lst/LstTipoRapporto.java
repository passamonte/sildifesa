package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the lst_tiporapporto database table.
 * 
 */
@Entity
@Table(name="lst_tiporapporto")
@NamedQuery(name="LstTiporapporto.findAll", query="SELECT l FROM LstTipoRapporto l")
public class LstTipoRapporto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nome;

	public LstTipoRapporto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}