package it.sildifesa.sildifesa.models.lst;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * The persistent class for the lst_contratto database table.
 * 
 */
@Entity
@Table(name="lst_contratto")
@NamedQuery(name="LstContratto.findAll", query="SELECT l FROM LstContratto l")
public class LstContratto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Nome obbligatorio")
	private String nome;

	@NotEmpty(message = "Livello obbligatorio")
	private String livello;



	public LstContratto() {
	}

	public LstContratto(Long id, String nome, String livello) {
		this.id = id;
		this.nome = nome;
		this.livello = livello;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLivello() {
		return livello;
	}

	public void setLivello(String livello) {
		this.livello = livello;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}