package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigInteger;


/**
 * The persistent class for the lst_arma database table.
 * 
 */
@Entity
@Table(name="lst_arma")
@NamedQuery(name="LstArma.findAll", query="SELECT l FROM LstArma l")
public class LstArma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String forzaArmata;

	@NotEmpty(message = "Nome Forza Armata obbligatorio")
	private String nome;

	public LstArma() {
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getForzaArmata() {
		return this.forzaArmata;
	}

	public void setForzaArmata(String forzaArmata) {
		this.forzaArmata = forzaArmata;
	}



	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}