package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the lst_supportofa database table.
 * 
 */
@Entity
@Table(name="lst_supportofa")
@NamedQuery(name="LstSupportofa.findAll", query="SELECT l FROM LstSupportoFa l")
public class LstSupportoFa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nome;

	public LstSupportoFa() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}