package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigInteger;


/**
 * The persistent class for the lst_grado database table.
 * 
 */
@Entity
@Table(name="lst_grado")
@NamedQuery(name="LstGrado.findAll", query="SELECT l FROM LstGrado l")
public class LstGrado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Codice obbligatorio")
	private String codice;

	private String forzaArmata;

	@NotEmpty(message = "Nome obbligatorio")
	private String nome;

	@NotEmpty(message = "Sigla obbligatoria")
	private String sigla;

	public LstGrado() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getForzaArmata() {
		return forzaArmata;
	}

	public void setForzaArmata(String forzaArmata) {
		this.forzaArmata = forzaArmata;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}