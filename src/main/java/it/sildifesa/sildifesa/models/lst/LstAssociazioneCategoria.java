package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the lst_associazionecategoria database table.
 * 
 */
@Entity
@Table(name="lst_associazionecategoria")
@NamedQuery(name="LstAssociazionecategoria.findAll", query="SELECT l FROM LstAssociazioneCategoria l")
public class LstAssociazioneCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;



	private String nome;

	public LstAssociazioneCategoria() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}