package it.sildifesa.sildifesa.models.lst;

import it.sildifesa.sildifesa.models.la.LaOfferta;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="lst_tipocontratto")
public class LstTipoContratto {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

//    @JsonIgnore
    @ManyToMany(mappedBy = "laOffertaTipoContratti")
    private Set<LaOfferta> laOfferta;


    public LstTipoContratto() {
    }

    public LstTipoContratto(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Set<LaOfferta> getLaOfferta() {
        return laOfferta;
    }

    public void setLaOfferta(Set<LaOfferta> laOfferta) {
        this.laOfferta = laOfferta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
