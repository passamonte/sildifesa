package it.sildifesa.sildifesa.models.lst;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * The persistent class for the lst_categoriaprotetta database table.
 * 
 */
@Entity
@Table(name="lst_categoriaprotetta")
@NamedQuery(name="LstCategoriaprotetta.findAll", query="SELECT l FROM LstCategoriaProtetta l")
public class LstCategoriaProtetta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Nome obbligatorio")
	private String nome;


	public LstCategoriaProtetta() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}