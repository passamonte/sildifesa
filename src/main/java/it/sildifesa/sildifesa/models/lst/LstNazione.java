package it.sildifesa.sildifesa.models.lst;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;


/**
 * The persistent class for the lst_nazione database table.
 * 
 */
@Entity
@Table(name="lst_nazione")
@NamedQuery(name="LstNazione.findAll", query="SELECT l FROM LstNazione l")
public class LstNazione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Codice obbligatorio")
	private String codice;

	@NotNull(message = "Campo Obbligatorio")
	private Byte ee;

	@NotEmpty(message = "Nome obbligatorio")
	private String nome;

	public LstNazione() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public Byte getEe() {
		return ee;
	}

	public void setEe(Byte ee) {
		this.ee = ee;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}