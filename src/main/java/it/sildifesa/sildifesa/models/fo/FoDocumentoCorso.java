package it.sildifesa.sildifesa.models.fo;


import javax.persistence.*;

@Entity
@Table(name = "fo_documentocorso")
public class FoDocumentoCorso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Lob
    private byte[] documentoCorsoFile;

    private String nomeDocumentoCorsoFile;

    private String estensioneDocumentoIdentitaFile;

    @OneToOne(mappedBy = "foDocumentoCorso", cascade = {CascadeType.PERSIST})
    private FoCorso foCorso;


    public FoDocumentoCorso() {
    }

    public FoDocumentoCorso(Long id, byte[] documentoCorsoFile, String nomeDocumentoCorsoFile, String estensioneDocumentoIdentitaFile) {
        this.id = id;
        this.documentoCorsoFile = documentoCorsoFile;
        this.nomeDocumentoCorsoFile = nomeDocumentoCorsoFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getDocumentoCorsoFile() {
        return documentoCorsoFile;
    }

    public void setDocumentoCorsoFile(byte[] documentoCorsoFile) {
        this.documentoCorsoFile = documentoCorsoFile;
    }

    public String getNomeDocumentoCorsoFile() {
        return nomeDocumentoCorsoFile;
    }

    public void setNomeDocumentoCorsoFile(String nomeDocumentoCorsoFile) {
        this.nomeDocumentoCorsoFile = nomeDocumentoCorsoFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public FoCorso getFoCorso() {
        return foCorso;
    }

    public void setFoCorso(FoCorso foCorso) {
        this.foCorso = foCorso;
    }
}
