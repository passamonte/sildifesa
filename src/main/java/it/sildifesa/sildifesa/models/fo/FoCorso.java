package it.sildifesa.sildifesa.models.fo;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.su.SuComune;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="fo_corso")
public class FoCorso implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String capSede;

	private String compenso;

	private String competenzeRichieste;

	private Integer creditiFormativi;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInizio;

	private String descrizione;

	private String durata;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caAnagraficaAzienda")
	private CaAnagraficaAzienda caAnagraficaAzienda;

	private String fascia;

	private String indirizzoSede;

	private Boolean interno;

	private String livelloContratto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "localitaSedeCorso")
	private SuComune localitaSedeCorso;

	private Integer numeroPosti;

	private Boolean perAutoimpiego;

	private Boolean perCooperativa;

	private Boolean perVolontari;

	private String riferimento;

	private String tipologiaContratto;

	private String tipoRapportoLavoro;

	private Boolean tirocinio;

	private String titolo;

	private Integer titoloStudioRichiesto;

	private Boolean attivo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFine;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInizioCandidatura;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFineCandidatura;

	private Integer annoFinanziario;

	private String sezioneTerritorialeCoordinatrice;

	private String certificazioni;

	private String benefit;

	private String telefonoReferente;

	private String emailReferente;

	@OneToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
	@JoinColumn(name = "documentocorso_id", referencedColumnName = "id")
	private FoDocumentoCorso foDocumentoCorso;

	@OneToMany(cascade = {CascadeType.PERSIST}, mappedBy = "foCorso", orphanRemoval = true)
	private Set<FoCorsoAnagrafica> foCorsoAnagraficaList;

	public FoCorso() {
	}

	public FoCorso(Long id, String capSede, String compenso, String competenzeRichieste, Integer creditiFormativi, Date dataInizio, String descrizione, String durata, CaAnagraficaAzienda caAnagraficaAzienda, String fascia, String indirizzoSede, Boolean interno, String livelloContratto, SuComune localitaSedeCorso, Integer numeroPosti, Boolean perAutoimpiego, Boolean perCooperativa, Boolean perVolontari, String riferimento, String tipologiaContratto, String tipoRapportoLavoro, Boolean tirocinio, String titolo, Integer titoloStudioRichiesto, Boolean attivo, FoDocumentoCorso foDocumentoCorso,
				   Date dataFine, Date dataInizioCandidatura, Date dataFineCandidatura, Integer annoFinanziario, String sezioneTerritorialeCoordinatrice, String certificazioni, String benefit, String telefonoReferente, String emailReferente, Set<FoCorsoAnagrafica> foCorsoAnagraficaList) {
		this.id = id;
		this.capSede = capSede;
		this.compenso = compenso;
		this.competenzeRichieste = competenzeRichieste;
		this.creditiFormativi = creditiFormativi;
		this.dataInizio = dataInizio;
		this.descrizione = descrizione;
		this.durata = durata;
		this.caAnagraficaAzienda = caAnagraficaAzienda;
		this.fascia = fascia;
		this.indirizzoSede = indirizzoSede;
		this.interno = interno;
		this.livelloContratto = livelloContratto;
		this.localitaSedeCorso = localitaSedeCorso;
		this.numeroPosti = numeroPosti;
		this.perAutoimpiego = perAutoimpiego;
		this.perCooperativa = perCooperativa;
		this.perVolontari = perVolontari;
		this.riferimento = riferimento;
		this.tipologiaContratto = tipologiaContratto;
		this.tipoRapportoLavoro = tipoRapportoLavoro;
		this.tirocinio = tirocinio;
		this.titolo = titolo;
		this.titoloStudioRichiesto = titoloStudioRichiesto;
		this.attivo = attivo;
		this.foDocumentoCorso = foDocumentoCorso;
		this.dataFine = dataFine;
		this.dataInizioCandidatura = dataInizioCandidatura;
		this.dataFineCandidatura = dataFineCandidatura;
		this.annoFinanziario = annoFinanziario;
		this.sezioneTerritorialeCoordinatrice = sezioneTerritorialeCoordinatrice;
		this.certificazioni = certificazioni;
		this.benefit = benefit;
		this.telefonoReferente = telefonoReferente;
		this.emailReferente = emailReferente;
		this.foCorsoAnagraficaList = foCorsoAnagraficaList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCapSede() {
		return capSede;
	}

	public void setCapSede(String capSede) {
		this.capSede = capSede;
	}

	public String getCompenso() {
		return compenso;
	}

	public void setCompenso(String compenso) {
		this.compenso = compenso;
	}

	public String getCompetenzeRichieste() {
		return competenzeRichieste;
	}

	public void setCompetenzeRichieste(String competenzeRichieste) {
		this.competenzeRichieste = competenzeRichieste;
	}

	public Integer getCreditiFormativi() {
		return creditiFormativi;
	}

	public void setCreditiFormativi(Integer creditiFormativi) {
		this.creditiFormativi = creditiFormativi;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getDurata() {
		return durata;
	}

	public void setDurata(String durata) {
		this.durata = durata;
	}

	public CaAnagraficaAzienda getCaAnagraficaAzienda() {
		return caAnagraficaAzienda;
	}

	public void setCaAnagraficaAzienda(CaAnagraficaAzienda caAnagraficaAzienda) {
		this.caAnagraficaAzienda = caAnagraficaAzienda;
	}

	public String getFascia() {
		return fascia;
	}

	public void setFascia(String fascia) {
		this.fascia = fascia;
	}

	public String getIndirizzoSede() {
		return indirizzoSede;
	}

	public void setIndirizzoSede(String indirizzoSede) {
		this.indirizzoSede = indirizzoSede;
	}

	public Boolean getInterno() {
		return interno;
	}

	public void setInterno(Boolean interno) {
		this.interno = interno;
	}

	public String getLivelloContratto() {
		return livelloContratto;
	}

	public void setLivelloContratto(String livelloContratto) {
		this.livelloContratto = livelloContratto;
	}

	public Integer getNumeroPosti() {
		return numeroPosti;
	}

	public void setNumeroPosti(Integer numeroPosti) {
		this.numeroPosti = numeroPosti;
	}

	public Boolean getPerAutoimpiego() {
		return perAutoimpiego;
	}

	public void setPerAutoimpiego(Boolean perAutoimpiego) {
		this.perAutoimpiego = perAutoimpiego;
	}

	public Boolean getPerCooperativa() {
		return perCooperativa;
	}

	public void setPerCooperativa(Boolean perCooperativa) {
		this.perCooperativa = perCooperativa;
	}

	public Boolean getPerVolontari() {
		return perVolontari;
	}

	public void setPerVolontari(Boolean perVolontari) {
		this.perVolontari = perVolontari;
	}

	public String getRiferimento() {
		return riferimento;
	}

	public void setRiferimento(String riferimento) {
		this.riferimento = riferimento;
	}

	public String getTipologiaContratto() {
		return tipologiaContratto;
	}

	public void setTipologiaContratto(String tipologiaContratto) {
		this.tipologiaContratto = tipologiaContratto;
	}

	public String getTipoRapportoLavoro() {
		return tipoRapportoLavoro;
	}

	public void setTipoRapportoLavoro(String tipoRapportoLavoro) {
		this.tipoRapportoLavoro = tipoRapportoLavoro;
	}

	public Boolean getTirocinio() {
		return tirocinio;
	}

	public void setTirocinio(Boolean tirocinio) {
		this.tirocinio = tirocinio;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public Integer getTitoloStudioRichiesto() {
		return titoloStudioRichiesto;
	}

	public void setTitoloStudioRichiesto(Integer titoloStudioRichiesto) {
		this.titoloStudioRichiesto = titoloStudioRichiesto;
	}

	public SuComune getLocalitaSedeCorso() {
		return localitaSedeCorso;
	}

	public void setLocalitaSedeCorso(SuComune localitaSedeCorso) {
		this.localitaSedeCorso = localitaSedeCorso;
	}

	public Boolean getAttivo() {
		return attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public FoDocumentoCorso getFoDocumentoCorso() {
		return foDocumentoCorso;
	}

	public void setFoDocumentoCorso(FoDocumentoCorso foDocumentoCorso) {
		this.foDocumentoCorso = foDocumentoCorso;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Date getDataInizioCandidatura() {
		return dataInizioCandidatura;
	}

	public void setDataInizioCandidatura(Date dataInizioCandidatura) {
		this.dataInizioCandidatura = dataInizioCandidatura;
	}

	public Date getDataFineCandidatura() {
		return dataFineCandidatura;
	}

	public void setDataFineCandidatura(Date dataFineCandidatura) {
		this.dataFineCandidatura = dataFineCandidatura;
	}

	public Integer getAnnoFinanziario() {
		return annoFinanziario;
	}

	public void setAnnoFinanziario(Integer annoFinanziario) {
		this.annoFinanziario = annoFinanziario;
	}

	public String getSezioneTerritorialeCoordinatrice() {
		return sezioneTerritorialeCoordinatrice;
	}

	public void setSezioneTerritorialeCoordinatrice(String sezioneTerritorialeCoordinatrice) {
		this.sezioneTerritorialeCoordinatrice = sezioneTerritorialeCoordinatrice;
	}

	public String getCertificazioni() {
		return certificazioni;
	}

	public void setCertificazioni(String certificazioni) {
		this.certificazioni = certificazioni;
	}

	public String getBenefit() {
		return benefit;
	}

	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}

	public String getTelefonoReferente() {
		return telefonoReferente;
	}

	public void setTelefonoReferente(String telefonoReferente) {
		this.telefonoReferente = telefonoReferente;
	}

	public String getEmailReferente() {
		return emailReferente;
	}

	public void setEmailReferente(String emailReferente) {
		this.emailReferente = emailReferente;
	}


	public Set<FoCorsoAnagrafica> getFoCorsoAnagraficaList() {
		return foCorsoAnagraficaList;
	}

	public void setFoCorsoAnagraficaList(Set<FoCorsoAnagrafica> foCorsoAnagraficaList) {
		this.foCorsoAnagraficaList = foCorsoAnagraficaList;
	}
}
