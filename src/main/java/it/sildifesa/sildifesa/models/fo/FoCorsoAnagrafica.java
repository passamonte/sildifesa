package it.sildifesa.sildifesa.models.fo;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;

import javax.persistence.*;

@Entity
@Table(name="fo_corso_anagrafica")
public class FoCorsoAnagrafica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST},  fetch = FetchType.LAZY)
    @JoinColumn(name = "caAnagrafica")
    private CaAnagrafica caAnagrafica;

    //bi-directional many-to-one association to SuTitolostudio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="foCorso")
    private FoCorso foCorso;

    public FoCorsoAnagrafica() {
    }

    public FoCorsoAnagrafica(Long id, CaAnagrafica caAnagrafica) {
        this.id = id;
        this.caAnagrafica = caAnagrafica;
    }

    public FoCorsoAnagrafica(Long id, CaAnagrafica caAnagrafica, FoCorso foCorso) {
        this.id = id;
        this.caAnagrafica = caAnagrafica;
        this.foCorso = foCorso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CaAnagrafica getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagrafica caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public FoCorso getFoCorso() {
        return foCorso;
    }

    public void setFoCorso(FoCorso foCorso) {
        this.foCorso = foCorso;
    }
}
