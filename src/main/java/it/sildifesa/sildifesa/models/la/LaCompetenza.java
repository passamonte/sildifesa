package it.sildifesa.sildifesa.models.la;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.List;

@Entity
@Table(name="la_competenza")
public class LaCompetenza {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String descrizione;

    private Integer livello;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipologiaCompetenza")
    @JsonIgnore
    private LaTipologiaCompetenza tipologiaCompetenza;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Integer getLivello() {
        return livello;
    }

    public void setLivello(Integer livello) {
        this.livello = livello;
    }

    public LaTipologiaCompetenza getTipologiaCompetenza() {
        return tipologiaCompetenza;
    }

    public void setTipologiaCompetenza(LaTipologiaCompetenza tipologiaCompetenza) {
        this.tipologiaCompetenza = tipologiaCompetenza;
    }
}
