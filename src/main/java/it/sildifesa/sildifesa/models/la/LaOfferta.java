package it.sildifesa.sildifesa.models.la;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.lst.LstContratto;
import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import it.sildifesa.sildifesa.models.su.SuComune;
import it.sildifesa.sildifesa.models.su.SuProfessione;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the la_offertasild database table.
 *
 */
@Entity
@Table(name="la_offerta")
public class LaOfferta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer anno;

	private String areaFunzionale;

	private String areaFunzionaleLettera;

	private Boolean chiusa;

	private String compenso;

	private Boolean concorso;

	private Boolean conteggiaRiserva;

	private String cpi;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFine;

	private String descrizione;

	private String descrizioneAttivita;

	private String descrizioneofferta;

	private Boolean diffusione;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "azienda")
	private CaAnagraficaAzienda azienda;

	private String livelloContratto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "localitaSedeOfferta")
	private SuComune localitaSedeOfferta;

	private String competenzeRichieste;

	private String note;

	private Integer numeroPosti;

	private Integer numeroPostiRiservati;

	private String numeroRif;

	private Boolean previstaRiserva;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "professione")
	private SuProfessione professione;

	private String regioneRif;

	private Boolean rilievo;

	private Boolean rilievoMassDir;

	private Double riservaDiLegge;

	private String rispostarilievo;

	private String rispostarilievomassdir;

	private String sede;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "settoreContratto")
	private LstContratto settoreContratto;

	private String tipologiaRapporto;

	private Boolean tipologiaRif;

	private String tipoOffertaLavoro;

	private Integer titoloStudioRichiesto;

	private Boolean attivo;

	private String cccnlApplicato;

	private Integer compensoMensile;

	@ManyToMany(cascade = {CascadeType.MERGE})
	@JoinTable(
			name="la_offerta_lst_tipocontratto"
			, joinColumns={
			@JoinColumn(name="la_offerta_id")
	}
			, inverseJoinColumns={
			@JoinColumn(name="lst_tipocontratto_id")
	}
	)
	private Set<LstTipoContratto> laOffertaTipoContratti;

	public LaOfferta() {
	}

	public LaOfferta(Long id, Integer anno, String areaFunzionale, String areaFunzionaleLettera, Boolean chiusa, String compenso, Boolean concorso, Boolean conteggiaRiserva, String cpi, Date data, Date dataFine, String descrizione, String descrizioneAttivita, String descrizioneofferta, Boolean diffusione, CaAnagraficaAzienda azienda, String livelloContratto, SuComune localitaSedeOfferta, String competenzeRichieste, String note, Integer numeroPosti, Integer numeroPostiRiservati, String numeroRif, Boolean previstaRiserva, SuProfessione professione, String regioneRif, Boolean rilievo, Boolean rilievoMassDir, Double riservaDiLegge, String rispostarilievo, String rispostarilievomassdir, String sede, LstContratto settoreContratto, String tipologiaRapporto, Boolean tipologiaRif, String tipoOffertaLavoro, Integer titoloStudioRichiesto, Boolean attivo) {
		this.id = id;
		this.anno = anno;
		this.areaFunzionale = areaFunzionale;
		this.areaFunzionaleLettera = areaFunzionaleLettera;
		this.chiusa = chiusa;
		this.compenso = compenso;
		this.concorso = concorso;
		this.conteggiaRiserva = conteggiaRiserva;
		this.cpi = cpi;
		this.data = data;
		this.dataFine = dataFine;
		this.descrizione = descrizione;
		this.descrizioneAttivita = descrizioneAttivita;
		this.descrizioneofferta = descrizioneofferta;
		this.diffusione = diffusione;
		this.azienda = azienda;
		this.livelloContratto = livelloContratto;
		this.localitaSedeOfferta = localitaSedeOfferta;
		this.competenzeRichieste = competenzeRichieste;
		this.note = note;
		this.numeroPosti = numeroPosti;
		this.numeroPostiRiservati = numeroPostiRiservati;
		this.numeroRif = numeroRif;
		this.previstaRiserva = previstaRiserva;
		this.professione = professione;
		this.regioneRif = regioneRif;
		this.rilievo = rilievo;
		this.rilievoMassDir = rilievoMassDir;
		this.riservaDiLegge = riservaDiLegge;
		this.rispostarilievo = rispostarilievo;
		this.rispostarilievomassdir = rispostarilievomassdir;
		this.sede = sede;
		this.settoreContratto = settoreContratto;
		this.tipologiaRapporto = tipologiaRapporto;
		this.tipologiaRif = tipologiaRif;
		this.tipoOffertaLavoro = tipoOffertaLavoro;
		this.titoloStudioRichiesto = titoloStudioRichiesto;
		this.attivo = attivo;
	}

	public LaOfferta(Long id, Integer anno, String areaFunzionale, String areaFunzionaleLettera, Boolean chiusa, String compenso, Boolean concorso, Boolean conteggiaRiserva, String cpi, Date data, Date dataFine, String descrizione, String descrizioneAttivita, String descrizioneofferta, Boolean diffusione, CaAnagraficaAzienda azienda, String livelloContratto, SuComune localitaSedeOfferta, String competenzeRichieste, String note, Integer numeroPosti, Integer numeroPostiRiservati, String numeroRif, Boolean previstaRiserva, SuProfessione professione, String regioneRif, Boolean rilievo, Boolean rilievoMassDir, Double riservaDiLegge, String rispostarilievo, String rispostarilievomassdir, String sede, LstContratto settoreContratto, String tipologiaRapporto, Boolean tipologiaRif, String tipoOffertaLavoro, Integer titoloStudioRichiesto, Boolean attivo, String cccnlApplicato, Integer compensoMensile, Set<LstTipoContratto> laOffertaTipoContratti) {
		this.id = id;
		this.anno = anno;
		this.areaFunzionale = areaFunzionale;
		this.areaFunzionaleLettera = areaFunzionaleLettera;
		this.chiusa = chiusa;
		this.compenso = compenso;
		this.concorso = concorso;
		this.conteggiaRiserva = conteggiaRiserva;
		this.cpi = cpi;
		this.data = data;
		this.dataFine = dataFine;
		this.descrizione = descrizione;
		this.descrizioneAttivita = descrizioneAttivita;
		this.descrizioneofferta = descrizioneofferta;
		this.diffusione = diffusione;
		this.azienda = azienda;
		this.livelloContratto = livelloContratto;
		this.localitaSedeOfferta = localitaSedeOfferta;
		this.competenzeRichieste = competenzeRichieste;
		this.note = note;
		this.numeroPosti = numeroPosti;
		this.numeroPostiRiservati = numeroPostiRiservati;
		this.numeroRif = numeroRif;
		this.previstaRiserva = previstaRiserva;
		this.professione = professione;
		this.regioneRif = regioneRif;
		this.rilievo = rilievo;
		this.rilievoMassDir = rilievoMassDir;
		this.riservaDiLegge = riservaDiLegge;
		this.rispostarilievo = rispostarilievo;
		this.rispostarilievomassdir = rispostarilievomassdir;
		this.sede = sede;
		this.settoreContratto = settoreContratto;
		this.tipologiaRapporto = tipologiaRapporto;
		this.tipologiaRif = tipologiaRif;
		this.tipoOffertaLavoro = tipoOffertaLavoro;
		this.titoloStudioRichiesto = titoloStudioRichiesto;
		this.attivo = attivo;
		this.cccnlApplicato = cccnlApplicato;
		this.compensoMensile = compensoMensile;
		this.laOffertaTipoContratti = laOffertaTipoContratti;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAnno() {
		return anno;
	}

	public void setAnno(Integer anno) {
		this.anno = anno;
	}

	public String getAreaFunzionale() {
		return areaFunzionale;
	}

	public void setAreaFunzionale(String areaFunzionale) {
		this.areaFunzionale = areaFunzionale;
	}

	public String getAreaFunzionaleLettera() {
		return areaFunzionaleLettera;
	}

	public void setAreaFunzionaleLettera(String areaFunzionaleLettera) {
		this.areaFunzionaleLettera = areaFunzionaleLettera;
	}

	public Boolean getChiusa() {
		return chiusa;
	}

	public void setChiusa(Boolean chiusa) {
		this.chiusa = chiusa;
	}

	public String getCompenso() {
		return compenso;
	}

	public void setCompenso(String compenso) {
		this.compenso = compenso;
	}

	public Boolean getConcorso() {
		return concorso;
	}

	public void setConcorso(Boolean concorso) {
		this.concorso = concorso;
	}

	public Boolean getConteggiaRiserva() {
		return conteggiaRiserva;
	}

	public void setConteggiaRiserva(Boolean conteggiaRiserva) {
		this.conteggiaRiserva = conteggiaRiserva;
	}

	public String getCpi() {
		return cpi;
	}

	public void setCpi(String cpi) {
		this.cpi = cpi;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getDescrizioneAttivita() {
		return descrizioneAttivita;
	}

	public void setDescrizioneAttivita(String descrizioneAttivita) {
		this.descrizioneAttivita = descrizioneAttivita;
	}

	public String getDescrizioneofferta() {
		return descrizioneofferta;
	}

	public void setDescrizioneofferta(String descrizioneofferta) {
		this.descrizioneofferta = descrizioneofferta;
	}

	public Boolean getDiffusione() {
		return diffusione;
	}

	public void setDiffusione(Boolean diffusione) {
		this.diffusione = diffusione;
	}

	public CaAnagraficaAzienda getAzienda() {
		return azienda;
	}

	public void setAzienda(CaAnagraficaAzienda azienda) {
		this.azienda = azienda;
	}

	public String getLivelloContratto() {
		return livelloContratto;
	}

	public void setLivelloContratto(String livelloContratto) {
		this.livelloContratto = livelloContratto;
	}

	public SuComune getLocalitaSedeOfferta() {
		return localitaSedeOfferta;
	}

	public void setLocalitaSedeOfferta(SuComune localitaSedeOfferta) {
		this.localitaSedeOfferta = localitaSedeOfferta;
	}

	public String getCompetenzeRichieste() {
		return competenzeRichieste;
	}

	public void setCompetenzeRichieste(String competenzeRichieste) {
		this.competenzeRichieste = competenzeRichieste;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getNumeroPosti() {
		return numeroPosti;
	}

	public void setNumeroPosti(Integer numeroPosti) {
		this.numeroPosti = numeroPosti;
	}

	public Integer getNumeroPostiRiservati() {
		return numeroPostiRiservati;
	}

	public void setNumeroPostiRiservati(Integer numeroPostiRiservati) {
		this.numeroPostiRiservati = numeroPostiRiservati;
	}

	public String getNumeroRif() {
		return numeroRif;
	}

	public void setNumeroRif(String numeroRif) {
		this.numeroRif = numeroRif;
	}

	public Boolean getPrevistaRiserva() {
		return previstaRiserva;
	}

	public void setPrevistaRiserva(Boolean previstaRiserva) {
		this.previstaRiserva = previstaRiserva;
	}

	public SuProfessione getProfessione() {
		return professione;
	}

	public void setProfessione(SuProfessione professione) {
		this.professione = professione;
	}

	public String getRegioneRif() {
		return regioneRif;
	}

	public void setRegioneRif(String regioneRif) {
		this.regioneRif = regioneRif;
	}

	public Boolean getRilievo() {
		return rilievo;
	}

	public void setRilievo(Boolean rilievo) {
		this.rilievo = rilievo;
	}

	public Boolean getRilievoMassDir() {
		return rilievoMassDir;
	}

	public void setRilievoMassDir(Boolean rilievoMassDir) {
		this.rilievoMassDir = rilievoMassDir;
	}

	public Double getRiservaDiLegge() {
		return riservaDiLegge;
	}

	public void setRiservaDiLegge(Double riservaDiLegge) {
		this.riservaDiLegge = riservaDiLegge;
	}

	public String getRispostarilievo() {
		return rispostarilievo;
	}

	public void setRispostarilievo(String rispostarilievo) {
		this.rispostarilievo = rispostarilievo;
	}

	public String getRispostarilievomassdir() {
		return rispostarilievomassdir;
	}

	public void setRispostarilievomassdir(String rispostarilievomassdir) {
		this.rispostarilievomassdir = rispostarilievomassdir;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public LstContratto getSettoreContratto() {
		return settoreContratto;
	}

	public void setSettoreContratto(LstContratto settoreContratto) {
		this.settoreContratto = settoreContratto;
	}

	public String getTipologiaRapporto() {
		return tipologiaRapporto;
	}

	public void setTipologiaRapporto(String tipologiaRapporto) {
		this.tipologiaRapporto = tipologiaRapporto;
	}

	public Boolean getTipologiaRif() {
		return tipologiaRif;
	}

	public void setTipologiaRif(Boolean tipologiaRif) {
		this.tipologiaRif = tipologiaRif;
	}

	public String getTipoOffertaLavoro() {
		return tipoOffertaLavoro;
	}

	public void setTipoOffertaLavoro(String tipoOffertaLavoro) {
		this.tipoOffertaLavoro = tipoOffertaLavoro;
	}

	public Integer getTitoloStudioRichiesto() {
		return titoloStudioRichiesto;
	}

	public void setTitoloStudioRichiesto(Integer titoloStudioRichiesto) {
		this.titoloStudioRichiesto = titoloStudioRichiesto;
	}

	public Boolean getAttivo() {
		return attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public String getCccnlApplicato() {
		return cccnlApplicato;
	}

	public void setCccnlApplicato(String cccnlApplicato) {
		this.cccnlApplicato = cccnlApplicato;
	}

	public Integer getCompensoMensile() {
		return compensoMensile;
	}

	public void setCompensoMensile(Integer compensoMensile) {
		this.compensoMensile = compensoMensile;
	}

	public Set<LstTipoContratto> getLaOffertaTipoContratti() {
		return laOffertaTipoContratti;
	}

	public void setLaOffertaTipoContratti(Set<LstTipoContratto> laOffertaTipoContratti) {
		this.laOffertaTipoContratti = laOffertaTipoContratti;
	}
}
