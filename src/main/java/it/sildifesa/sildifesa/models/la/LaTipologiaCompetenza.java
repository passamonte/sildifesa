package it.sildifesa.sildifesa.models.la;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.List;

@Entity
@Table(name="la_tipologiacompetenza")
public class LaTipologiaCompetenza {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String tipologia;

    private String dettaglio;

    @OneToMany(mappedBy="tipologiaCompetenza", cascade = CascadeType.MERGE)
    @JsonIgnore
    private List<LaCompetenza> competenze;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public String getDettaglio() {
        return dettaglio;
    }

    public void setDettaglio(String dettaglio) {
        this.dettaglio = dettaglio;
    }

    public List<LaCompetenza> getCompetenze() {
        return competenze;
    }

    public void setCompetenze(List<LaCompetenza> competenze) {
        this.competenze = competenze;
    }
}
