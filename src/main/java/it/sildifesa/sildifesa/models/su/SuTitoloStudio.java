package it.sildifesa.sildifesa.models.su;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="su_titolostudio")
public class SuTitoloStudio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String codice;


	private String DEN_ab;


	private String denominazione;


	private Integer livello;


	private String nome;


	private String sinonimiTitoloStudio;


	private String specializzazione;

	private String tipo;


	private String tipoScuolaIstituto;


	private String TSI_ab;


	//bi-directional many-to-one association to CaAnagraficaTitolostudio
	@JsonIgnore
	@OneToMany(mappedBy = "suTitoloStudio")
	private List<CaAnagraficaTitoloDiStudio> caAnagraficaTitolostudios;

	public SuTitoloStudio() {
	}

	public SuTitoloStudio(Long id, String codice, String DEN_ab, String denominazione, Integer livello, String nome, String sinonimiTitoloStudio, String specializzazione, String tipo, String tipoScuolaIstituto, String TSI_ab) {
		this.id = id;
		this.codice = codice;
		this.DEN_ab = DEN_ab;
		this.denominazione = denominazione;
		this.livello = livello;
		this.nome = nome;
		this.sinonimiTitoloStudio = sinonimiTitoloStudio;
		this.specializzazione = specializzazione;
		this.tipo = tipo;
		this.tipoScuolaIstituto = tipoScuolaIstituto;
		this.TSI_ab = TSI_ab;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDEN_ab() {
		return DEN_ab;
	}

	public void setDEN_ab(String DEN_ab) {
		this.DEN_ab = DEN_ab;
	}

	public String getDenominazione() {
		return denominazione;
	}

	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	public Integer getLivello() {
		return livello;
	}

	public void setLivello(Integer livello) {
		this.livello = livello;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSinonimiTitoloStudio() {
		return sinonimiTitoloStudio;
	}

	public void setSinonimiTitoloStudio(String sinonimiTitoloStudio) {
		this.sinonimiTitoloStudio = sinonimiTitoloStudio;
	}

	public String getSpecializzazione() {
		return specializzazione;
	}

	public void setSpecializzazione(String specializzazione) {
		this.specializzazione = specializzazione;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoScuolaIstituto() {
		return tipoScuolaIstituto;
	}

	public void setTipoScuolaIstituto(String tipoScuolaIstituto) {
		this.tipoScuolaIstituto = tipoScuolaIstituto;
	}

	public String getTSI_ab() {
		return TSI_ab;
	}

	public void setTSI_ab(String TSI_ab) {
		this.TSI_ab = TSI_ab;
	}

	public List<CaAnagraficaTitoloDiStudio> getCaAnagraficaTitolostudios() {
		return caAnagraficaTitolostudios;
	}

	public void setCaAnagraficaTitolostudios(List<CaAnagraficaTitoloDiStudio> caAnagraficaTitolostudios) {
		this.caAnagraficaTitolostudios = caAnagraficaTitolostudios;
	}
}
