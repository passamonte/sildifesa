package it.sildifesa.sildifesa.models.su;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.controller.CaAnagraficaController;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;


/**
 * The persistent class for the su_comune database table.
 * 
 */
@Entity
@Table(name="su_comune")
public class SuComune {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@NotNull(message = "Comune obbligatorio")
	private Long id;

	private String cap;


	private String codiceCatastale;


	private String codiceFisco;


	private String codiceIstat;


	private String codiceRegioneBNL;


	private String nazione;


	private String nome;


	private String prefisso;


	private String provincia;


	private String provinciaEstesa;


	private String regione;


	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="suComune1")
	@JsonIgnore
	private List<CaAnagrafica> caAnagraficas1;

	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="suComune2")
	@JsonIgnore
	private List<CaAnagrafica> caAnagraficas2;


	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="localitaSedeOfferta")
	@JsonIgnore
	private List<LaOfferta> laOfferte;

	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="localitaSedeCorso")
	@JsonIgnore
	private List<FoCorso> foCorsi;

	public SuComune() {
	}

	public SuComune(Long id, String cap, String codiceCatastale, String codiceFisco, String codiceIstat, String codiceRegioneBNL, String nazione, String nome, String prefisso, String provincia, String provinciaEstesa, String regione) {
		this.id = id;
		this.cap = cap;
		this.codiceCatastale = codiceCatastale;
		this.codiceFisco = codiceFisco;
		this.codiceIstat = codiceIstat;
		this.codiceRegioneBNL = codiceRegioneBNL;
		this.nazione = nazione;
		this.nome = nome;
		this.prefisso = prefisso;
		this.provincia = provincia;
		this.provinciaEstesa = provinciaEstesa;
		this.regione = regione;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getCodiceCatastale() {
		return codiceCatastale;
	}

	public void setCodiceCatastale(String codiceCatastale) {
		this.codiceCatastale = codiceCatastale;
	}

	public String getCodiceFisco() {
		return codiceFisco;
	}

	public void setCodiceFisco(String codiceFisco) {
		this.codiceFisco = codiceFisco;
	}

	public String getCodiceIstat() {
		return codiceIstat;
	}

	public void setCodiceIstat(String codiceIstat) {
		this.codiceIstat = codiceIstat;
	}

	public String getCodiceRegioneBNL() {
		return codiceRegioneBNL;
	}

	public void setCodiceRegioneBNL(String codiceRegioneBNL) {
		this.codiceRegioneBNL = codiceRegioneBNL;
	}

	public String getNazione() {
		return nazione;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPrefisso() {
		return prefisso;
	}

	public void setPrefisso(String prefisso) {
		this.prefisso = prefisso;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getProvinciaEstesa() {
		return provinciaEstesa;
	}

	public void setProvinciaEstesa(String provinciaEstesa) {
		this.provinciaEstesa = provinciaEstesa;
	}

	public String getRegione() {
		return regione;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public List<CaAnagrafica> getCaAnagraficas1() {
		return caAnagraficas1;
	}

	public void setCaAnagraficas1(List<CaAnagrafica> caAnagraficas1) {
		this.caAnagraficas1 = caAnagraficas1;
	}

	public CaAnagrafica addCaAnagraficas1(CaAnagrafica caAnagraficas1) {
		getCaAnagraficas1().add(caAnagraficas1);
		caAnagraficas1.setSuComune1(this);

		return caAnagraficas1;
	}

	public CaAnagrafica removeCaAnagraficas1(CaAnagrafica caAnagraficas1) {
		getCaAnagraficas1().remove(caAnagraficas1);
		caAnagraficas1.setSuComune1(null);

		return caAnagraficas1;
	}

	public List<CaAnagrafica> getCaAnagraficas2() {
		return this.caAnagraficas2;
	}

	public void setCaAnagraficas2(List<CaAnagrafica> caAnagraficas2) {
		this.caAnagraficas2 = caAnagraficas2;
	}

	public CaAnagrafica addCaAnagraficas2(CaAnagrafica caAnagraficas2) {
		getCaAnagraficas2().add(caAnagraficas2);
		caAnagraficas2.setSuComune2(this);

		return caAnagraficas2;
	}

	public CaAnagrafica removeCaAnagraficas2(CaAnagrafica caAnagraficas2) {
		getCaAnagraficas2().remove(caAnagraficas2);
		caAnagraficas2.setSuComune2(null);

		return caAnagraficas2;
	}



}
