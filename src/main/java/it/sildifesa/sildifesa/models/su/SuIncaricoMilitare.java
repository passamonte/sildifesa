package it.sildifesa.sildifesa.models.su;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the su_incaricomilitare database table.
 * 
 */
@Entity
@Table(name="su_incaricomilitare")
public class SuIncaricoMilitare implements Serializable {

	@Id
	private Long id;

	private String codice;


	private String descrizioneBreve;


	private String forzaArmata;


	private String nome;

	private BigInteger transcodificaProfessione;


	private BigInteger transcodificaSettore;


	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="suIncaricomilitare")
	private List<CaAnagrafica> caAnagraficas;

	public SuIncaricoMilitare() {
	}

	public SuIncaricoMilitare(Long id, String codice, String descrizioneBreve, String forzaArmata, String nome, BigInteger transcodificaProfessione, BigInteger transcodificaSettore) {
		this.id = id;
		this.codice = codice;
		this.descrizioneBreve = descrizioneBreve;
		this.forzaArmata = forzaArmata;
		this.nome = nome;
		this.transcodificaProfessione = transcodificaProfessione;
		this.transcodificaSettore = transcodificaSettore;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDescrizioneBreve() {
		return descrizioneBreve;
	}

	public void setDescrizioneBreve(String descrizioneBreve) {
		this.descrizioneBreve = descrizioneBreve;
	}

	public String getForzaArmata() {
		return forzaArmata;
	}

	public void setForzaArmata(String forzaArmata) {
		this.forzaArmata = forzaArmata;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigInteger getTranscodificaProfessione() {
		return transcodificaProfessione;
	}

	public void setTranscodificaProfessione(BigInteger transcodificaProfessione) {
		this.transcodificaProfessione = transcodificaProfessione;
	}

	public BigInteger getTranscodificaSettore() {
		return transcodificaSettore;
	}

	public void setTranscodificaSettore(BigInteger transcodificaSettore) {
		this.transcodificaSettore = transcodificaSettore;
	}

	public List<CaAnagrafica> getCaAnagraficas() {
		return this.caAnagraficas;
	}

	public void setCaAnagraficas(List<CaAnagrafica> caAnagraficas) {
		this.caAnagraficas = caAnagraficas;
	}

	public CaAnagrafica addCaAnagrafica(CaAnagrafica caAnagrafica) {
		getCaAnagraficas().add(caAnagrafica);
		caAnagrafica.setSuIncaricomilitare(this);

		return caAnagrafica;
	}

	public CaAnagrafica removeCaAnagrafica(CaAnagrafica caAnagrafica) {
		getCaAnagraficas().remove(caAnagrafica);
		caAnagrafica.setSuIncaricomilitare(null);

		return caAnagrafica;
	}

}
