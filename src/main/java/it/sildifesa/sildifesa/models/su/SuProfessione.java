package it.sildifesa.sildifesa.models.su;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.la.LaOfferta;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name="su_professione")
public class SuProfessione {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String codice;

	private String nome;

	private BigInteger parent;

	@ManyToMany(mappedBy = "suProfessioni")
	@JsonIgnore
	private List<CaAnagrafica> caAnagrafiche;

	@OneToMany(mappedBy="professione")
	@JsonIgnore
	private List<LaOfferta> offerte;

	public SuProfessione() {
	}

	public SuProfessione(Long id, String codice, String nome, BigInteger parent, List<CaAnagrafica> caAnagrafiche, List<LaOfferta> offerte) {
		this.id = id;
		this.codice = codice;
		this.nome = nome;
		this.parent = parent;
		this.caAnagrafiche = caAnagrafiche;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigInteger getParent() {
		return parent;
	}

	public void setParent(BigInteger parent) {
		this.parent = parent;
	}

	public List<CaAnagrafica> getCaAnagrafiche() {
		return caAnagrafiche;
	}

	public void setCaAnagrafiche(List<CaAnagrafica> caAnagrafiche) {
		this.caAnagrafiche = caAnagrafiche;
	}

	public List<LaOfferta> getOfferte() {
		return offerte;
	}

	public void setOfferte(List<LaOfferta> offerte) {
		this.offerte = offerte;
	}
}
