package it.sildifesa.sildifesa.models.su;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * The persistent class for the su_specialita database table.
 * 
 */
@Entity
@Table(name="su_specialita")
public class SuSpecialita  {

	@Id
	private Long id;

	private String forzaArmata;

	@NotNull(message = "Specialita obbligatoria")
	private String nome;


	//bi-directional many-to-one association to CaAnagrafica
	@OneToMany(mappedBy="suSpecialita")
	private List<CaAnagrafica> caAnagraficas;

	public SuSpecialita() {
	}

	public SuSpecialita(Long id, String forzaArmata, @NotNull(message = "Specialita obbligatoria") String nome) {
		this.id = id;
		this.forzaArmata = forzaArmata;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getForzaArmata() {
		return forzaArmata;
	}

	public void setForzaArmata(String forzaArmata) {
		this.forzaArmata = forzaArmata;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<CaAnagrafica> getCaAnagraficas() {
		return caAnagraficas;
	}

	public void setCaAnagraficas(List<CaAnagrafica> caAnagraficas) {
		this.caAnagraficas = caAnagraficas;
	}

	public CaAnagrafica addCaAnagrafica(CaAnagrafica caAnagrafica) {
		getCaAnagraficas().add(caAnagrafica);
		caAnagrafica.setSuSpecialita(this);

		return caAnagrafica;
	}

	public CaAnagrafica removeCaAnagrafica(CaAnagrafica caAnagrafica) {
		getCaAnagraficas().remove(caAnagrafica);
		caAnagrafica.setSuSpecialita(null);

		return caAnagrafica;
	}

}
