package it.sildifesa.sildifesa.models.ca;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "ca_documentoazienda")
public class CaDocumentoAzienda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Lob
    private byte[] documentoAziendaFile;

    private String nomeDocumentoAziendaFile;

    private String estensioneDocumentoIdentitaFile;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataIscrizioneCameraCommercio;

    @OneToOne(mappedBy = "caDocumentoAzienda")
    private CaAnagraficaAzienda caAnagraficaAzienda;


    public CaDocumentoAzienda() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getDocumentoAziendaFile() {
        return documentoAziendaFile;
    }

    public void setDocumentoAziendaFile(byte[] documentoAziendaFile) {
        this.documentoAziendaFile = documentoAziendaFile;
    }

    public String getNomeDocumentoAziendaFile() {
        return nomeDocumentoAziendaFile;
    }

    public void setNomeDocumentoAziendaFile(String nomeDocumentoAziendaFile) {
        this.nomeDocumentoAziendaFile = nomeDocumentoAziendaFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public Date getDataIscrizioneCameraCommercio() {
        return dataIscrizioneCameraCommercio;
    }

    public void setDataIscrizioneCameraCommercio(Date dataIscrizioneCameraCommercio) {
        this.dataIscrizioneCameraCommercio = dataIscrizioneCameraCommercio;
    }
}
