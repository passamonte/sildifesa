package it.sildifesa.sildifesa.models.ca;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import it.sildifesa.sildifesa.models.su.SuComune;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ca_anagraficaazienda")
public class CaAnagraficaAzienda {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Ragione Sociale obbligatoria")
    private String ragioneSociale;

    @Length(max = 5)
    @NotNull(message = "Cap comune residenza obbligatorio")
    private String capSedeLegale;


    @NotEmpty(message = "P. IVA / C.F. Obbligatorio")
    private String partitaIva;


    @NotEmpty(message = " Attenzione il campo e-mail è obbligatorio")
    @Email(message = "Attenzione inserire una e-mail valida")
    private String email;


    @NotEmpty(message = " Attenzione il campo PEC è obbligatorio")
    @Email(message = "Attenzione inserire una PEC valida")
    private String emailPec;

    @NotEmpty(message = " Attenzione il campo Indirizzo Sede Legale è obbligatorio")
    private String indirizzoSedeLegale;


    @NotNull(message = "Numero di telefono obbligatorio")
    private String numeroTelefono;


    private String fax;

    private String descrizioneAttivita;

    private String formaGiuridica;

    private String sitoWeb;

    private String numeroRea;

    private String statoAttivita;

    private Boolean attivo;

    @NotEmpty(message = "Attenzione il Nome del Referente è obbligatorio")
    private String nomeReferente;


    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAttoDiCostituzione;


    //bi-directional many-to-one association to SuComune
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comuneSedeLegale")
    @JsonIgnore
    @Valid
    private SuComune comuneSedeLegale;

    //bi-directional many-to-one association to TipoAzienda
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipologiaAzienda")
    @JsonIgnore
    @Valid
    private CaTipoAzienda tipologiaAzienda;


    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "documentoazienda_id", referencedColumnName = "id")
    private CaDocumentoAzienda caDocumentoAzienda;

    @OneToMany(mappedBy = "caAnagraficaAzienda",fetch = FetchType.LAZY, orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<FoCorso> corsi;

    @OneToMany(mappedBy="azienda", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    @JsonIgnore
    private List<LaOfferta> offerte;

    public CaAnagraficaAzienda() {
    }

    public CaAnagraficaAzienda(String ragioneSociale, String capSedeLegale, String partitaIva, String email, String emailPec, String indirizzoSedeLegale, String numeroTelefono, String fax, String descrizioneAttivita, String formaGiuridica, String sitoWeb, String numeroRea, String statoAttivita, Boolean attivo, String nomeReferente, Date dataAttoDiCostituzione, SuComune comuneSedeLegale, CaTipoAzienda tipologiaAzienda, CaDocumentoAzienda caDocumentoAzienda, List<FoCorso> corsi) {
        this.ragioneSociale = ragioneSociale;
        this.capSedeLegale = capSedeLegale;
        this.partitaIva = partitaIva;
        this.email = email;
        this.emailPec = emailPec;
        this.indirizzoSedeLegale = indirizzoSedeLegale;
        this.numeroTelefono = numeroTelefono;
        this.fax = fax;
        this.descrizioneAttivita = descrizioneAttivita;
        this.formaGiuridica = formaGiuridica;
        this.sitoWeb = sitoWeb;
        this.numeroRea = numeroRea;
        this.statoAttivita = statoAttivita;
        this.attivo = attivo;
        this.nomeReferente = nomeReferente;
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
        this.comuneSedeLegale = comuneSedeLegale;
        this.tipologiaAzienda = tipologiaAzienda;
        this.caDocumentoAzienda = caDocumentoAzienda;
        this.corsi = corsi;
    }

    public CaAnagraficaAzienda(Long id, String ragioneSociale, String capSedeLegale, String partitaIva, String email, String emailPec, String indirizzoSedeLegale, String numeroTelefono, String fax, String descrizioneAttivita, String formaGiuridica, String sitoWeb, String numeroRea, String statoAttivita, Boolean attivo, String nomeReferente, Date dataAttoDiCostituzione) {
        this.id = id;
        this.ragioneSociale = ragioneSociale;
        this.capSedeLegale = capSedeLegale;
        this.partitaIva = partitaIva;
        this.email = email;
        this.emailPec = emailPec;
        this.indirizzoSedeLegale = indirizzoSedeLegale;
        this.numeroTelefono = numeroTelefono;
        this.fax = fax;
        this.descrizioneAttivita = descrizioneAttivita;
        this.formaGiuridica = formaGiuridica;
        this.sitoWeb = sitoWeb;
        this.numeroRea = numeroRea;
        this.statoAttivita = statoAttivita;
        this.attivo = attivo;
        this.nomeReferente = nomeReferente;
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
    }

    public List<LaOfferta> getOfferte() {
        return offerte;
    }

    public void setOfferte(List<LaOfferta> offerte) {
        this.offerte = offerte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getCapSedeLegale() {
        return capSedeLegale;
    }

    public void setCapSedeLegale(String capSedeLegale) {
        this.capSedeLegale = capSedeLegale;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailPec() {
        return emailPec;
    }

    public void setEmailPec(String emailPec) {
        this.emailPec = emailPec;
    }

    public String getIndirizzoSedeLegale() {
        return indirizzoSedeLegale;
    }

    public void setIndirizzoSedeLegale(String indirizzoSedeLegale) {
        this.indirizzoSedeLegale = indirizzoSedeLegale;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getDescrizioneAttivita() {
        return descrizioneAttivita;
    }

    public void setDescrizioneAttivita(String descrizioneAttivita) {
        this.descrizioneAttivita = descrizioneAttivita;
    }

    public String getFormaGiuridica() {
        return formaGiuridica;
    }

    public void setFormaGiuridica(String formaGiuridica) {
        this.formaGiuridica = formaGiuridica;
    }

    public String getSitoWeb() {
        return sitoWeb;
    }

    public void setSitoWeb(String sitoWeb) {
        this.sitoWeb = sitoWeb;
    }

    public String getNumeroRea() {
        return numeroRea;
    }

    public void setNumeroRea(String numeroRea) {
        this.numeroRea = numeroRea;
    }

    public String getStatoAttivita() {
        return statoAttivita;
    }

    public void setStatoAttivita(String statoAttivita) {
        this.statoAttivita = statoAttivita;
    }

    public String getNomeReferente() {
        return nomeReferente;
    }

    public void setNomeReferente(String nomeReferente) {
        this.nomeReferente = nomeReferente;
    }

    public Date getDataAttoDiCostituzione() {
        return dataAttoDiCostituzione;
    }

    public void setDataAttoDiCostituzione(Date dataAttoDiCostituzione) {
        this.dataAttoDiCostituzione = dataAttoDiCostituzione;
    }

    public SuComune getComuneSedeLegale() {
        return comuneSedeLegale;
    }

    public void setComuneSedeLegale(SuComune comuneSedeLegale) {
        this.comuneSedeLegale = comuneSedeLegale;
    }

    public CaTipoAzienda getTipologiaAzienda() {
        return tipologiaAzienda;
    }

    public void setTipologiaAzienda(CaTipoAzienda tipologiaAzienda) {
        this.tipologiaAzienda = tipologiaAzienda;
    }

    public CaDocumentoAzienda getCaDocumentoAzienda() {
        return caDocumentoAzienda;
    }

    public void setCaDocumentoAzienda(CaDocumentoAzienda caDocumentoAzienda) {
        this.caDocumentoAzienda = caDocumentoAzienda;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public List<FoCorso> getCorsi() {
        return corsi;
    }

    public void setCorsi(List<FoCorso> corsi) {
        this.corsi = corsi;
    }
}
