package it.sildifesa.sildifesa.models.ca;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "ca_tipoazienda")
public class CaTipoAzienda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull(message = "Forma Giuridica obbligatoria")
    private Long id;

    private String nomeTipologia;

    private String nomeTipologiaBreve;

    //bi-directional many-to-one association to CaAnagrafica
    @OneToMany(mappedBy="comuneSedeLegale")
    @JsonIgnore
    private List<CaAnagraficaAzienda> azienda;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeTipologia() {
        return nomeTipologia;
    }

    public void setNomeTipologia(String nomeTipologia) {
        this.nomeTipologia = nomeTipologia;
    }

    public List<CaAnagraficaAzienda> getAzienda() {
        return azienda;
    }

    public void setAzienda(List<CaAnagraficaAzienda> azienda) {
        this.azienda = azienda;
    }

    public String getNomeTipologiaBreve() {
        return nomeTipologiaBreve;
    }

    public void setNomeTipologiaBreve(String nomeTipologiaBreve) {
        this.nomeTipologiaBreve = nomeTipologiaBreve;
    }
}
