package it.sildifesa.sildifesa.models.ca;

import it.sildifesa.sildifesa.models.su.SuTitoloStudio;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ca_anagrafica_titolostudio")
public class CaAnagraficaTitoloDiStudio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Anno titolo di studio obbligatorio")
    private Integer annoConseguimento;

    @NotNull(message = "Istituto conseguimento è obbligatorio")
    private String istitutoConseguimento;

    //bi-directional many-to-one association to CaAnagrafica
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caAnagrafica")
    private CaAnagrafica caAnagrafica;

    //bi-directional many-to-one association to SuTitolostudio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="suTitoloDiStudio")
    private SuTitoloStudio suTitoloStudio;

    public CaAnagraficaTitoloDiStudio() {
    }

    public CaAnagraficaTitoloDiStudio(Long id, Integer annoConseguimento, String istitutoConseguimento, SuTitoloStudio suTitoloStudio) {
        this.id = id;
        this.annoConseguimento = annoConseguimento;
        this.istitutoConseguimento = istitutoConseguimento;
        this.suTitoloStudio = suTitoloStudio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnnoConseguimento() {
        return annoConseguimento;
    }

    public void setAnnoConseguimento(Integer annoConseguimento) {
        this.annoConseguimento = annoConseguimento;
    }

    public String getIstitutoConseguimento() {
        return istitutoConseguimento;
    }

    public void setIstitutoConseguimento(String istitutoConseguimento) {
        this.istitutoConseguimento = istitutoConseguimento;
    }

    public CaAnagrafica getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagrafica caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public SuTitoloStudio getSuTitoloStudio() {
        return suTitoloStudio;
    }

    public void setSuTitoloStudio(SuTitoloStudio suTitoloStudio) {
        this.suTitoloStudio = suTitoloStudio;
    }
}
