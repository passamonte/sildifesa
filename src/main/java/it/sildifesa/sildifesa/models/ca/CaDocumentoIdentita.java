package it.sildifesa.sildifesa.models.ca;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "ca_documentoidentita")
public class CaDocumentoIdentita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull(message = "numero documento d'identità obbligatorio")
    private String numDocumentoIdentita;

    @Lob
    private byte[] documentoIdentitaFile;

    private String nomeDocumentoIdentitaFile;

    private String estensioneDocumentoIdentitaFile;

    @OneToOne(mappedBy = "caDocumentoIdentita")
    private CaAnagrafica caAnagrafica;

    @ManyToOne
    @JoinColumn(name = "tipoDocumento_id", referencedColumnName = "id")
    @Valid
    private CaTipoDocumentoIdentita caTipoDocumentoIdentita;

    public CaDocumentoIdentita() {
    }

    public CaDocumentoIdentita(Long id, String numDocumentoIdentita, byte[] documentoIdentitaFile, String nomeDocumentoIdentitaFile, String estensioneDocumentoIdentitaFile, CaTipoDocumentoIdentita caTipoDocumentoIdentita) {
        this.id = id;
        this.numDocumentoIdentita = numDocumentoIdentita;
        this.documentoIdentitaFile = documentoIdentitaFile;
        this.nomeDocumentoIdentitaFile = nomeDocumentoIdentitaFile;
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
        this.caTipoDocumentoIdentita = caTipoDocumentoIdentita;
    }



    public Long getId() {
        return id;
    }

    public CaAnagrafica getCaAnagrafica() {
        return caAnagrafica;
    }

    public void setCaAnagrafica(CaAnagrafica caAnagrafica) {
        this.caAnagrafica = caAnagrafica;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumDocumentoIdentita() {
        return numDocumentoIdentita;
    }

    public void setNumDocumentoIdentita(String numDocumentoIdentita) {
        this.numDocumentoIdentita = numDocumentoIdentita;
    }

    public byte[] getDocumentoIdentitaFile() {
        return documentoIdentitaFile;
    }

    public void setDocumentoIdentitaFile(byte[] documentoIdentitaFile) {
        this.documentoIdentitaFile = documentoIdentitaFile;
    }

    public String getNomeDocumentoIdentitaFile() {
        return nomeDocumentoIdentitaFile;
    }

    public void setNomeDocumentoIdentitaFile(String nomeDocumentoIdentitaFile) {
        this.nomeDocumentoIdentitaFile = nomeDocumentoIdentitaFile;
    }

    public String getEstensioneDocumentoIdentitaFile() {
        return estensioneDocumentoIdentitaFile;
    }

    public void setEstensioneDocumentoIdentitaFile(String estensioneDocumentoIdentitaFile) {
        this.estensioneDocumentoIdentitaFile = estensioneDocumentoIdentitaFile;
    }

    public CaTipoDocumentoIdentita getCaTipoDocumentoIdentita() {
        return caTipoDocumentoIdentita;
    }

    public void setCaTipoDocumentoIdentita(CaTipoDocumentoIdentita caTipoDocumentoIdentita) {
        this.caTipoDocumentoIdentita = caTipoDocumentoIdentita;
    }
}
