package it.sildifesa.sildifesa.models.ca;

import it.sildifesa.sildifesa.models.fo.FoCorsoAnagrafica;
import it.sildifesa.sildifesa.models.su.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "ca_anagrafica")
public class CaAnagrafica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private byte agevolazioniFiscali;


    private String annoTitoloStudio;


    private String arma;


    private String attestatiStudio;

    @Length(max= 5)
    private String capComuneNascita;

    @Length(max= 5)
    @NotNull(message = "Cap comune residenza obbligatorio")
    private String capComuneResidenza;


    private String categorieProtette;


    private String codiceFascicolazione;

	@NotEmpty(message = "Codice Fiscale obbligatorio")
	@Pattern(message = "Codice Fiscale non corretto", regexp = "^[a-zA-Z]{6}[0-9]{2}[abcdehlmprstABCDEHLMPRST]{1}[0-9]{2}([a-zA-Z]{1}[0-9]{3})[a-zA-Z]{1}$")
    private String codiceFiscale;


    @NotEmpty(message = "Cognome obbligatorio")
    private String cognome;


    private BigInteger comandoRFC;


    private String cpi;


    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "La Data di Congedo è obbligatoria")
    private Date dataCongedo;


    @Temporal(TemporalType.TIMESTAMP)
    private Date dataDomanda;


    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "La Data di Incorporazione è obbligatoria")
    private Date dataIncorporazione;


    @Temporal(TemporalType.TIMESTAMP)
    private Date datainviopsw;


    @Temporal(TemporalType.TIMESTAMP)
    private Date dataIscrizioneCPI;


    @Temporal(TemporalType.TIMESTAMP)
    private Date dataModificaPassword;

    @NotNull(message = "Data di nascita obbligatoria")
    @Past(message = "La Data non è corretta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascita;


    @Temporal(TemporalType.TIMESTAMP)
    private Date dataStatoSistema;


    private String dettaglioAgevolazioniFiscali;


    private String dettaglioTitoloStudio;


    private String distrettoMilitare;

    @NotEmpty(message =" Attenzione il campo e-mail è obbligatorio")
    @Email(message = "Attenzione inserire una e-mail valida")
    private String email;


    private String enteTitoloStudio;

    @NotNull(message = "F.A. di appartenenza obbligatorio")
    private String forzaArmata;


    private String gradoMilitare;


    private String indirizzoComunicazioni;


    private String indirizzoFamiglia;

    @NotEmpty(message =" Attenzione il campo Indirizzo Residenza è obbligatorio")
    private String indirizzoResidenza;

    @NotEmpty(message = "Nome obbligatorio")
    private String nome;

    private String note;

    private String noteStatoSistema;


    private short numeroFigli;

    @NotNull(message = "numero di telefono obbligatorio")
    private String numeroTelefono;


    private String password;

    private String rafferma;


    private BigInteger sedeServizio;

    @NotEmpty(message =" Attenzione il campo sesso è obbligatorio")
    private String sesso;


    private int sottoStatoSistema;


    private String statoCivile;

    @NotNull(message = "Lo Stato Giuridico è obbligatorio")
    private String statoGiuridico;


    private int statoSistema;


    private String tipoFerma;


    private String username;


    private String votoTitoloStudio;

    @NotNull(message = "Indirizzo domicilio obbligatorio")
    private String indirizzoDomicilio;

    @NotNull(message = "Cap domicilio obbligatorio")
    private String capDomicilio;




    //bi-directional many-to-one association to SuComune
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comuneResidenza")
    @Valid
    private SuComune suComune1;

    //bi-directional many-to-one association to SuComune
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comuneNascita")
    @Valid
    private SuComune suComune2;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comuneDomicilio")
    @Valid
    private SuComune suComune3;

    //bi-directional many-to-one association to SuEntemilitare
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "enteServizio")
    private SuEnteMilitare suEntemilitare;

    //bi-directional many-to-one association to SuIncaricomilitare
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incaricoMilitare")
    private SuIncaricoMilitare suIncaricomilitare;

    //bi-directional many-to-one association to SuSpecialita
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialita")
    @Valid
    private SuSpecialita suSpecialita;


    //bi-directional many-to-one association to CaAnagraficaTitolostudio
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "caAnagrafica", orphanRemoval = true)
    @Valid
    private List<CaAnagraficaTitoloDiStudio> caAnagraficaTitolostudios;


    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "documentoidentita_id", referencedColumnName = "id")
    @Valid
    private CaDocumentoIdentita caDocumentoIdentita;



    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name="ca_anagrafica_su_professioni"
            , joinColumns={
            @JoinColumn(name="ca_anagrafica_id")
    }
            , inverseJoinColumns={
            @JoinColumn(name="su_professione_id")
    }
    )
    private Set<SuProfessione> suProfessioni;

    private boolean statoAnagrafica;

    @OneToMany(mappedBy = "caAnagrafica")
    private Set<FoCorsoAnagrafica> foCorsoAnagraficas;

    public CaAnagrafica() {
    }

    public CaAnagrafica(Long id) {
        this.id = id;
    }

    public CaAnagrafica(Long id, byte agevolazioniFiscali, String annoTitoloStudio, String arma, String attestatiStudio, String capComuneNascita, String capComuneResidenza, String categorieProtette, String codiceFascicolazione, String codiceFiscale, String cognome, BigInteger comandoRFC, String cpi, Date dataCongedo, Date dataDomanda, Date dataIncorporazione, Date datainviopsw, Date dataIscrizioneCPI, Date dataModificaPassword, Date dataNascita, Date dataStatoSistema, String dettaglioAgevolazioniFiscali, String dettaglioTitoloStudio, String distrettoMilitare, String email, String enteTitoloStudio, String forzaArmata, String gradoMilitare, String indirizzoComunicazioni, String indirizzoFamiglia, String indirizzoResidenza, String nome, String note, String noteStatoSistema, short numeroFigli, String numeroTelefono, String password, String rafferma, BigInteger sedeServizio, String sesso, int sottoStatoSistema, String statoCivile, String statoGiuridico, int statoSistema, String tipoFerma, String username, String votoTitoloStudio, String indirizzoDomicilio, String capDomicilio, SuComune suComune1, SuComune suComune2, SuComune suComune3, SuEnteMilitare suEntemilitare, SuIncaricoMilitare suIncaricomilitare, SuSpecialita suSpecialita, List<CaAnagraficaTitoloDiStudio> caAnagraficaTitolostudios, CaDocumentoIdentita caDocumentoIdentita, boolean statoAnagrafica, Set<SuProfessione> suProfessioni, Set<FoCorsoAnagrafica> foCorsoAnagraficas) {
        this.id = id;
        this.agevolazioniFiscali = agevolazioniFiscali;
        this.annoTitoloStudio = annoTitoloStudio;
        this.arma = arma;
        this.attestatiStudio = attestatiStudio;
        this.capComuneNascita = capComuneNascita;
        this.capComuneResidenza = capComuneResidenza;
        this.categorieProtette = categorieProtette;
        this.codiceFascicolazione = codiceFascicolazione;
        this.codiceFiscale = codiceFiscale;
        this.cognome = cognome;
        this.comandoRFC = comandoRFC;
        this.cpi = cpi;
        this.dataCongedo = dataCongedo;
        this.dataDomanda = dataDomanda;
        this.dataIncorporazione = dataIncorporazione;
        this.datainviopsw = datainviopsw;
        this.dataIscrizioneCPI = dataIscrizioneCPI;
        this.dataModificaPassword = dataModificaPassword;
        this.dataNascita = dataNascita;
        this.dataStatoSistema = dataStatoSistema;
        this.dettaglioAgevolazioniFiscali = dettaglioAgevolazioniFiscali;
        this.dettaglioTitoloStudio = dettaglioTitoloStudio;
        this.distrettoMilitare = distrettoMilitare;
        this.email = email;
        this.enteTitoloStudio = enteTitoloStudio;
        this.forzaArmata = forzaArmata;
        this.gradoMilitare = gradoMilitare;
        this.indirizzoComunicazioni = indirizzoComunicazioni;
        this.indirizzoFamiglia = indirizzoFamiglia;
        this.indirizzoResidenza = indirizzoResidenza;
        this.nome = nome;
        this.note = note;
        this.noteStatoSistema = noteStatoSistema;
        this.numeroFigli = numeroFigli;
        this.numeroTelefono = numeroTelefono;
        this.password = password;
        this.rafferma = rafferma;
        this.sedeServizio = sedeServizio;
        this.sesso = sesso;
        this.sottoStatoSistema = sottoStatoSistema;
        this.statoCivile = statoCivile;
        this.statoGiuridico = statoGiuridico;
        this.statoSistema = statoSistema;
        this.tipoFerma = tipoFerma;
        this.username = username;
        this.votoTitoloStudio = votoTitoloStudio;
        this.indirizzoDomicilio = indirizzoDomicilio;
        this.capDomicilio = capDomicilio;
        this.suComune1 = suComune1;
        this.suComune2 = suComune2;
        this.suComune3 = suComune3;
        this.suEntemilitare = suEntemilitare;
        this.suIncaricomilitare = suIncaricomilitare;
        this.suSpecialita = suSpecialita;
        this.caAnagraficaTitolostudios = caAnagraficaTitolostudios;
        this.caDocumentoIdentita = caDocumentoIdentita;
        this.statoAnagrafica = statoAnagrafica;
        this.suProfessioni = suProfessioni;
        this.foCorsoAnagraficas = foCorsoAnagraficas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte getAgevolazioniFiscali() {
        return agevolazioniFiscali;
    }

    public void setAgevolazioniFiscali(byte agevolazioniFiscali) {
        this.agevolazioniFiscali = agevolazioniFiscali;
    }

    public String getAnnoTitoloStudio() {
        return annoTitoloStudio;
    }

    public void setAnnoTitoloStudio(String annoTitoloStudio) {
        this.annoTitoloStudio = annoTitoloStudio;
    }

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }

    public String getAttestatiStudio() {
        return attestatiStudio;
    }

    public void setAttestatiStudio(String attestatiStudio) {
        this.attestatiStudio = attestatiStudio;
    }

    public String getCapComuneNascita() {
        return capComuneNascita;
    }

    public void setCapComuneNascita(String capComuneNascita) {
        this.capComuneNascita = capComuneNascita;
    }

    public String getCapComuneResidenza() {
        return capComuneResidenza;
    }

    public void setCapComuneResidenza(String capComuneResidenza) {
        this.capComuneResidenza = capComuneResidenza;
    }

    public String getCategorieProtette() {
        return categorieProtette;
    }

    public void setCategorieProtette(String categorieProtette) {
        this.categorieProtette = categorieProtette;
    }

    public String getCodiceFascicolazione() {
        return codiceFascicolazione;
    }

    public void setCodiceFascicolazione(String codiceFascicolazione) {
        this.codiceFascicolazione = codiceFascicolazione;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public BigInteger getComandoRFC() {
        return comandoRFC;
    }

    public void setComandoRFC(BigInteger comandoRFC) {
        this.comandoRFC = comandoRFC;
    }

    public String getCpi() {
        return cpi;
    }

    public void setCpi(String cpi) {
        this.cpi = cpi;
    }

    public Date getDataCongedo() {
        return dataCongedo;
    }

    public void setDataCongedo(Date dataCongedo) {
        this.dataCongedo = dataCongedo;
    }

    public Date getDataDomanda() {
        return dataDomanda;
    }

    public void setDataDomanda(Date dataDomanda) {
        this.dataDomanda = dataDomanda;
    }

    public Date getDataIncorporazione() {
        return dataIncorporazione;
    }

    public void setDataIncorporazione(Date dataIncorporazione) {
        this.dataIncorporazione = dataIncorporazione;
    }

    public Date getDatainviopsw() {
        return datainviopsw;
    }

    public void setDatainviopsw(Date datainviopsw) {
        this.datainviopsw = datainviopsw;
    }

    public Date getDataIscrizioneCPI() {
        return dataIscrizioneCPI;
    }

    public void setDataIscrizioneCPI(Date dataIscrizioneCPI) {
        this.dataIscrizioneCPI = dataIscrizioneCPI;
    }

    public Date getDataModificaPassword() {
        return dataModificaPassword;
    }

    public void setDataModificaPassword(Date dataModificaPassword) {
        this.dataModificaPassword = dataModificaPassword;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public Date getDataStatoSistema() {
        return dataStatoSistema;
    }

    public void setDataStatoSistema(Date dataStatoSistema) {
        this.dataStatoSistema = dataStatoSistema;
    }

    public String getDettaglioAgevolazioniFiscali() {
        return dettaglioAgevolazioniFiscali;
    }

    public void setDettaglioAgevolazioniFiscali(String dettaglioAgevolazioniFiscali) {
        this.dettaglioAgevolazioniFiscali = dettaglioAgevolazioniFiscali;
    }

    public String getDettaglioTitoloStudio() {
        return dettaglioTitoloStudio;
    }

    public void setDettaglioTitoloStudio(String dettaglioTitoloStudio) {
        this.dettaglioTitoloStudio = dettaglioTitoloStudio;
    }

    public String getDistrettoMilitare() {
        return distrettoMilitare;
    }

    public void setDistrettoMilitare(String distrettoMilitare) {
        this.distrettoMilitare = distrettoMilitare;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnteTitoloStudio() {
        return enteTitoloStudio;
    }

    public void setEnteTitoloStudio(String enteTitoloStudio) {
        this.enteTitoloStudio = enteTitoloStudio;
    }

    public String getForzaArmata() {
        return forzaArmata;
    }

    public void setForzaArmata(String forzaArmata) {
        this.forzaArmata = forzaArmata;
    }

    public String getGradoMilitare() {
        return gradoMilitare;
    }

    public void setGradoMilitare(String gradoMilitare) {
        this.gradoMilitare = gradoMilitare;
    }

    public String getIndirizzoComunicazioni() {
        return indirizzoComunicazioni;
    }

    public void setIndirizzoComunicazioni(String indirizzoComunicazioni) {
        this.indirizzoComunicazioni = indirizzoComunicazioni;
    }

    public String getIndirizzoFamiglia() {
        return indirizzoFamiglia;
    }

    public void setIndirizzoFamiglia(String indirizzoFamiglia) {
        this.indirizzoFamiglia = indirizzoFamiglia;
    }

    public String getIndirizzoResidenza() {
        return indirizzoResidenza;
    }

    public void setIndirizzoResidenza(String indirizzoResidenza) {
        this.indirizzoResidenza = indirizzoResidenza;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteStatoSistema() {
        return noteStatoSistema;
    }

    public void setNoteStatoSistema(String noteStatoSistema) {
        this.noteStatoSistema = noteStatoSistema;
    }

    public short getNumeroFigli() {
        return numeroFigli;
    }

    public void setNumeroFigli(short numeroFigli) {
        this.numeroFigli = numeroFigli;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRafferma() {
        return rafferma;
    }

    public void setRafferma(String rafferma) {
        this.rafferma = rafferma;
    }

    public BigInteger getSedeServizio() {
        return sedeServizio;
    }

    public void setSedeServizio(BigInteger sedeServizio) {
        this.sedeServizio = sedeServizio;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public int getSottoStatoSistema() {
        return sottoStatoSistema;
    }

    public void setSottoStatoSistema(int sottoStatoSistema) {
        this.sottoStatoSistema = sottoStatoSistema;
    }

    public String getStatoCivile() {
        return statoCivile;
    }

    public void setStatoCivile(String statoCivile) {
        this.statoCivile = statoCivile;
    }

    public String getStatoGiuridico() {
        return statoGiuridico;
    }

    public void setStatoGiuridico(String statoGiuridico) {
        this.statoGiuridico = statoGiuridico;
    }

    public int getStatoSistema() {
        return statoSistema;
    }

    public void setStatoSistema(int statoSistema) {
        this.statoSistema = statoSistema;
    }

    public String getTipoFerma() {
        return tipoFerma;
    }

    public void setTipoFerma(String tipoFerma) {
        this.tipoFerma = tipoFerma;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVotoTitoloStudio() {
        return votoTitoloStudio;
    }

    public void setVotoTitoloStudio(String votoTitoloStudio) {
        this.votoTitoloStudio = votoTitoloStudio;
    }

    public String getIndirizzoDomicilio() {
        return indirizzoDomicilio;
    }

    public void setIndirizzoDomicilio(String indirizzoDomicilio) {
        this.indirizzoDomicilio = indirizzoDomicilio;
    }

    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

    public SuComune getSuComune1() {
        return suComune1;
    }

    public void setSuComune1(SuComune suComune1) {
        this.suComune1 = suComune1;
    }

    public SuComune getSuComune2() {
        return suComune2;
    }

    public void setSuComune2(SuComune suComune2) {
        this.suComune2 = suComune2;
    }

    public SuComune getSuComune3() {
        return suComune3;
    }

    public void setSuComune3(SuComune suComune3) {
        this.suComune3 = suComune3;
    }

    public SuEnteMilitare getSuEntemilitare() {
        return suEntemilitare;
    }

    public void setSuEntemilitare(SuEnteMilitare suEntemilitare) {
        this.suEntemilitare = suEntemilitare;
    }

    public SuIncaricoMilitare getSuIncaricomilitare() {
        return suIncaricomilitare;
    }

    public void setSuIncaricomilitare(SuIncaricoMilitare suIncaricomilitare) {
        this.suIncaricomilitare = suIncaricomilitare;
    }

    public SuSpecialita getSuSpecialita() {
        return suSpecialita;
    }

    public void setSuSpecialita(SuSpecialita suSpecialita) {
        this.suSpecialita = suSpecialita;
    }

    public List<CaAnagraficaTitoloDiStudio> getCaAnagraficaTitolostudios() {
        return caAnagraficaTitolostudios;
    }

    public void setCaAnagraficaTitolostudios(List<CaAnagraficaTitoloDiStudio> caAnagraficaTitolostudios) {
        this.caAnagraficaTitolostudios = caAnagraficaTitolostudios;
    }

    public CaDocumentoIdentita getCaDocumentoIdentita() {
        return caDocumentoIdentita;
    }

    public void setCaDocumentoIdentita(CaDocumentoIdentita caDocumentoIdentita) {
        this.caDocumentoIdentita = caDocumentoIdentita;
    }

    public Set<SuProfessione> getSuProfessioni() {
        return suProfessioni;
    }

    public void setSuProfessioni(Set<SuProfessione> suProfessioni) {
        this.suProfessioni = suProfessioni;
    }

    public boolean isStatoAnagrafica() {
        return statoAnagrafica;
    }

    public void setStatoAnagrafica(boolean statoAnagrafica) {
        this.statoAnagrafica = statoAnagrafica;
    }

    public Set<FoCorsoAnagrafica> getFoCorsoAnagraficas() {
        return foCorsoAnagraficas;
    }

    public void setFoCorsoAnagraficas(Set<FoCorsoAnagrafica> foCorsoAnagraficas) {
        this.foCorsoAnagraficas = foCorsoAnagraficas;
    }
}
