package it.sildifesa.sildifesa.models.ca;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "ca_tipodocumentoidentita")
public class CaTipoDocumentoIdentita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull(message = "Tipo documento obbligatorio")
    private Long id;

    private String nome;



    @OneToMany(mappedBy = "caTipoDocumentoIdentita")
    private List<CaDocumentoIdentita> caDocumentoIdentita;



    public CaTipoDocumentoIdentita() {
    }

    public CaTipoDocumentoIdentita(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<CaDocumentoIdentita> getCaDocumentoIdentita() {
        return caDocumentoIdentita;
    }

    public void setCaDocumentoIdentita(List<CaDocumentoIdentita> caDocumentoIdentita) {
        this.caDocumentoIdentita = caDocumentoIdentita;
    }
}
