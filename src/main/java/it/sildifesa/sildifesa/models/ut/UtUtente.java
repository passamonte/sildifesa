package it.sildifesa.sildifesa.models.ut;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The persistent class for the ut_utente database table.
 * 
 */
@Entity
@Table(name="ut_utente")
@NamedQuery(name="UtUtente.findAll", query="SELECT u FROM UtUtente u")
public class UtUtente implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Cognome obbligatorio")
	private String cognome;


	@Temporal(TemporalType.TIMESTAMP)
	private Date dataModificaPassword;


	@Temporal(TemporalType.TIMESTAMP)
	private Date dataUltimoAccesso;

	private String descrizioneRuolo;

	@NotEmpty(message = "Email obbligatoria")
	private String email;

	private int livelloPermessi;

	@NotEmpty(message = "Nome obbligatorio")
	private String nome;

	@NotEmpty(message = "Password obbligatoria")
	private String password;

	@Length(min=4, max= 16, message = "Attenzione, l'username deve essere compreso tra i 4 e i 16 caratteri")
	@NotEmpty(message = "Username obbligatorio")
	private String username;

	private Boolean abilitato;

	@Transient
	private String passwordConfirm;

	@ManyToMany(cascade = {CascadeType.MERGE})
	@JoinTable(
			name="ut_utente_ut_roles"
			, joinColumns={
			@JoinColumn(name="ut_utente_id")
	}
			, inverseJoinColumns={
			@JoinColumn(name="ut_role_id")
	}
	)
	private List<UtRole> roles;

	private String tokenConferma;

	private String tokenReset;

	public UtUtente() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getDataModificaPassword() {
		return dataModificaPassword;
	}

	public void setDataModificaPassword(Date dataModificaPassword) {
		this.dataModificaPassword = dataModificaPassword;
	}

	public String getDescrizioneRuolo() {
		return descrizioneRuolo;
	}

	public void setDescrizioneRuolo(String descrizioneRuolo) {
		this.descrizioneRuolo = descrizioneRuolo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getLivelloPermessi() {
		return livelloPermessi;
	}

	public void setLivelloPermessi(int livelloPermessi) {
		this.livelloPermessi = livelloPermessi;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getAbilitato() {
		return abilitato;
	}

	public void setAbilitato(Boolean abilitato) {
		this.abilitato = abilitato;
	}

	public Date getDataUltimoAccesso() {
		return dataUltimoAccesso;
	}

	public void setDataUltimoAccesso(Date dataUltimoAccesso) {
		this.dataUltimoAccesso = dataUltimoAccesso;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public List<UtRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UtRole> roles) {
		this.roles = roles;
	}

	public String getTokenConferma() {
		return tokenConferma;
	}

	public void setTokenConferma(String tokenConferma) {
		this.tokenConferma = tokenConferma;

	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return this.abilitato;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id, username, password, dataUltimoAccesso);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UtUtente utente = (UtUtente) o;
		return Objects.equals(id, utente.id) &&
				Objects.equals(username, utente.username) &&
				Objects.equals(password, utente.password) &&
				Objects.equals(dataUltimoAccesso, utente.dataUltimoAccesso);
	}
}
