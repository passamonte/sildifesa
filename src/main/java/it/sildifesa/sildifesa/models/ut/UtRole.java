package it.sildifesa.sildifesa.models.ut;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ut_role")
public class UtRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private List<UtUtente> users;

    public UtRole() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UtUtente> getUsers() {
        return users;
    }

    public void setUsers(List<UtUtente> users) {
        this.users = users;
    }
}
