package it.sildifesa.sildifesa.services.rp;

import it.sildifesa.sildifesa.dao.rp.RpCodificaLivelloTitoloRepository;
import it.sildifesa.sildifesa.models.rp.RpCodificaLivelloTitolo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RpCodificaLivelloTitoloServiceImpl implements RpCodificaLivelloTitoloService {

    @Autowired
    RpCodificaLivelloTitoloRepository rpCodificaLivelloTitoloRepository;


    @Override
    @Transactional
    public List<RpCodificaLivelloTitolo> elencaRpCodificaLivelloTitolo() {
        return (List<RpCodificaLivelloTitolo>) rpCodificaLivelloTitoloRepository.findAll();
    }

    @Override
    @Transactional
    public RpCodificaLivelloTitolo caricaSingoloElemento(Long input) {
        return rpCodificaLivelloTitoloRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(RpCodificaLivelloTitolo rpCodificaLivelloTitolo) {
        rpCodificaLivelloTitoloRepository.save(rpCodificaLivelloTitolo);
    }

    @Override
    @Transactional
    public void aggiorna(RpCodificaLivelloTitolo rpCodificaLivelloTitolo) {
        rpCodificaLivelloTitoloRepository.save(rpCodificaLivelloTitolo);
    }

    @Override
    @Transactional
    public void rimuovi(RpCodificaLivelloTitolo rpCodificaLivelloTitolo) {
        rpCodificaLivelloTitoloRepository.delete(rpCodificaLivelloTitolo);
    }
}
