package it.sildifesa.sildifesa.services.rp;

import it.sildifesa.sildifesa.models.rp.RpCodificaLivelloTitolo;
import it.sildifesa.sildifesa.models.su.SuComune;

import java.util.List;

public interface RpCodificaLivelloTitoloService {
    public List<RpCodificaLivelloTitolo> elencaRpCodificaLivelloTitolo();

    public RpCodificaLivelloTitolo caricaSingoloElemento(Long input);

    public void inserisciNuovo(RpCodificaLivelloTitolo rpCodificaLivelloTitolo);

    public void aggiorna(RpCodificaLivelloTitolo rpCodificaLivelloTitolo);

    public void rimuovi(RpCodificaLivelloTitolo rpCodificaLivelloTitolo);
}
