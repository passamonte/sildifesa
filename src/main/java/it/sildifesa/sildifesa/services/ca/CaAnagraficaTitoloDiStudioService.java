package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;

import java.util.List;

public interface CaAnagraficaTitoloDiStudioService {


    public List<CaAnagraficaTitoloDiStudio> elencaCaAnagraficaTitoloDiStudio();

    public CaAnagraficaTitoloDiStudio caricaSingoloElemento(Long input);

    public void inserisciNuovo(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio);

    public void aggiorna(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio);

    public void rimuovi(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio);
}
