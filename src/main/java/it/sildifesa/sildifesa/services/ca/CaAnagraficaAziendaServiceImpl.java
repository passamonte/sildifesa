package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dao.ca.CaAnagraficaAziendaRepository;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class CaAnagraficaAziendaServiceImpl implements CaAnagraficaAziendaService {


    @Autowired
    CaAnagraficaAziendaRepository caAnagraficaAziendaRepository;


    @Override
    @Transactional
    public Page<CaAnagraficaAzienda> elencaCaAnagraficaAziende(CaAnagraficaAzienda anagraficaAzienda,Pageable pageable) {

        return caAnagraficaAziendaRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Long.class != criteriaQuery.getResultType()) {
                root.fetch("comuneSedeLegale", JoinType.LEFT);
            }
            if (Objects.nonNull(anagraficaAzienda.getRagioneSociale())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("ragioneSociale"), "%" + anagraficaAzienda.getRagioneSociale() + "%"));
            }
            if (Objects.nonNull(anagraficaAzienda.getPartitaIva())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("partitaIva"), "%" + anagraficaAzienda.getPartitaIva() + "%"));
            }
            if (Objects.nonNull(anagraficaAzienda.getNomeReferente())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("nomeReferente"), "%" + anagraficaAzienda.getNomeReferente() + "%"));
            }
            jpaCriteriaList.add(root.get("attivo").in(true));

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);

    }

    @Override
    @Transactional
    public CaAnagraficaAzienda caricaSingoloElemento(Long input) {
        return caAnagraficaAziendaRepository.findById(input).get();
    }


    @Override
    @Transactional
    public void inserisciNuovo(CaAnagraficaAzienda caAnagraficaAzienda) {
        caAnagraficaAziendaRepository.save(caAnagraficaAzienda);
    }

    @Override
    @Transactional
    public void aggiorna(CaAnagraficaAzienda caAnagraficaAzienda) {
        caAnagraficaAziendaRepository.save(caAnagraficaAzienda);
    }

    @Override
    @Transactional
    public void rimuovi(CaAnagraficaAzienda caAnagraficaAzienda) {
        caAnagraficaAziendaRepository.delete(caAnagraficaAzienda);
    }

    @Override
    @Transactional
    public Page<CaAnagraficaAzienda> elencaCaAnagraficaAziendeCompleta(Pageable pageable) {

        List<CaAnagraficaAzienda> listaAnagraficaAziende = (List<CaAnagraficaAzienda>) caAnagraficaAziendaRepository.findAllComplete();

        Page<CaAnagraficaAzienda> listaAnagraficaPage = SildifesaUtility.listToPage(listaAnagraficaAziende, pageable);

        return listaAnagraficaPage;

    }

    @Override
    @Transactional
    public CaAnagraficaAzienda caricaSingoloElementoCompleto(Long input) {
        return caAnagraficaAziendaRepository.findOneComplete(input);
    }
}
