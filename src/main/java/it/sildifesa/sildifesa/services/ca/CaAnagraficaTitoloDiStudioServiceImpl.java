package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaAnagraficaTitoloDiStudioRepository;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CaAnagraficaTitoloDiStudioServiceImpl implements CaAnagraficaTitoloDiStudioService {
    @Autowired
    private CaAnagraficaTitoloDiStudioRepository caAnagraficaTitoloDiStudioRepository;

    @Override
    @Transactional
    public List<CaAnagraficaTitoloDiStudio> elencaCaAnagraficaTitoloDiStudio() {
        return (List<CaAnagraficaTitoloDiStudio>) caAnagraficaTitoloDiStudioRepository.findAll();
    }

    @Override
    @Transactional
    public CaAnagraficaTitoloDiStudio caricaSingoloElemento(Long input) {
        return caAnagraficaTitoloDiStudioRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio) {
        caAnagraficaTitoloDiStudioRepository.save(caAnagraficaTitoloDiStudio);
    }

    @Override
    @Transactional
    public void aggiorna(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio) {
        caAnagraficaTitoloDiStudioRepository.save(caAnagraficaTitoloDiStudio);
    }

    @Override
    @Transactional
    public void rimuovi(CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio) {
        caAnagraficaTitoloDiStudioRepository.delete(caAnagraficaTitoloDiStudio);
    }
}
