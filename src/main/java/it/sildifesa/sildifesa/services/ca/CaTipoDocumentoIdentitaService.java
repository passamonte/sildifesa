package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.ca.CaTipoDocumentoIdentita;

import java.util.List;

public interface CaTipoDocumentoIdentitaService {
    public List<CaTipoDocumentoIdentita> elencaCaTipoDocumentoIdentita();

    public CaTipoDocumentoIdentita caricaSingoloElemento(Long input);

    public void inserisciNuovo(CaTipoDocumentoIdentita caTipoDocumentoIdentita);

    public void aggiorna(CaTipoDocumentoIdentita caTipoDocumentoIdentita);

    public void rimuovi(CaTipoDocumentoIdentita caTipoDocumentoIdentita);
}
