package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaDocumentoIdentitaRepository;
import it.sildifesa.sildifesa.models.ca.CaDocumentoIdentita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaDocumentoIdentitaServiceImpl implements CaDocumentoIdentitaService {

    @Autowired
    private CaDocumentoIdentitaRepository caDocumentoIdentitaRepository;


    @Override
    public List<CaDocumentoIdentita> elencaCaDocumentoIdentita() {
        return (List<CaDocumentoIdentita>) caDocumentoIdentitaRepository.findAll();
    }

    @Override
    public CaDocumentoIdentita caricaSingoloElemento(Long input) {
        return caDocumentoIdentitaRepository.findById(input).get();
    }

    @Override
    public void inserisciNuovo(CaDocumentoIdentita caDocumentoIdentita) {
        caDocumentoIdentitaRepository.save(caDocumentoIdentita);
    }

    @Override
    public void aggiorna(CaDocumentoIdentita caDocumentoIdentita) {
        caDocumentoIdentitaRepository.save(caDocumentoIdentita);
    }

    @Override
    public void rimuovi(CaDocumentoIdentita caDocumentoIdentita) {
        caDocumentoIdentitaRepository.delete(caDocumentoIdentita);
    }
}
