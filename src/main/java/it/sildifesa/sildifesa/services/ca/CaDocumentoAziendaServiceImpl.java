package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaDocumentoAziendaRepository;
import it.sildifesa.sildifesa.models.ca.CaDocumentoAzienda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CaDocumentoAziendaServiceImpl implements CaDocumentoAziendaService {

    @Autowired
    CaDocumentoAziendaRepository caDocumentoAziendaRepository;

    @Override
    @Transactional
    public List<CaDocumentoAzienda> elencaCaDocumentoAzienda() {
        return (List<CaDocumentoAzienda>) caDocumentoAziendaRepository.findAll();
    }

    @Override
    @Transactional
    public CaDocumentoAzienda caricaSingoloElemento(Long input) {
        return caDocumentoAziendaRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(CaDocumentoAzienda caDocumentoAzienda) {
        caDocumentoAziendaRepository.save(caDocumentoAzienda);
    }

    @Override
    @Transactional
    public void aggiorna(CaDocumentoAzienda caDocumentoAzienda) {
        caDocumentoAziendaRepository.save(caDocumentoAzienda);
    }

    @Override
    @Transactional
    public void rimuovi(CaDocumentoAzienda caDocumentoAzienda) {
        caDocumentoAziendaRepository.delete(caDocumentoAzienda);
    }
}
