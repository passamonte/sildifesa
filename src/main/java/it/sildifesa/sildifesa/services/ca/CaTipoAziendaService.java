package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;

import java.util.List;

public interface CaTipoAziendaService {


    public List<CaTipoAzienda> elencaCaTipoAzienda();

    public CaTipoAzienda caricaSingoloElemento(Long input);

    public void inserisciNuovo(CaTipoAzienda caTipoAzienda);

    public void aggiorna(CaTipoAzienda caTipoAzienda);

    public void rimuovi(CaTipoAzienda caTipoAzienda);

}
