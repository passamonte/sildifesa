package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaAnagraficaRepository;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CaAnagraficaServiceImpl implements CaAnagraficaService {

    @Autowired
    private CaAnagraficaRepository caAnagraficaRepository;

    @Override
    public Page<CaAnagrafica> elencaCaAnagrafiche(CaAnagrafica anagrafica, Pageable pageable) {
        return caAnagraficaRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Objects.nonNull(anagrafica.getNome())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("nome"), "%" + anagrafica.getNome() + "%"));
            }
            if (Objects.nonNull(anagrafica.getCognome())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("cognome"), "%" + anagrafica.getCognome() + "%"));
            }
            jpaCriteriaList.add(root.get("statoAnagrafica").in(true));

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);

    }

    @Override
    @Transactional
    public CaAnagrafica caricaSingoloElemento(Long input) {
        return caAnagraficaRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(CaAnagrafica caAnagrafica) {
        caAnagraficaRepository.save(caAnagrafica);

    }

    @Override
    @Transactional
    public void aggiorna(CaAnagrafica caAnagrafica) {
        caAnagraficaRepository.save(caAnagrafica);

    }

    @Override
    public void rimuovi(CaAnagrafica caAnagrafica) {
        caAnagraficaRepository.delete(caAnagrafica);

    }

    @Override
    public CaAnagrafica caricaSingoloElementoEager(Long input){
        return caAnagraficaRepository.caricaSingoloElementoEager(input);
    }

}
