package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.ca.CaDocumentoAzienda;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CaDocumentoAziendaService {
    public List<CaDocumentoAzienda> elencaCaDocumentoAzienda();

    public CaDocumentoAzienda caricaSingoloElemento(Long input);


    public void inserisciNuovo(CaDocumentoAzienda caDocumentoAzienda);

    public void aggiorna(CaDocumentoAzienda caDocumentoAzienda);

    public void rimuovi(CaDocumentoAzienda caDocumentoAzienda);
}
