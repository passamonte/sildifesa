package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CaAnagraficaAziendaService {

    Page<CaAnagraficaAzienda> elencaCaAnagraficaAziende(CaAnagraficaAzienda caAnagraficaAzienda, Pageable pageable);

    CaAnagraficaAzienda caricaSingoloElemento(Long input);


    void inserisciNuovo(CaAnagraficaAzienda caAnagraficaAzienda);

    void aggiorna(CaAnagraficaAzienda caAnagraficaAzienda);

    void rimuovi(CaAnagraficaAzienda caAnagraficaAzienda);

    Page<CaAnagraficaAzienda> elencaCaAnagraficaAziendeCompleta(Pageable pageable);

    CaAnagraficaAzienda caricaSingoloElementoCompleto(Long input);

}
