package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaDocumentoIdentita;
import it.sildifesa.sildifesa.models.ca.CaTipoDocumentoIdentita;

import java.util.List;

public interface CaDocumentoIdentitaService {
    List<CaDocumentoIdentita> elencaCaDocumentoIdentita();

    CaDocumentoIdentita caricaSingoloElemento(Long input);

    void inserisciNuovo(CaDocumentoIdentita caTipoDocumentoIdentita);

    void aggiorna(CaDocumentoIdentita caTipoDocumentoIdentita);

    void rimuovi(CaDocumentoIdentita caTipoDocumentoIdentita);
}
