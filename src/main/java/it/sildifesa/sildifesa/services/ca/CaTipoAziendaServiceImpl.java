package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaTipoAziendaRepository;
import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CaTipoAziendaServiceImpl implements CaTipoAziendaService {

    @Autowired
    CaTipoAziendaRepository caTipoAziendaRepository;


    @Override
    @Transactional
    public List<CaTipoAzienda> elencaCaTipoAzienda() {
        return (List<CaTipoAzienda>) caTipoAziendaRepository.findAll();
    }

    @Override
    @Transactional
    public CaTipoAzienda caricaSingoloElemento(Long input) {
        return caTipoAziendaRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(CaTipoAzienda caTipoAzienda) {
        caTipoAziendaRepository.save(caTipoAzienda);
    }

    @Override
    @Transactional
    public void aggiorna(CaTipoAzienda caTipoAzienda) {
        caTipoAziendaRepository.save(caTipoAzienda);
    }

    @Override
    @Transactional
    public void rimuovi(CaTipoAzienda caTipoAzienda) {
        caTipoAziendaRepository.delete(caTipoAzienda);
    }
}
