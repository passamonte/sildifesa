package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CaAnagraficaService {

    Page<CaAnagrafica> elencaCaAnagrafiche(CaAnagrafica anagrafica, Pageable pageable);

    CaAnagrafica caricaSingoloElemento(Long input);

    CaAnagrafica caricaSingoloElementoEager(Long input);

    void inserisciNuovo(CaAnagrafica caAnagrafica);

    void aggiorna(CaAnagrafica caAnagrafica);

    void rimuovi(CaAnagrafica caAnagrafica);

}
