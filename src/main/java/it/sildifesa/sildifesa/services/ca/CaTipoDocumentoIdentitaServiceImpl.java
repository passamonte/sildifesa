package it.sildifesa.sildifesa.services.ca;

import it.sildifesa.sildifesa.dao.ca.CaTipoDocumentoIdentitaRepository;
import it.sildifesa.sildifesa.models.ca.CaTipoDocumentoIdentita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaTipoDocumentoIdentitaServiceImpl implements CaTipoDocumentoIdentitaService {

    @Autowired
    private CaTipoDocumentoIdentitaRepository caTipoDocumentoIdentitaRepository;


    @Override
    public List<CaTipoDocumentoIdentita> elencaCaTipoDocumentoIdentita() {
        return (List<CaTipoDocumentoIdentita>) caTipoDocumentoIdentitaRepository.findAll();
    }

    @Override
    public CaTipoDocumentoIdentita caricaSingoloElemento(Long input) {
        return caTipoDocumentoIdentitaRepository.findById(input).get();
    }

    @Override
    public void inserisciNuovo(CaTipoDocumentoIdentita caTipoDocumentoIdentita) {
        caTipoDocumentoIdentitaRepository.save(caTipoDocumentoIdentita);
    }

    @Override
    public void aggiorna(CaTipoDocumentoIdentita caTipoDocumentoIdentita) {
        caTipoDocumentoIdentitaRepository.save(caTipoDocumentoIdentita);
    }

    @Override
    public void rimuovi(CaTipoDocumentoIdentita caTipoDocumentoIdentita) {
        caTipoDocumentoIdentitaRepository.delete(caTipoDocumentoIdentita);
    }
}
