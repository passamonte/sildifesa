package it.sildifesa.sildifesa.services.securityservice;

import it.sildifesa.sildifesa.models.ut.UtUtente;
import it.sildifesa.sildifesa.services.ut.UtUtenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UtUtenteValidator  implements Validator {

    @Autowired
    private UtUtenteService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UtUtente.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UtUtente user = (UtUtente) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (user.getUsername().length() < 4 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.utenteVolontario.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.utenteVolontario.password");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.utenteVolontario.passwordConfirm");
        }
    }

}
