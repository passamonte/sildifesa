package it.sildifesa.sildifesa.services.securityservice;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
