package it.sildifesa.sildifesa.services.la;

import it.sildifesa.sildifesa.models.la.LaOfferta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LaOffertaService {


    Page<LaOfferta> elencaLaOfferte(Pageable pageable);

    LaOfferta caricaSingoloElemento(Long input);

    void inserisciNuovo(LaOfferta laOfferta);

    void aggiorna(LaOfferta laOfferta);

    void rimuovi(LaOfferta laOfferta);

    Page<LaOfferta> elencaLaOfferteCompleta(Pageable pageable);

    LaOfferta caricaSingoloElementoCompleto(Long input);

    LaOfferta caricaSingoloEager(Long input);


    Page<LaOfferta> elencaLaOffertePage(LaOfferta laOfferta, Pageable pageable);

}
