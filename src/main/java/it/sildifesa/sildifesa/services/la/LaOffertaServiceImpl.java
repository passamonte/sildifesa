package it.sildifesa.sildifesa.services.la;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dao.la.LaOffertaRepository;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class LaOffertaServiceImpl implements LaOffertaService {

    @Autowired
    LaOffertaRepository laOffertaRepository;

    private SildifesaUtility sildifesaUtility;


    @Override
    @Transactional
    public Page<LaOfferta> elencaLaOfferte(Pageable pageable) {
        List<LaOfferta> listaOfferte = (List<LaOfferta>) laOffertaRepository.findAll();

        Page<LaOfferta> listaOffertePage = sildifesaUtility.listToPage(listaOfferte, pageable);

        return listaOffertePage;
    }

    @Override
    @Transactional
    public Page<LaOfferta> elencaLaOffertePage(LaOfferta laOfferta, Pageable pageable) {
        return laOffertaRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Long.class != criteriaQuery.getResultType()) {
                root.fetch("azienda", JoinType.LEFT);
                root.fetch("localitaSedeOfferta", JoinType.LEFT);
                root.fetch("professione", JoinType.LEFT);
                root.fetch("settoreContratto", JoinType.LEFT);
                root.fetch("laOffertaTipoContratti", JoinType.LEFT);
            }
            if (Objects.nonNull(laOfferta.getDescrizione())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("descrizione"), "%" + laOfferta.getDescrizione() + "%"));
            }
            if (Objects.nonNull(laOfferta.getNumeroPosti())){
                jpaCriteriaList.add(criteriaBuilder.equal(root.get("numeroPosti"), laOfferta.getNumeroPosti()));
            }
            if (Objects.nonNull(laOfferta.getLocalitaSedeOfferta()) && Objects.nonNull(laOfferta.getLocalitaSedeOfferta().getNome())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("localitaSedeOfferta").get("nome"), "%" + laOfferta.getLocalitaSedeOfferta().getNome() + "%"));
            }
            jpaCriteriaList.add(root.get("attivo").in(true));

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    @Transactional
    public LaOfferta caricaSingoloElemento(Long input) {
        return laOffertaRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LaOfferta laOfferta) {
        laOffertaRepository.save(laOfferta);
    }

    @Override
    @Transactional
    public void aggiorna(LaOfferta laOfferta) {
        laOffertaRepository.save(laOfferta);
    }

    @Override
    @Transactional
    public void rimuovi(LaOfferta laOfferta) {
        laOffertaRepository.delete(laOfferta);
    }

    @Override
    @Transactional
    public Page<LaOfferta> elencaLaOfferteCompleta(Pageable pageable) {
        List<LaOfferta> listaOfferte = (List<LaOfferta>) laOffertaRepository.findAllComplete();

        Page<LaOfferta> listaOffertePage = sildifesaUtility.listToPage(listaOfferte, pageable);

        return listaOffertePage;
    }

    @Override
    @Transactional
    public LaOfferta caricaSingoloElementoCompleto(Long input) {
        return laOffertaRepository.findOneComplete(input);
    }

    public LaOfferta caricaSingoloEager(Long input){
        return laOffertaRepository.caricaSingoloEager(input);
    }

}
