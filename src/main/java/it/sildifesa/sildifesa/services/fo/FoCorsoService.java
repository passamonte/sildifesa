package it.sildifesa.sildifesa.services.fo;

import it.sildifesa.sildifesa.models.fo.FoCorso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FoCorsoService {

    Page<FoCorso> elencaFoCorsi(FoCorso foCorso, Pageable pageable);

    void inserisciNuovo(FoCorso corso);

    FoCorso caricaSingoloEager(Long id);

    void aggiorna(FoCorso foCorso);

}
