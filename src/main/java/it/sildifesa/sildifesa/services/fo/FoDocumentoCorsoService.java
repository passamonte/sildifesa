package it.sildifesa.sildifesa.services.fo;

import it.sildifesa.sildifesa.models.fo.FoDocumentoCorso;

public interface FoDocumentoCorsoService {

    FoDocumentoCorso caricaSingoloElemento(Long input);

    void inserisciNuovo(FoDocumentoCorso foDocumentoCorso);

    void aggiorna(FoDocumentoCorso foDocumentoCorso);

    void rimuovi(FoDocumentoCorso foDocumentoCorso);

    FoDocumentoCorso caricaSingoloElementoEager(Long input);
}
