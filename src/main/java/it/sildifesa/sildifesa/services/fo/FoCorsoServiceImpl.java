package it.sildifesa.sildifesa.services.fo;

import it.sildifesa.sildifesa.dao.fo.FoCorsoRepository;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class FoCorsoServiceImpl implements FoCorsoService {


    @Autowired
    FoCorsoRepository foCorsoRepository;

    @Override
    @Transactional
    public Page<FoCorso> elencaFoCorsi(FoCorso foCorso, Pageable pageable) {
        return foCorsoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Long.class != criteriaQuery.getResultType()) {
                root.fetch("caAnagraficaAzienda", JoinType.LEFT);
                root.fetch("localitaSedeCorso", JoinType.LEFT);
            }
            if (Objects.nonNull(foCorso.getTitolo())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("titolo"), "%" + foCorso.getTitolo() + "%"));
            }
            if (Objects.nonNull(foCorso.getCaAnagraficaAzienda()) && Objects.nonNull(foCorso.getCaAnagraficaAzienda().getRagioneSociale())){
                jpaCriteriaList.add(criteriaBuilder.like(root.get("caAnagraficaAzienda").get("ragioneSociale"), "%" + foCorso.getCaAnagraficaAzienda().getRagioneSociale() + "%"));
            }
            jpaCriteriaList.add(root.get("attivo").in(true));

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    public void inserisciNuovo(FoCorso corso){
        foCorsoRepository.save(corso);
    }

    @Override
    public FoCorso caricaSingoloEager(Long id){
        return foCorsoRepository.caricaSingoloElementoEager(id);
    }

    @Override
    @Transactional
    public void aggiorna(FoCorso corso) {
        foCorsoRepository.save(corso);

    }
}
