package it.sildifesa.sildifesa.services.fo;

import it.sildifesa.sildifesa.dao.fo.FoDocumentoCorsoRepository;
import it.sildifesa.sildifesa.models.fo.FoDocumentoCorso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FoDocumentoCorsoServiceImpl implements FoDocumentoCorsoService {

    @Autowired
    private FoDocumentoCorsoRepository foDocumentoCorsoRepository;


    @Override
    public FoDocumentoCorso caricaSingoloElemento(Long input) {
        return foDocumentoCorsoRepository.findById(input).get();
    }

    @Override
    public void inserisciNuovo(FoDocumentoCorso foDocumentoCorso) {
        foDocumentoCorsoRepository.save(foDocumentoCorso);
    }

    @Override
    public void aggiorna(FoDocumentoCorso foDocumentoCorso) {
        foDocumentoCorsoRepository.save(foDocumentoCorso);
    }

    @Override
    public void rimuovi(FoDocumentoCorso foDocumentoCorso) {
        foDocumentoCorsoRepository.delete(foDocumentoCorso);
    }

    @Override
    public FoDocumentoCorso caricaSingoloElementoEager(Long input) {
        return foDocumentoCorsoRepository.caricaSingoloElementoEager(input);
    }
}
