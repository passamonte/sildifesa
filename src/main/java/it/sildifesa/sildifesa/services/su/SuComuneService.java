package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.models.su.SuComune;

import java.util.List;

public interface SuComuneService {

    public List<SuComune> elencaSuComuni();

    public SuComune caricaSingoloElemento(Long input);

    public void inserisciNuovo(SuComune suComune);

    public void aggiorna(SuComune suComune);

    public void rimuovi(SuComune suComune);

    public void inserisciNuovoIstat();
}
