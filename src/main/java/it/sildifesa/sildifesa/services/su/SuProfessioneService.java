package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.models.su.SuProfessione;

import java.util.List;

public interface SuProfessioneService {

    public List<SuProfessione> elencaSuProfessioni();

    public SuProfessione caricaSingoloElemento(Long input);

    public void inserisciNuovo(SuProfessione suProfessione);

    public void aggiorna(SuProfessione suProfessione);

    public void rimuovi(SuProfessione suProfessione);

}
