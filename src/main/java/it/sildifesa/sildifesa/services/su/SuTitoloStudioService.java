package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.models.su.SuComune;
import it.sildifesa.sildifesa.models.su.SuTitoloStudio;

import java.util.List;

public interface SuTitoloStudioService {

    public List<SuTitoloStudio> elencaSuTitoloStudio();

    public SuTitoloStudio caricaSingoloElemento(Long input);

    public void inserisciNuovo(SuTitoloStudio suTitoloStudio);

    public void aggiorna(SuTitoloStudio suTitoloStudio);

    public void rimuovi(SuTitoloStudio suTitoloStudio);
}
