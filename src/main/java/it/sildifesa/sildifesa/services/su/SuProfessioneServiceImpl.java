package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.dao.su.SuProfessioneRepository;
import it.sildifesa.sildifesa.models.su.SuProfessione;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuProfessioneServiceImpl implements SuProfessioneService {

    @Autowired
    private SuProfessioneRepository suProfessioneRepository;

    @Override
    public List<SuProfessione> elencaSuProfessioni() {
        return (List<SuProfessione>) suProfessioneRepository.findAll();
    }

    @Override
    public SuProfessione caricaSingoloElemento(Long input) {
        return suProfessioneRepository.findById(input).get();
    }

    @Override
    public void inserisciNuovo(SuProfessione suProfessione) {
        suProfessioneRepository.save(suProfessione);
    }

    @Override
    public void aggiorna(SuProfessione suProfessione) {
        suProfessioneRepository.save(suProfessione);
    }

    @Override
    public void rimuovi(SuProfessione suProfessione) {
        suProfessioneRepository.delete(suProfessione);
    }
}
