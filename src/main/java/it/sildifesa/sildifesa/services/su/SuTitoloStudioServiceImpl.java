package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.dao.su.SuTitoloStudioRepository;
import it.sildifesa.sildifesa.models.su.SuTitoloStudio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuTitoloStudioServiceImpl implements SuTitoloStudioService {
    @Autowired
    private SuTitoloStudioRepository suTitoloStudioRepository;


    @Override
    public List<SuTitoloStudio> elencaSuTitoloStudio() {
        return (List<SuTitoloStudio>) suTitoloStudioRepository.findAll();
    }

    @Override
    public SuTitoloStudio caricaSingoloElemento(Long input) {
        return suTitoloStudioRepository.findById(input).get();
    }

    @Override
    public void inserisciNuovo(SuTitoloStudio suTitoloStudio) {
        suTitoloStudioRepository.save(suTitoloStudio);
    }

    @Override
    public void aggiorna(SuTitoloStudio suTitoloStudio) {
        suTitoloStudioRepository.save(suTitoloStudio);
    }

    @Override
    public void rimuovi(SuTitoloStudio suTitoloStudio) {
        suTitoloStudioRepository.delete(suTitoloStudio);
    }
}
