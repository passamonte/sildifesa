package it.sildifesa.sildifesa.services.su;

import it.sildifesa.sildifesa.controller.gestione.GestioneSistemaController;
import it.sildifesa.sildifesa.dao.su.SuComuneRepository;
import it.sildifesa.sildifesa.models.su.SuComune;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

@Service
public class SuComuneServiceImpl implements SuComuneService {
    private static final Logger LOGGER = LogManager.getLogger(SuComuneServiceImpl.class);

    @Value("${app.upload.dir:${user.home}}")
    public String downlDir;


    @Autowired
    SuComuneRepository suComuneRepository;

    @Override
    @Transactional
    public List<SuComune> elencaSuComuni() {
        return (List<SuComune>) suComuneRepository.findAll();
    }

    @Override
    @Transactional
    public SuComune caricaSingoloElemento(Long input) {
        return suComuneRepository.findById(input).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(SuComune suComune) {
        suComuneRepository.save(suComune);
    }

    @Override
    @Transactional
    public void aggiorna(SuComune suComune) {
        suComuneRepository.save(suComune);
    }

    @Override
    @Transactional
    public void rimuovi(SuComune suComune) {
        suComuneRepository.delete(suComune);
    }

    @Override
    @Transactional
    public void inserisciNuovoIstat() {

    }
}
