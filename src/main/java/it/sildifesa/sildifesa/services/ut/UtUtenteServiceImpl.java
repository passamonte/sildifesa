package it.sildifesa.sildifesa.services.ut;

import it.sildifesa.sildifesa.dao.ut.UtRoleRepository;
import it.sildifesa.sildifesa.dao.ut.UtUtenteRepository;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.ut.UtRole;
import it.sildifesa.sildifesa.models.ut.UtUtente;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
public class UtUtenteServiceImpl implements UtUtenteService, UserDetailsService {

    private static final Logger logger = LogManager.getLogger(UtUtenteServiceImpl.class);


    @Autowired
    private UtUtenteRepository utUtenteRepository;
    @Autowired
    private UtRoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

//    @Override
//    public void save(UtUtente user) {
//        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//        user.setRoles(new List<UtRole>(roleRepository.findAll()) {
//        });
//        utUtenteRepository.save(user);
//    }

    @Override
    @Transactional
    public UtUtente findByUsername(String username) {
        return utUtenteRepository.findByUsername(username);
    }


    @Override
    @Transactional
    public void insertNewUtente(UtUtente utente) {
        utente.setPassword(bCryptPasswordEncoder.encode(utente.getPassword()));


        /////// GLI UTENTI REGISTRATI SARANNO ASSEGNATI CON RUOLO "VOLONTARIO" ////////
        utente.setRoles(new ArrayList<UtRole>(roleRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
            jpaCriteriaList.add(root.get("name").in("VOLONTARIO_ROLE"));
            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        })
        ));
        utUtenteRepository.save(utente);
    }

    @Override
    @Transactional
    public void registrazioneUtente(UtUtente primoUtente) {
        utUtenteRepository.save(primoUtente);
    }

    @Override
    @Transactional
    public List<UtUtente> getElencoUtenti() {
        return (List<UtUtente>) utUtenteRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        UtUtente user = utUtenteRepository.findByUsername(username);
        if (user == null) throw new UsernameNotFoundException(username);
        if (!user.getAbilitato()) throw new InternalAuthenticationServiceException(username);

        user.setDataUltimoAccesso(new Date());
        utUtenteRepository.save(user);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (UtRole role : user.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }


    @Override
    @Transactional
    public boolean controlloEsistenzaUsername(String username) {
        Integer utentiPresenti = utUtenteRepository.countByUsernameEqual(username);
        if (utentiPresenti > 0) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public UtUtente trovaUtentePerMail(String email) {
        return utUtenteRepository.findByEmailEqual(email);

    }

    @Override
    @Transactional
    public void aggiorna(UtUtente utUtente) {
        utUtenteRepository.save(utUtente);
    }

    @Override
    @Transactional
    public void resetPassword(UtUtente utenteVolontario, String nuovaPassword) {

        utenteVolontario.setPassword(bCryptPasswordEncoder.encode(nuovaPassword));
        utenteVolontario.setDataModificaPassword(new Date());
        utenteVolontario.setTokenConferma(null);
        utUtenteRepository.save(utenteVolontario);


    }


    @Override
    @Transactional
    public Page<UtUtente> elencaUtUtentiPaginati(Pageable pageable) {
        return utUtenteRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> jpaCriteriaList = new ArrayList<>();
//            if (Long.class != criteriaQuery.getResultType()) {
//                root.fetch("ut_utente_ut_roles", JoinType.LEFT);
//            }
//            jpaCriteriaList.add(root.get("attivo").in(true));

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);
    }

    @Override
    @Transactional
    public boolean controlloEsistenzaUsernameByEmail(String email) {
        Integer utentiPresenti = utUtenteRepository.countByEmailEqual(email);
        if (utentiPresenti > 0) {
            return true;
        }
        return false;
    }
}
