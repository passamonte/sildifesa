package it.sildifesa.sildifesa.services.ut;

import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.ut.UtUtente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UtUtenteService {

    void insertNewUtente(UtUtente utente);

    void registrazioneUtente(UtUtente primoUtente);

    List<UtUtente> getElencoUtenti();

    UtUtente findByUsername(String username);

    boolean controlloEsistenzaUsername(String username);

    boolean controlloEsistenzaUsernameByEmail(String email);

    public UtUtente trovaUtentePerMail(String email);

    Page<UtUtente> elencaUtUtentiPaginati(Pageable pageable);


    public void aggiorna(UtUtente utUtente);

    @Transactional
    void resetPassword(UtUtente utenteVolontario, String nuovaPassword);
}
