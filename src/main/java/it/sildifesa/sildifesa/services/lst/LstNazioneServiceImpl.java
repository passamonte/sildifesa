package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstNazioneRepository;
import it.sildifesa.sildifesa.models.lst.LstGrado;
import it.sildifesa.sildifesa.models.lst.LstNazione;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstNazioneServiceImpl implements LstNazioneService {

    @Autowired
    LstNazioneRepository lstNazioneRepository;

    @Override
    @Transactional
    public List<LstNazione> elencaLstNazione() {
        return (List<LstNazione>) lstNazioneRepository.findAll();
    }

    @Override
    @Transactional
    public LstNazione caricaSingoloElemento(Long idInput) {
        return lstNazioneRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstNazione lstNazione) {
        lstNazioneRepository.save(lstNazione);
    }

    @Override
    @Transactional
    public void aggiorna(LstNazione lstNazione) {
        lstNazioneRepository.save(lstNazione);
    }

    @Override
    @Transactional
    public void rimuovi(LstNazione lstNazione) {
        lstNazioneRepository.delete(lstNazione);
    }


    @Override
    @Transactional
    public Page<LstNazione> findPaginated(Pageable pageable) {


        List<LstNazione> listaNazione = (List<LstNazione>) lstNazioneRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstNazione> list;

        if (listaNazione.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaNazione.size());
            list = listaNazione.subList(startItem, toIndex);
        }

        Page<LstNazione> listaNazionePage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaNazione.size());

        return listaNazionePage;
    }
}
