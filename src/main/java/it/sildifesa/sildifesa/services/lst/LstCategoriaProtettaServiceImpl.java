package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstCategoriaProtettaRepository;
import it.sildifesa.sildifesa.models.lst.LstCategoriaProtetta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstCategoriaProtettaServiceImpl implements LstCategoriaProtettaService{


    @Autowired
    LstCategoriaProtettaRepository lstCategoriaProtettaRepository;

    @Override
    @Transactional
    public List<LstCategoriaProtetta> elencaLstCategoriaProtetta() {
        return (List<LstCategoriaProtetta>) lstCategoriaProtettaRepository.findAll();
    }

    @Override
    @Transactional
    public LstCategoriaProtetta caricaSingoloElemento(Long idInput) {
        return lstCategoriaProtettaRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstCategoriaProtetta lstCategoriaProtetta) {
        lstCategoriaProtettaRepository.save(lstCategoriaProtetta);
    }

    @Override
    @Transactional
    public void aggiorna(LstCategoriaProtetta lstCategoriaProtetta) {
        lstCategoriaProtettaRepository.save(lstCategoriaProtetta);
    }

    @Override
    @Transactional
    public void rimuovi(LstCategoriaProtetta lstCategoriaProtetta) {
        lstCategoriaProtettaRepository.delete(lstCategoriaProtetta);
    }

    @Override
    @Transactional
    public Page<LstCategoriaProtetta> findPaginated(Pageable pageable) {


        List<LstCategoriaProtetta> listaCategoriaProtetta = (List<LstCategoriaProtetta>) lstCategoriaProtettaRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstCategoriaProtetta> list;

        if (listaCategoriaProtetta.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaCategoriaProtetta.size());
            list = listaCategoriaProtetta.subList(startItem, toIndex);
        }

        Page<LstCategoriaProtetta> listaCategoriaProtettaPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaCategoriaProtetta.size());

        return listaCategoriaProtettaPage;
    }

}
