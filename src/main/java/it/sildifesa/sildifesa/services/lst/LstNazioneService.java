package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import it.sildifesa.sildifesa.models.lst.LstGrado;
import it.sildifesa.sildifesa.models.lst.LstNazione;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstNazioneService {

    public List<LstNazione> elencaLstNazione();

    public LstNazione caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstNazione lstNazione);

    public void aggiorna(LstNazione lstNazione);

    public void rimuovi(LstNazione lstNazione);

    public Page<LstNazione> findPaginated(Pageable pageable);
}
