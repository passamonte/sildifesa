package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstRuoloRepository;
import it.sildifesa.sildifesa.models.lst.LstRuolo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstRuoloServiceImpl implements LstRuoloService {

    @Autowired
    LstRuoloRepository lstRuoloRepository;

    @Override
    @Transactional
    public List<LstRuolo> elencaLstRuolo() {
        return (List<LstRuolo>) lstRuoloRepository.findAll();
    }

    @Override
    @Transactional
    public LstRuolo caricaSingoloElemento(Long idInput) {
        return lstRuoloRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstRuolo lstRuolo) {
        lstRuoloRepository.save(lstRuolo);
    }

    @Override
    @Transactional
    public void aggiorna(LstRuolo lstRuolo) {
        lstRuoloRepository.save(lstRuolo);
    }

    @Override
    @Transactional
    public void rimuovi(LstRuolo lstRuolo) {
        lstRuoloRepository.delete(lstRuolo);
    }

    @Override
    @Transactional
    public Page<LstRuolo> findPaginated(Pageable pageable) {


        List<LstRuolo> listaRuolo = (List<LstRuolo>) lstRuoloRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstRuolo> list;

        if (listaRuolo.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaRuolo.size());
            list = listaRuolo.subList(startItem, toIndex);
        }

        Page<LstRuolo> listaRuoloPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaRuolo.size());

        return listaRuoloPage;
    }
}
