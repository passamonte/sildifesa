package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstDistrettoMilitareService {


    public List<LstDistrettoMilitare> elencaLstDistrettoMilitare();

    public LstDistrettoMilitare caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstDistrettoMilitare lstDistrettoMilitare);

    public void aggiorna(LstDistrettoMilitare lstDistrettoMilitare);

    public void rimuovi(LstDistrettoMilitare lstDistrettoMilitare);

    public Page<LstDistrettoMilitare> findPaginated(Pageable pageable);


}
