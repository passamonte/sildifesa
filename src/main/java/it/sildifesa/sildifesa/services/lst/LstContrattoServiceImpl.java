package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstContrattoRepository;
import it.sildifesa.sildifesa.models.lst.LstContratto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstContrattoServiceImpl implements LstContrattoService {

    @Autowired
    LstContrattoRepository lstContrattoRepository;

    @Override
    @Transactional
    public List<LstContratto> elencaLstContratto() {
        return (List<LstContratto>) lstContrattoRepository.findAll();
    }

    @Override
    @Transactional
    public LstContratto caricaSingoloElemento(Long idInput) {
        return lstContrattoRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstContratto lstContratto) {
        lstContrattoRepository.save(lstContratto);
    }

    @Override
    @Transactional
    public void aggiorna(LstContratto lstContratto) {
        lstContrattoRepository.save(lstContratto);
    }

    @Override
    @Transactional
    public void rimuovi(LstContratto lstContratto) {
        lstContrattoRepository.delete(lstContratto);
    }

    @Override
    @Transactional
    public Page<LstContratto> findPaginated(Pageable pageable) {

        List<LstContratto> listaCategoriaProtetta = (List<LstContratto>) lstContrattoRepository.findAll();

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstContratto> list;

        if (listaCategoriaProtetta.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaCategoriaProtetta.size());
            list = listaCategoriaProtetta.subList(startItem, toIndex);
        }

        Page<LstContratto> listaCategoriaProtettaPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaCategoriaProtetta.size());

        return listaCategoriaProtettaPage;
    }
}
