package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dao.lst.LstArmaRepository;
import it.sildifesa.sildifesa.models.lst.LstArma;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class LstArmaServiceImpl implements LstArmaService{

    @Autowired
    LstArmaRepository lstArmaRepository;

    @Override
    @Transactional
    public List<LstArma> elencaLstArma() {
        return (List<LstArma>) lstArmaRepository.findAll();
    }

    @Override
    @Transactional
    public LstArma caricaSingoloElemento(Long idInput) {
        return lstArmaRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstArma lstArma) {
        lstArmaRepository.save(lstArma);
    }

    @Override
    @Transactional
    public void aggiorna(LstArma lstArma) {
        lstArmaRepository.save(lstArma);
    }

    @Override
    @Transactional
    public void rimuovi(LstArma lstArma) {
        lstArmaRepository.delete(lstArma);
    }

    @Override
    @Transactional
    public Page<LstArma> findPaginated(LstArma arma, Pageable pageable) {
        return lstArmaRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Objects.nonNull(arma.getNome())) {
                jpaCriteriaList.add(criteriaBuilder.like(root.get("nome"), "%" + arma.getNome() + "%"));
            }

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);
    }

    public List<String> findDistinctByForzaArmata(){
        return lstArmaRepository.findDistinctByForzaArmata();
    }

    public List<LstArma> findAllByForzaArmata(String forzaArmata){
        return lstArmaRepository.findAllByForzaArmata(forzaArmata);
    }

}
