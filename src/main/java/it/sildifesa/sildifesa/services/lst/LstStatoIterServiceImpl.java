package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstStatoIterRepository;
import it.sildifesa.sildifesa.models.lst.LstStatoIter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstStatoIterServiceImpl implements LstStatoIterService {

    @Autowired
    LstStatoIterRepository lstStatoIterRepository;

    @Override
    @Transactional
    public List<LstStatoIter> elencaLstStatoIter() {
        return (List<LstStatoIter>) lstStatoIterRepository.findAll();
    }

    @Override
    @Transactional
    public LstStatoIter caricaSingoloElemento(Long idInput) {
        return lstStatoIterRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstStatoIter lstStatoIter) {
        lstStatoIterRepository.save(lstStatoIter);
    }

    @Override
    @Transactional
    public void aggiorna(LstStatoIter lstStatoIter) {
        lstStatoIterRepository.save(lstStatoIter);
    }

    @Override
    @Transactional
    public void rimuovi(LstStatoIter lstStatoIter) {
        lstStatoIterRepository.delete(lstStatoIter);
    }

    @Override
    @Transactional
    public Page<LstStatoIter> findPaginated(Pageable pageable) {


        List<LstStatoIter> listaStatoIter = (List<LstStatoIter>) lstStatoIterRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstStatoIter> list;

        if (listaStatoIter.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaStatoIter.size());
            list = listaStatoIter.subList(startItem, toIndex);
        }

        Page<LstStatoIter> listaStatoIterPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaStatoIter.size());

        return listaStatoIterPage;
    }
}
