package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstStatoIter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstStatoIterService {
    public List<LstStatoIter> elencaLstStatoIter();

    public LstStatoIter caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstStatoIter lstStatoIter);

    public void aggiorna(LstStatoIter lstStatoIter);

    public void rimuovi(LstStatoIter lstStatoIter);

    public Page<LstStatoIter> findPaginated(Pageable pageable);
}
