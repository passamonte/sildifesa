package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstTipoRapportoRepository;
import it.sildifesa.sildifesa.models.lst.LstTipoRapporto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LstTipoRapportoServiceImpl implements LstTipoRapportoService {
    @Autowired
    LstTipoRapportoRepository lstTipoRapportoRepository;

    @Override
    @Transactional
    public List<LstTipoRapporto> elencaLstTipoRapporto() {
        return (List<LstTipoRapporto>) lstTipoRapportoRepository.findAll();
    }

    @Override
    @Transactional
    public LstTipoRapporto caricaSingoloElemento(Long idInput) {
        return lstTipoRapportoRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstTipoRapporto lstTipoRapporto) {
        lstTipoRapportoRepository.save(lstTipoRapporto);
    }

    @Override
    @Transactional
    public void aggiorna(LstTipoRapporto lstTipoRapporto) {
        lstTipoRapportoRepository.save(lstTipoRapporto);
    }

    @Override
    @Transactional
    public void rimuovi(LstTipoRapporto lstTipoRapporto) {
        lstTipoRapportoRepository.delete(lstTipoRapporto);
    }
}
