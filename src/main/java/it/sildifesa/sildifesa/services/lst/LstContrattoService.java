package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstContratto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstContrattoService  {


    public List<LstContratto> elencaLstContratto();

    public LstContratto caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstContratto lstContratto);

    public void aggiorna(LstContratto lstContratto);

    public void rimuovi(LstContratto lstContratto);

    public Page<LstContratto> findPaginated(Pageable pageable);
}
