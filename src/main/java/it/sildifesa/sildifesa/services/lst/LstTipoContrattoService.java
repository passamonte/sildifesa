package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstTipoContrattoService {


    public List<LstTipoContratto> elencaLstTipoContratto();

    public LstTipoContratto caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstTipoContratto lstTipoContratto);

    public void aggiorna(LstTipoContratto lstTipoContratto);

    public void rimuovi(LstTipoContratto lstTipoContratto);


}
