package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstRuolo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstRuoloService {

    public List<LstRuolo> elencaLstRuolo();

    public LstRuolo caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstRuolo lstRuolo);

    public void aggiorna(LstRuolo lstRuolo);

    public void rimuovi(LstRuolo lstRuolo);

    public Page<LstRuolo> findPaginated(Pageable pageable);

}
