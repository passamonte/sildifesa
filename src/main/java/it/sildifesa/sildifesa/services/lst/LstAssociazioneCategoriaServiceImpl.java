package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstAssociazioneCategoriaRepository;
import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstAssociazioneCategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
@Service
public class LstAssociazioneCategoriaServiceImpl implements LstAssociazioneCategoriaService {

    @Autowired
    LstAssociazioneCategoriaRepository lstAssociazioneCategoriaRepository;

    @Override
    @Transactional
    public List<LstAssociazioneCategoria> elencaLstAssociazioneCategoria() {
        return (List<LstAssociazioneCategoria>) lstAssociazioneCategoriaRepository.findAll();
    }

    @Override
    @Transactional
    public LstAssociazioneCategoria caricaSingoloElemento(Long idInput) {
        return lstAssociazioneCategoriaRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstAssociazioneCategoria lstAssociazioneCategoria) {
        lstAssociazioneCategoriaRepository.save(lstAssociazioneCategoria);
    }

    @Override
    @Transactional
    public void aggiorna(LstAssociazioneCategoria lstAssociazioneCategoria) {
        lstAssociazioneCategoriaRepository.save(lstAssociazioneCategoria);
    }

    @Override
    @Transactional
    public void rimuovi(LstAssociazioneCategoria lstAssociazioneCategoria) {
        lstAssociazioneCategoriaRepository.delete(lstAssociazioneCategoria);
    }


    @Override
    @Transactional
    public Page<LstAssociazioneCategoria> findPaginated(Pageable pageable) {


        List<LstAssociazioneCategoria> listaAssCat = (List<LstAssociazioneCategoria>) lstAssociazioneCategoriaRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstAssociazioneCategoria> list;

        if (listaAssCat.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaAssCat.size());
            list = listaAssCat.subList(startItem, toIndex);
        }

        Page<LstAssociazioneCategoria> listaAssCatPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaAssCat.size());

        return listaAssCatPage;
    }
}
