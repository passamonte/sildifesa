package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstAssociazioneCategoria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstAssociazioneCategoriaService {

    public List<LstAssociazioneCategoria> elencaLstAssociazioneCategoria();

    public LstAssociazioneCategoria caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstAssociazioneCategoria lstAssociazioneCategoria);

    public void aggiorna(LstAssociazioneCategoria lstAssociazioneCategoria);

    public void rimuovi(LstAssociazioneCategoria lstAssociazioneCategoria);

    public Page<LstAssociazioneCategoria> findPaginated(Pageable pageable);


}
