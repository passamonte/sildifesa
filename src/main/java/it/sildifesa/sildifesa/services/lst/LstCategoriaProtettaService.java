package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstCategoriaProtetta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstCategoriaProtettaService {


    public List<LstCategoriaProtetta> elencaLstCategoriaProtetta();

    public LstCategoriaProtetta caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstCategoriaProtetta lstCategoriaProtetta);

    public void aggiorna(LstCategoriaProtetta lstCategoriaProtetta);

    public void rimuovi(LstCategoriaProtetta lstCategoriaProtetta);

    public Page<LstCategoriaProtetta> findPaginated(Pageable pageable);
}
