package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstSupportoFaRepository;
import it.sildifesa.sildifesa.models.lst.LstSupportoFa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstSupportoFaServiceImpl implements LstSupportoFaService {
    @Autowired
    LstSupportoFaRepository lstSupportoFaRepository;

    @Override
    @Transactional
    public List<LstSupportoFa> elencaLstSupportoFa() {
        return (List<LstSupportoFa>) lstSupportoFaRepository.findAll();
    }

    @Override
    @Transactional
    public LstSupportoFa caricaSingoloElemento(Long idInput) {
        return lstSupportoFaRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstSupportoFa lstSupportoFa) {
        lstSupportoFaRepository.save(lstSupportoFa);
    }

    @Override
    @Transactional
    public void aggiorna(LstSupportoFa lstSupportoFa) {
        lstSupportoFaRepository.save(lstSupportoFa);
    }

    @Override
    @Transactional
    public void rimuovi(LstSupportoFa lstSupportoFa) {
        lstSupportoFaRepository.delete(lstSupportoFa);
    }

    @Override
    @Transactional
    public Page<LstSupportoFa> findPaginated(Pageable pageable) {


        List<LstSupportoFa> listaSupportoFa = (List<LstSupportoFa>) lstSupportoFaRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstSupportoFa> list;

        if (listaSupportoFa.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaSupportoFa.size());
            list = listaSupportoFa.subList(startItem, toIndex);
        }

        Page<LstSupportoFa> listaSupportoFaPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaSupportoFa.size());

        return listaSupportoFaPage;
    }
}
