package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstSupportoFa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstSupportoFaService  {

    public List<LstSupportoFa> elencaLstSupportoFa();

    public LstSupportoFa caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstSupportoFa lstSupportoFa);

    public void aggiorna(LstSupportoFa lstSupportoFa);

    public void rimuovi(LstSupportoFa lstSupportoFa);

    public Page<LstSupportoFa> findPaginated(Pageable pageable);
}
