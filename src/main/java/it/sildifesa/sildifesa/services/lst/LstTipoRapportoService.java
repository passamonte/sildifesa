package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import it.sildifesa.sildifesa.models.lst.LstTipoRapporto;

import java.util.List;

public interface LstTipoRapportoService {
    public List<LstTipoRapporto> elencaLstTipoRapporto();

    public LstTipoRapporto caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstTipoRapporto lstTipoRapporto);

    public void aggiorna(LstTipoRapporto lstTipoRapporto);

    public void rimuovi(LstTipoRapporto lstTipoRapporto);

}
