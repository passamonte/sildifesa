package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstGrado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstGradoService {

    List<LstGrado> elencaLstGrado();

    LstGrado caricaSingoloElemento(Long idInput);

    void inserisciNuovo(LstGrado lstGrado);

    void aggiorna(LstGrado lstGrado);

    void rimuovi(LstGrado lstGrado);

    Page<LstGrado> findPaginated(LstGrado grado, Pageable pageable);

}
