package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstContrattoRepository;
import it.sildifesa.sildifesa.dao.lst.LstTipoContrattoRepository;
import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LstTipoContrattoServiceImpl implements LstTipoContrattoService {

    @Autowired
    LstTipoContrattoRepository lstTipoContrattoRepository;

    @Override
    @Transactional
    public List<LstTipoContratto> elencaLstTipoContratto() {
        return (List<LstTipoContratto>) lstTipoContrattoRepository.findAll();
    }

    @Override
    @Transactional
    public LstTipoContratto caricaSingoloElemento(Long idInput) {
        return lstTipoContrattoRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstTipoContratto lstTipoContratto) {
        lstTipoContrattoRepository.save(lstTipoContratto);
    }

    @Override
    @Transactional
    public void aggiorna(LstTipoContratto lstTipoContratto) {
        lstTipoContrattoRepository.save(lstTipoContratto);
    }

    @Override
    @Transactional
    public void rimuovi(LstTipoContratto lstTipoContratto) {
        lstTipoContrattoRepository.delete(lstTipoContratto);
    }
}