package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstArma;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstArmaService {

    List<LstArma> elencaLstArma();

    LstArma caricaSingoloElemento(Long idInput);

    void inserisciNuovo(LstArma lstArma);

    void aggiorna(LstArma lstArma);

    void rimuovi(LstArma lstArma);

    Page<LstArma> findPaginated(LstArma arma, Pageable pageable);

    List<String> findDistinctByForzaArmata();

    List<LstArma> findAllByForzaArmata(String forzaArmata);

}
