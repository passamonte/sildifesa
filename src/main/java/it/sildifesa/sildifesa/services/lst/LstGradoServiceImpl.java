package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dao.lst.LstGradoRepository;
import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstGrado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class LstGradoServiceImpl implements LstGradoService {

    @Autowired
    LstGradoRepository lstGradoRepository;

    @Override
    @Transactional
    public List<LstGrado> elencaLstGrado() {
        return lstGradoRepository.findAll();
    }


    @Override
    @Transactional
    public LstGrado caricaSingoloElemento(Long idInput) {
        Optional<LstGrado> grado = lstGradoRepository.findById(idInput);
        return grado.isPresent()?grado.get():null;
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstGrado lstGrado) {
        lstGradoRepository.save(lstGrado);
    }

    @Override
    @Transactional
    public void aggiorna(LstGrado lstGrado) {
        lstGradoRepository.save(lstGrado);
    }

    @Override
    @Transactional
    public void rimuovi(LstGrado lstGrado) {
        lstGradoRepository.delete(lstGrado);
    }


    @Override
    @Transactional
    public Page<LstGrado> findPaginated(LstGrado grado, Pageable pageable) {
        return lstGradoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> jpaCriteriaList = new ArrayList<>();
            if (Objects.nonNull(grado.getNome())) {
                jpaCriteriaList.add(criteriaBuilder.like(root.get("nome"), "%" + grado.getNome() + "%"));
            }

            return criteriaBuilder.and(jpaCriteriaList.toArray(new Predicate[0]));
        }, pageable);
    }

}
