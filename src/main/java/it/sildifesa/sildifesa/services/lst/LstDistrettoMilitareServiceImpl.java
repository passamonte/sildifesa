package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstDistrettoMilitareRepository;
import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstDistrettoMilitareServiceImpl implements LstDistrettoMilitareService {

    @Autowired
    LstDistrettoMilitareRepository lstDistrettoMilitareRepository;

    @Override
    @Transactional
    public List<LstDistrettoMilitare> elencaLstDistrettoMilitare() {
        return (List<LstDistrettoMilitare>) lstDistrettoMilitareRepository.findAll();
    }

    @Override
    @Transactional
    public LstDistrettoMilitare caricaSingoloElemento(Long idInput) {
        return lstDistrettoMilitareRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstDistrettoMilitare lstDistrettoMilitare) {
        lstDistrettoMilitareRepository.save(lstDistrettoMilitare);
    }

    @Override
    @Transactional
    public void aggiorna(LstDistrettoMilitare lstDistrettoMilitare) {
        lstDistrettoMilitareRepository.save(lstDistrettoMilitare);
    }

    @Override
    @Transactional
    public void rimuovi(LstDistrettoMilitare lstDistrettoMilitare) {
        lstDistrettoMilitareRepository.delete(lstDistrettoMilitare);
    }

    @Override
    @Transactional
    public Page<LstDistrettoMilitare> findPaginated(Pageable pageable) {


        List<LstDistrettoMilitare> listaDistrettoMilitare = (List<LstDistrettoMilitare>) lstDistrettoMilitareRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstDistrettoMilitare> list;

        if (listaDistrettoMilitare.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaDistrettoMilitare.size());
            list = listaDistrettoMilitare.subList(startItem, toIndex);
        }

        Page<LstDistrettoMilitare> listaDistrettoMilitarePage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaDistrettoMilitare.size());

        return listaDistrettoMilitarePage;
    }
}
