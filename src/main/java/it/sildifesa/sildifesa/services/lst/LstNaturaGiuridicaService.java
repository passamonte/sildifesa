package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.models.lst.LstNaturaGiuridica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LstNaturaGiuridicaService {

    public List<LstNaturaGiuridica> elencaLstNaturaGiuridica();

    public LstNaturaGiuridica caricaSingoloElemento(Long idInput);

    public void inserisciNuovo(LstNaturaGiuridica lstNaturaGiuridica);

    public void aggiorna(LstNaturaGiuridica lstNaturaGiuridica);

    public void rimuovi(LstNaturaGiuridica lstNaturaGiuridica);

    public Page<LstNaturaGiuridica> findPaginated(Pageable pageable);
}
