package it.sildifesa.sildifesa.services.lst;

import it.sildifesa.sildifesa.dao.lst.LstNaturaGiuridicaRepository;
import it.sildifesa.sildifesa.models.lst.LstNaturaGiuridica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class LstNaturaGiuridicaServiceImpl implements LstNaturaGiuridicaService {
    @Autowired
    LstNaturaGiuridicaRepository lstNaturaGiuridicaRepository;

    @Override
    @Transactional
    public List<LstNaturaGiuridica> elencaLstNaturaGiuridica() {
        return (List<LstNaturaGiuridica>) lstNaturaGiuridicaRepository.findAll();
    }

    @Override
    @Transactional
    public LstNaturaGiuridica caricaSingoloElemento(Long idInput) {
        return lstNaturaGiuridicaRepository.findById(idInput).get();
    }

    @Override
    @Transactional
    public void inserisciNuovo(LstNaturaGiuridica lstNaturaGiuridica) {
        lstNaturaGiuridicaRepository.save(lstNaturaGiuridica);
    }

    @Override
    @Transactional
    public void aggiorna(LstNaturaGiuridica lstNaturaGiuridica) {
        lstNaturaGiuridicaRepository.save(lstNaturaGiuridica);
    }

    @Override
    @Transactional
    public void rimuovi(LstNaturaGiuridica lstNaturaGiuridica) {
        lstNaturaGiuridicaRepository.delete(lstNaturaGiuridica);
    }

    @Override
    @Transactional
    public Page<LstNaturaGiuridica> findPaginated(Pageable pageable) {

        List<LstNaturaGiuridica> listaNaturaGiuridica = (List<LstNaturaGiuridica>) lstNaturaGiuridicaRepository.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LstNaturaGiuridica> list;

        if (listaNaturaGiuridica.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, listaNaturaGiuridica.size());
            list = listaNaturaGiuridica.subList(startItem, toIndex);
        }

        Page<LstNaturaGiuridica> listaNaturaGiuridicaPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), listaNaturaGiuridica.size());

        return listaNaturaGiuridicaPage;
    }
}
