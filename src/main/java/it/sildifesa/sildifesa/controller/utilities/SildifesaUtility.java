package it.sildifesa.sildifesa.controller.utilities;

import it.sildifesa.sildifesa.dto.*;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import it.sildifesa.sildifesa.models.lst.LstContratto;
import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import it.sildifesa.sildifesa.models.su.SuComune;
import it.sildifesa.sildifesa.models.su.SuProfessione;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SildifesaUtility {
    private static final Logger LOGGER = LogManager.getLogger(SildifesaUtility.class);

    public static final int NUM_PAGINE_DA_MOSTRARE = 5;
    public static final int NUM_PAGINE_PRECEDENTI = 2;

    public static final List<String> UPDATE_ANAG_VOLONTARI = Arrays.asList("nome", "cognome", "dataNascita", "sesso", "numeroTelefono", "suComune1", "codiceFiscale", "numeroFigli", "statoCivile", "caAnagraficaTitolostudios", "indirizzoResidenza", "suComune2", "capComuneResidenza", "indirizzoDomicilio", "suComune3", "capDomicilio", "caDocumentoIdentita", "email", "forzaArmata", "arma", "gradoMilitare", "suSpecialita", "statoGiuridico", "tipoFerma", "rafferma", "dataIncorporazione", "dataCongedo");
    public static final List<String> UPDATE_ANAG_AZIENDA = Arrays.asList("ragioneSociale", "partitaIva", "dataAttoDiCostituzione", "nomeReferente", "numeroTelefono", "fax", "comuneSedeLegale", "indirizzoSedeLegale", "capSedeLegale", "tipologiaAzienda", "email", "emailPec", "caDocumentoAzienda");
    public static final List<String> UPDATE_CORSI = Arrays.asList("titolo", "numeroPosti", "descrizione", "tirocinio", "riferimento", "sezioneTerritorialeCoordinatrice", "annoFinanziario", "dataInizio", "dataFine", "durata", "dataInizioCandidatura", "dataFineCandidatura", "titoloStudioRichiesto", "competenzeRichieste", "foDocumentoCorso", "certificazioni", "benefit", "caAnagraficaAzienda", "emailReferente", "telefonoReferente", "indirizzoSede", "localitaSedeCorso", "capSede");
    public static final List<String> UPDATE_OFFERTE = Arrays.asList("anno", "areaFunzionale", "areaFunzionaleLettera", "chiusa",  "compenso", "concorso" , "conteggiaRiserva", "cpi", "data", "dataFine", "descrizione", "descrizioneAttivita", "descrizioneofferta", "diffusione", "azienda", "livelloContratto", "localitaSedeOfferta", "competenzeRichieste", "note", "numeroPosti", "numeroPostiRiservati", "numeroRif", "previstaRiserva", "professione", "regioneRif", "rilievo", "rilievoMassDir", "riservaDiLegge", "rispostarilievo", "rispostarilievomassdir", "sede", "settoreContratto", "tipologiaRapporto", "tipologiaRif", "tipoOffertaLavoro", "titoloStudioRichiesto", "attivo", "cccnlApplicato", "compensoMensile", "laOffertaTipoContratti");
    public static <T> void managePagination(Model model, Page<T> listaPaginata, int currentPage) {

        int totalPages = listaPaginata.getTotalPages();
        model.addAttribute("lista", listaPaginata);

        if (totalPages > 0) {

            int pageStart;
            int pageEnd;
            int delta = totalPages - currentPage;

            if (totalPages > NUM_PAGINE_DA_MOSTRARE && delta >= 0 && delta < NUM_PAGINE_PRECEDENTI + 1) {
                pageStart = totalPages - NUM_PAGINE_DA_MOSTRARE;
                pageEnd = totalPages;
            } else {
                pageStart = Math.max(0, currentPage - NUM_PAGINE_PRECEDENTI);
                pageEnd = Math.min(pageStart + 5, totalPages);
            }


            List<Integer> partialPageNumbers = IntStream.rangeClosed(pageStart + 1, pageEnd)
                    .boxed()
                    .collect(Collectors.toList());

            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());

            model.addAttribute("pageNumbers", pageNumbers);
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("totalPages", totalPages);

            model.addAttribute("partialPageNumbers", partialPageNumbers);
            model.addAttribute("pageStart", pageStart);
            model.addAttribute("pageEnd", pageEnd);
        }

    }
/*
    public static void managePaginationBkp(Model model, int totalPages, int currentPage) {

        int pageStart;
        int pageEnd;
        int delta = totalPages - currentPage;

        if (totalPages > NUM_PAGINE_DA_MOSTRARE && delta >= 0 && delta < NUM_PAGINE_PRECEDENTI + 1) {
            pageStart = totalPages - NUM_PAGINE_DA_MOSTRARE;
            pageEnd = totalPages;
        } else {
            pageStart = Math.max(0, currentPage - NUM_PAGINE_PRECEDENTI);
            pageEnd = Math.min(pageStart + 5, totalPages);
        }


        List<Integer> partialPageNumbers = IntStream.rangeClosed(pageStart + 1, pageEnd)
                .boxed()
                .collect(Collectors.toList());

        model.addAttribute("partialPageNumbers", partialPageNumbers);
        model.addAttribute("pageStart", pageStart);
        model.addAttribute("pageEnd", pageEnd);

    }*/


    public static <T> T controlloOggettoVuoto(T obj) {

        for (Field field : obj.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.get(obj) != null) {

                    LOGGER.info("L'Oggetto non è vuoto");
                    return obj;
                }
            } catch (Exception e) {
                LOGGER.error("Errore nella verifica");
            }
        }
        LOGGER.info("L'Oggetto è vuoto e verrà settato a NULL");
        obj = null;
        return obj;
    }


    public static List<CaAnagraficaTitoloDiStudio> controlloTitoliStudio(List<CaAnagraficaTitoloDiStudio> lista) {
        List<CaAnagraficaTitoloDiStudio> listaDaRitornare = new ArrayList<>();
        AtomicReference<Boolean> flag = new AtomicReference<>(true);
        lista.forEach(item -> {
            flag.set(true);
            if (listaDaRitornare.size() == 0) {
                listaDaRitornare.add(item);
            }
            listaDaRitornare.forEach(value -> {
                if (value.getSuTitoloStudio().getId().equals(item.getSuTitoloStudio().getId())) {
                    flag.set(false);
                }
            });
            if (flag.get()) {
                listaDaRitornare.add(item);
            }

        });
        return listaDaRitornare;
    }
    public static List<CaAnagraficaTitoloDiStudioDto> controlloTitoliStudioDto(List<CaAnagraficaTitoloDiStudioDto> lista) {
        List<CaAnagraficaTitoloDiStudioDto> listaDaRitornare = new ArrayList<>();
        AtomicReference<Boolean> flag = new AtomicReference<>(true);
        lista.forEach(item -> {
            flag.set(true);
            if (listaDaRitornare.size() == 0) {
                listaDaRitornare.add(item);
            }
            listaDaRitornare.forEach(value -> {
                if (value.getSuTitoloStudio().getId().equals(item.getSuTitoloStudio().getId())) {
                    flag.set(false);
                }
            });
            if (flag.get()) {
                listaDaRitornare.add(item);
            }

        });
        return listaDaRitornare;
    }

    // function to generate a random string of length n
    public static String generateToken() {
        int n = 32;
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();

    }

    public static String fromNullToString(Object field) {
        if (Objects.nonNull(field)){
            if (field.toString().equals("true")){
                return "Si";
            }
            if (field.toString().equals("false")){
                return "No";
            }
            return field.toString();
        }else{
            return "Nessun valore inserito";
        }
    }

    public static <T> Page<T> listToPage(List<T> lista, Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<T> list;

        if (lista.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, lista.size());
            list = lista.subList(startItem, toIndex);
        }

        Page<T> listaAnagraficaPage
                = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), lista.size());

        return listaAnagraficaPage;
    }


    public static String controllaUsername(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }

    public static String titoloStudioUtil(Integer item){
        if (Objects.isNull(item))
            return "Nessun Valore Inserito";
        switch (item){
            case 0:
                return "Nessuno";
            case 10:
                return "Elementare";
            case 20:
                return "Media";
            case 30:
                return "Diploma";
            case 60:
                return "Laurea";
        }
        return "Nessun Valore Inserito";
    }

    public static Object setObjectFromFront(Object destinatario, Object mittente, List<String> campi){
        Class classe = destinatario.getClass();
        try {
            for (String item : campi) {
                Field field = classe.getDeclaredField(item);
                field.setAccessible(true);
                field.set(destinatario, field.get(mittente));
            }
        } catch(NoSuchFieldException | IllegalAccessException e){
            LOGGER.warn("Errore in fase di conversione oggetto da FrontEnd", e);
        }
        return destinatario;
    }

    public static boolean bindMultiselect(LstTipoContratto contratto, Set<LstTipoContrattoDto> lista){
        return Objects.nonNull(lista) && lista.stream().filter(item -> contratto.getId().equals(item.getId())).collect(Collectors.toSet()).size() > 0;
    }

}
