package it.sildifesa.sildifesa.controller;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.*;
import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;
import it.sildifesa.sildifesa.models.ca.CaDocumentoIdentita;
import it.sildifesa.sildifesa.services.ca.CaAnagraficaService;
import it.sildifesa.sildifesa.services.ca.CaAnagraficaTitoloDiStudioService;
import it.sildifesa.sildifesa.services.ca.CaDocumentoIdentitaService;
import it.sildifesa.sildifesa.services.ca.CaTipoDocumentoIdentitaService;
import it.sildifesa.sildifesa.services.lst.LstArmaService;
import it.sildifesa.sildifesa.services.rp.RpCodificaLivelloTitoloService;
import it.sildifesa.sildifesa.services.su.SuComuneService;
import it.sildifesa.sildifesa.services.su.SuProfessioneService;
import it.sildifesa.sildifesa.services.su.SuTitoloStudioService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("bo/anagrafica")
public class CaAnagraficaController {
    private static final Logger LOGGER = LogManager.getLogger(CaAnagraficaController.class);

    @Autowired
    private CaAnagraficaService caAnagraficaService;

    @Autowired
    private SuComuneService suComuneService;

    @Autowired
    private CaTipoDocumentoIdentitaService caTipoDocumentoIdentitaService;

    @Autowired
    private SuTitoloStudioService suTitoloStudioService;

    @Autowired
    private RpCodificaLivelloTitoloService rpCodificaLivelloTitoloService;

    @Autowired
    private CaAnagraficaTitoloDiStudioService caAnagraficaTitoloDiStudioService;

    @Autowired
    private LstArmaService lstArmaService;

    @Autowired
    private CaDocumentoIdentitaService caDocumentoIdentitaService;

    @Autowired
    private SuProfessioneService suProfessioneService;


    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("list")
    public String listAnagrafica(Model model, ModelMap modelMap, @ModelAttribute("caAnagrafica") CaAnagrafica caAnagrafica, @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<CaAnagrafica> listaFiltrata = caAnagraficaService.elencaCaAnagrafiche(caAnagrafica, PageRequest.of(currentPage - 1, pageSize));
        Pagination<CaAnagraficaDto> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(CaAnagraficaDto.fromListModelToListDto(listaFiltrata.getContent()));
        Page<CaAnagraficaDto> listaAnag = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaAnag, currentPage);
        model.addAttribute("listaAnagrafica", listaAnag);

        return "anagrafica/listaAnagrafiche";
    }

    @GetMapping("insert")
    @Transactional
    public String insertAnagrafica(@ModelAttribute("anagrafica") CaAnagraficaDto caAnagrafica, @ModelAttribute("anagrafica_titolo") CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio, Model model) {
        List<CaAnagraficaTitoloDiStudio> listaCaAnagraficaTitolo = new ArrayList<>();
        model.addAttribute("anagrafica", caAnagrafica).addAttribute("comuni", suComuneService.elencaSuComuni())
                .addAttribute("tipoDocumenti", caTipoDocumentoIdentitaService.elencaCaTipoDocumentoIdentita())
                .addAttribute("titoliStudio", suTitoloStudioService.elencaSuTitoloStudio())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("caAnagraficaTitolostudios", listaCaAnagraficaTitolo.add(new CaAnagraficaTitoloDiStudio()))
                .addAttribute("listaForzaArmata", lstArmaService.findDistinctByForzaArmata())
                .addAttribute("livelloTitolo", false);
        return "anagrafica/inserimentoAnagrafica";
    }

    @PostMapping("executeInsert")
    public String executeInserAnagrafica(@Valid @ModelAttribute("anagrafica") CaAnagraficaDto anagrafica,
                                         BindingResult result,
                                         @RequestParam("file") MultipartFile file,
                                         @RequestParam(value = "livelloTitolo") String livelloTitolo,
                                         Model model,
                                         RedirectAttributes redirectAttributes) {



        if (result.hasErrors() || "".equals(file.getOriginalFilename()) || livelloTitolo == null) {

            if ("".equals(file.getOriginalFilename())) {
                model.addAttribute("error","Documento Identità obbligatorio");
            }
            if (livelloTitolo == null) {
                model.addAttribute("errorLivello","Livello titolo di Studio obbligatorio");
            }

            List<CaAnagraficaTitoloDiStudio> listaCaAnagraficaTitolo = new ArrayList<>();
            model.addAttribute("anagrafica", anagrafica).addAttribute("comuni", suComuneService.elencaSuComuni())
                    .addAttribute("tipoDocumenti", caTipoDocumentoIdentitaService.elencaCaTipoDocumentoIdentita())
                    .addAttribute("titoliStudio", suTitoloStudioService.elencaSuTitoloStudio())
                    .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                    .addAttribute("caAnagraficaTitolostudios", listaCaAnagraficaTitolo.add(new CaAnagraficaTitoloDiStudio()))
                    .addAttribute("listaForzaArmata", lstArmaService.findDistinctByForzaArmata())
                    .addAttribute("livelloTitolo",true);
            LOGGER.warn("Errore Validazione Entità Anagrafica");
            return "anagrafica/inserimentoAnagrafica";
        }
        try {
            anagrafica.setSuComune3(SildifesaUtility.controlloOggettoVuoto(anagrafica.getSuComune3()));
            anagrafica.setSuComune2(SildifesaUtility.controlloOggettoVuoto(anagrafica.getSuComune2()));
            anagrafica.setSuComune1(SildifesaUtility.controlloOggettoVuoto(anagrafica.getSuComune1()));
            anagrafica.setCaAnagraficaTitolostudios(SildifesaUtility.controlloTitoliStudioDto(anagrafica.getCaAnagraficaTitolostudios()));
            anagrafica.getCaAnagraficaTitolostudios().forEach(item -> {
                item.setCaAnagrafica(anagrafica);
            });
            anagrafica.setCaDocumentoIdentita(SildifesaUtility.controlloOggettoVuoto(anagrafica.getCaDocumentoIdentita()));
            anagrafica.getCaDocumentoIdentita().setCaTipoDocumentoIdentita(SildifesaUtility.controlloOggettoVuoto(anagrafica.getCaDocumentoIdentita().getCaTipoDocumentoIdentita()));
            anagrafica.getCaDocumentoIdentita().setNomeDocumentoIdentitaFile(file.getOriginalFilename());
            anagrafica.getCaDocumentoIdentita().setEstensioneDocumentoIdentitaFile(file.getContentType());
            anagrafica.getCaDocumentoIdentita().setDocumentoIdentitaFile(file.getBytes());
            anagrafica.setStatoAnagrafica(true);
            CaAnagrafica modelToInsert = CaAnagraficaDto.fromDtoToModel(anagrafica);
            modelToInsert.getCaAnagraficaTitolostudios().forEach(value -> value.setCaAnagrafica(modelToInsert));
            caAnagraficaService.inserisciNuovo(modelToInsert);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuto con successo");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:list";
    }

    @GetMapping("/downloadDocumentoIdentita")
    public String downloadFile(@RequestParam("idCaAnagrafica") Long idCaAnagrafica, HttpServletRequest request, HttpServletResponse response) {
        try {
            CaAnagrafica caAnagrafica = caAnagraficaService.caricaSingoloElemento(idCaAnagrafica);
            response.setHeader("Content-Disposition", "attachment;filename=" + caAnagrafica.getCaDocumentoIdentita().getNomeDocumentoIdentitaFile());
            response.getOutputStream().write(caAnagrafica.getCaDocumentoIdentita().getDocumentoIdentitaFile());
            OutputStream o = response.getOutputStream();
            o.write(caAnagrafica.getCaDocumentoIdentita().getDocumentoIdentitaFile());
            o.close();
        } catch (Exception e) {
            LOGGER.error("Errore durante il Download del Documento Identita", e);
        }

        return "redirect:list";
    }
    @GetMapping("modificaAnagrafica")
    public String modificaAnagrafica(@ModelAttribute("anagrafica") CaAnagraficaDto caAnagraficaDto, @ModelAttribute("anagrafica_titolo") CaAnagraficaTitoloDiStudio caAnagraficaTitoloDiStudio, Model model, @RequestParam("id") Long id) {
        caAnagraficaDto = CaAnagraficaDto.fromModelToDto(caAnagraficaService.caricaSingoloElementoEager(id));

        List<CaAnagraficaTitoloDiStudio> listaCaAnagraficaTitolo = new ArrayList<>();
        model.addAttribute("anagrafica", caAnagraficaDto).addAttribute("comuni", suComuneService.elencaSuComuni())
                .addAttribute("tipoDocumenti", caTipoDocumentoIdentitaService.elencaCaTipoDocumentoIdentita())
                .addAttribute("titoliStudio", suTitoloStudioService.elencaSuTitoloStudio())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("caAnagraficaTitolostudios", listaCaAnagraficaTitolo.add(new CaAnagraficaTitoloDiStudio()))
                .addAttribute("listaForzaArmata", lstArmaService.findDistinctByForzaArmata())
                .addAttribute("livelloTitolo", false);
        return "anagrafica/modificaAnagrafica";
    }

    @PostMapping("executeUpdateAnagrafica")
    public String executeUpdateAnagrafica(@Valid @ModelAttribute("anagrafica") CaAnagraficaDto anagrafica,
                                          BindingResult result,
                                          @RequestParam("file") MultipartFile file,
                                          @RequestParam(value = "livelloTitolo") String livelloTitolo,
                                          Model model, RedirectAttributes redirectAttributes) {



        if (result.hasErrors() || ("".equals(file.getOriginalFilename()) && anagrafica.getCaDocumentoIdentita().getId() == null) || livelloTitolo == null) {

            if ("".equals(file.getOriginalFilename()) && anagrafica.getCaDocumentoIdentita().getId() == null) {
                model.addAttribute("error","Documento Identità obbligatorio");
            }
            if (livelloTitolo == null) {
                model.addAttribute("errorLivello","Livello titolo di Studio obbligatorio");
            }

            List<CaAnagraficaTitoloDiStudio> listaCaAnagraficaTitolo = new ArrayList<>();
            model.addAttribute("anagrafica", anagrafica).addAttribute("comuni", suComuneService.elencaSuComuni())
                    .addAttribute("tipoDocumenti", caTipoDocumentoIdentitaService.elencaCaTipoDocumentoIdentita())
                    .addAttribute("titoliStudio", suTitoloStudioService.elencaSuTitoloStudio())
                    .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                    .addAttribute("caAnagraficaTitolostudios", listaCaAnagraficaTitolo.add(new CaAnagraficaTitoloDiStudio()))
                    .addAttribute("listaForzaArmata", lstArmaService.findDistinctByForzaArmata())
                    .addAttribute("livelloTitolo",true);
            LOGGER.warn("Errore Validazione Entità Anagrafica");
            return "anagrafica/modificaAnagrafica";
        }
        try {
            CaAnagrafica anag = CaAnagraficaDto.fromDtoToModel(anagrafica);
            anag.setSuComune3(SildifesaUtility.controlloOggettoVuoto(anag.getSuComune3()));
            anag.setSuComune2(SildifesaUtility.controlloOggettoVuoto(anag.getSuComune2()));
            anag.setSuComune1(SildifesaUtility.controlloOggettoVuoto(anag.getSuComune1()));
            anag.setCaAnagraficaTitolostudios(SildifesaUtility.controlloTitoliStudio(anag.getCaAnagraficaTitolostudios()));
            anag.getCaAnagraficaTitolostudios().forEach(item -> item.setCaAnagrafica(anag));
            if (!"".equals(file.getOriginalFilename()) || anag.getCaDocumentoIdentita().getId() == null) {
                anag.setCaDocumentoIdentita(SildifesaUtility.controlloOggettoVuoto(anag.getCaDocumentoIdentita()));
                anag.getCaDocumentoIdentita().setCaTipoDocumentoIdentita(SildifesaUtility.controlloOggettoVuoto(anag.getCaDocumentoIdentita().getCaTipoDocumentoIdentita()));
                anag.getCaDocumentoIdentita().setNomeDocumentoIdentitaFile(file.getOriginalFilename());
                anag.getCaDocumentoIdentita().setEstensioneDocumentoIdentitaFile(file.getContentType());
                anag.getCaDocumentoIdentita().setDocumentoIdentitaFile(file.getBytes());
            } else {
                CaDocumentoIdentita doc = caDocumentoIdentitaService.caricaSingoloElemento(anag.getCaDocumentoIdentita().getId());
                anag.setCaDocumentoIdentita(doc);
            }
            anag.getCaAnagraficaTitolostudios().forEach(value -> value.setCaAnagrafica(anag));
            anag.setStatoAnagrafica(true);
            CaAnagrafica caAnagrafica = caAnagraficaService.caricaSingoloElementoEager(anag.getId());
            SildifesaUtility.setObjectFromFront(caAnagrafica, anag, SildifesaUtility.UPDATE_ANAG_VOLONTARI);
            caAnagraficaService.inserisciNuovo(caAnagrafica);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }


        return "redirect:list";
    }

    @GetMapping("eliminaAnagrafica")
    public String eliminaAnagrafica(Long id, Model model, RedirectAttributes redirectAttributes) {

        try{
            CaAnagrafica caAnagrafica = caAnagraficaService.caricaSingoloElementoEager(id);
            caAnagrafica.setStatoAnagrafica(false);
            caAnagraficaService.aggiorna(caAnagrafica);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Error e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }

        return "redirect:list";
    }

    @GetMapping("dettaglioAnagrafica")
    public String dettaglioAnagrafica(Model model, @RequestParam("id") Long id) {
        CaAnagraficaDto anagrafica = CaAnagraficaDto.fromModelToDto(caAnagraficaService.caricaSingoloElementoEager(id));
        List<RpCodificaLivelloTitoloDto> listaTitoli = rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo().stream()
                .map(item -> RpCodificaLivelloTitoloDto.fromModelToDto(item)).collect(Collectors.toList());
        List<CaTipoDocumentoIdentitaDto> listaDoc = caTipoDocumentoIdentitaService.elencaCaTipoDocumentoIdentita().stream()
                .map(item -> CaTipoDocumentoIdentitaDto.fromModelToDto(item)).collect(Collectors.toList());
        model.addAttribute("anagrafica", anagrafica)
                .addAttribute("tipoDocumenti", listaDoc)
                .addAttribute("livelloStudio", listaTitoli);
        return "anagrafica/dettaglioAnagrafica";
    }

}
