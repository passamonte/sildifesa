package it.sildifesa.sildifesa.controller.utenti;

import it.sildifesa.sildifesa.controller.CaAnagraficaController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.FoCorsoDto;
import it.sildifesa.sildifesa.dto.Pagination;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.ut.UtUtente;
import it.sildifesa.sildifesa.services.ut.UtUtenteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("bo/gestioneUtenti")
public class GestioneUtentiController {
    private static final Logger LOGGER = LogManager.getLogger(GestioneUtentiController.class);

    @Autowired
    private UtUtenteService utUtenteService;



    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("lista")
    public String listUtenti(Model model, ModelMap modelMap, @RequestParam("page") Optional<Integer> page,
                             @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<UtUtente> listaFiltrata = utUtenteService.elencaUtUtentiPaginati(PageRequest.of(currentPage - 1, pageSize));
        Pagination<UtUtente> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(listaFiltrata.stream().collect(Collectors.toList()));
        Page<UtUtente> listaUtenti = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaUtenti, currentPage);
        model.addAttribute("listaUtenti", listaUtenti);

        return "gestioneutenti/listaUtenti";
    }

    @GetMapping("inserisciUtente")
    public String inserisciUtente(Model model, ModelMap modelMap) {

//        model.addAttribute("utenteVolontario", new UtUtente());
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (auth != null) {
//            new SecurityContextLogoutHandler().logout(request, response, auth);
//        }

        return "#";
    }


}
