package it.sildifesa.sildifesa.controller.ajax;

import it.sildifesa.sildifesa.dao.ca.CaAnagraficaAziendaRepository;
import it.sildifesa.sildifesa.dao.lst.LstArmaRepository;
import it.sildifesa.sildifesa.dao.lst.LstGradoRepository;
import it.sildifesa.sildifesa.dao.su.SuComuneRepository;
import it.sildifesa.sildifesa.dao.su.SuProfessioneRepository;
import it.sildifesa.sildifesa.dao.su.SuSpecialitaRepository;
import it.sildifesa.sildifesa.dao.su.SuTitoloStudioRepository;
import it.sildifesa.sildifesa.dto.CaAnagraficaAziendaDto;
import it.sildifesa.sildifesa.dto.SuProfessioneDto;
import it.sildifesa.sildifesa.dto.SuSpecialitaDto;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstGrado;
import it.sildifesa.sildifesa.models.su.SuComune;
import it.sildifesa.sildifesa.models.su.SuProfessione;
import it.sildifesa.sildifesa.models.su.SuSpecialita;
import it.sildifesa.sildifesa.models.su.SuTitoloStudio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AjaxController {
    @Autowired
    SuTitoloStudioRepository suTitoloStudioRepository;

    @Autowired
    SuComuneRepository suComuneRepository;

    @Autowired
    LstArmaRepository lstArmaRepository;

    @Autowired
    LstGradoRepository lstGradoRepository;

    @Autowired
    SuSpecialitaRepository suSpecialitaRepository;

    @Autowired
    CaAnagraficaAziendaRepository caAnagraficaAziendaRepository;

    @Autowired
    SuProfessioneRepository suProfessioneRepository;

    List<SuTitoloStudio> listaTitoliStudioFiltrata;

    @GetMapping("/getTitoloDiStudio")
    public List<SuTitoloStudio> ajaxGetTitoloBetween() {
        listaTitoliStudioFiltrata = suTitoloStudioRepository.getTitoloSuLivelloBetween();
        return listaTitoliStudioFiltrata;
    }

    @GetMapping("/getTitoloDiStudioLower")
    public List<SuTitoloStudio> ajaxGetTitoloLower(@PathParam("livello") Integer livello) {
        listaTitoliStudioFiltrata = suTitoloStudioRepository.getTitoloSuLivelloLower(livello);
        return listaTitoliStudioFiltrata;
    }

    @GetMapping("/getTitoloLaurea")
    public List<SuTitoloStudio> ajaxGetTitoloLaurea() {
        listaTitoliStudioFiltrata = suTitoloStudioRepository.getTitoloSuLivelloLaurea();
        return listaTitoliStudioFiltrata;
    }

    @GetMapping("/getComuni")
    public List<SuComune> ajaxGetComuni(@PathParam("comune") String comune) {
        List<SuComune> listaFiltrata = suComuneRepository.findAllByNomeContains(comune);
        return listaFiltrata;
    }

    @GetMapping("/getArma")
    public List<LstArma> getArma(@PathParam("forzaArmata") String forzaArmata){
        return lstArmaRepository.findAllByForzaArmata(forzaArmata);
    }

    @GetMapping("/getGrado")
    public List<LstGrado> getGrado(@PathParam("forzaArmata") String forzaArmata){
        return lstGradoRepository.findAllByForzaArmata(forzaArmata);
    }

    @GetMapping("/getIncarico")
    public List<SuSpecialitaDto> getIncarico(@PathParam("incarico") String incarico, @PathParam("forzaArmata") String forzaArmata){
        List<SuSpecialitaDto> dtos = new ArrayList<>();
        List<SuSpecialita> specs = suSpecialitaRepository.findAllByNomeContainsAndForzaArmata(incarico, forzaArmata);
        specs.forEach(item -> dtos.add(SuSpecialitaDto.fromModelToDtoSenzaRelazioni(item)));
        return dtos;
    }

    @GetMapping("/getAziende")
    public List<CaAnagraficaAziendaDto> getAziende(@PathParam("azienda") String azienda){
        List<CaAnagraficaAziendaDto> dtos = new ArrayList<>();
        List<CaAnagraficaAzienda> specs = caAnagraficaAziendaRepository.findAllByRagioneSocialeContains(azienda);
        specs.forEach(item -> dtos.add(CaAnagraficaAziendaDto.fromModelToDto(item)));
        return dtos;
    }

    @GetMapping("/getProfessioni")
    public List<SuProfessioneDto> getProfessioni(@PathParam("professione") String professione){
        List<SuProfessioneDto> dtos = new ArrayList<>();
        List<SuProfessione> specs = suProfessioneRepository.findAllByNomeContains(professione);
        specs.forEach(item -> dtos.add(SuProfessioneDto.fromModelToDto(item)));
        return dtos;
    }
}
