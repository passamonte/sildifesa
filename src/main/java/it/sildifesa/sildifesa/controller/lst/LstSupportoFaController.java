package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstSupportoFa;
import it.sildifesa.sildifesa.services.lst.LstSupportoFaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "bo/supportoFa")
public class LstSupportoFaController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstSupportoFaController.class);

    @Autowired
    LstSupportoFaService lstSupportoFaService;

    @GetMapping("/listaSupportoFa")
    public String listaSupportoFa(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstSupportoFa> listaSupportoFa = lstSupportoFaService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaSupportoFa, currentPage);

        return "lst/lstsupportofa/listaSupportoFa";

    }

    @GetMapping("/inserisciSupportoFa")
    public String inserisciSupportoFa(@ModelAttribute("supportoFa") LstSupportoFa lstSupportoFa, Model model) {
        model.addAttribute("supportoFa", lstSupportoFa);
        return "lst/lstsupportofa/inserimentoSupportoFa";
    }

    @GetMapping("/modificaSupportoFa")
    public String modificaSupportoFa( Model model, @RequestParam("id") Long id) {
        LstSupportoFa lstSupportoFa = lstSupportoFaService.caricaSingoloElemento(id);
        model.addAttribute("supportoFa", lstSupportoFa);
        return "lst/lstsupportofa/modificaSupportoFa";
    }

    @PostMapping("/eseguiInserimentoSupportoFa")
    public String eseguiInserimentoSupportoFa(@Valid @ModelAttribute("supportoFa") LstSupportoFa lstSupportoFa, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstsupportofa/inserimentoSupportoFa";
        }
        try{
            lstSupportoFaService.inserisciNuovo(lstSupportoFa);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaSupportoFa";
    }



    @PostMapping("/eseguiModificaSupportoFa")
    public String eseguiModificaSupportoFa(@Valid @ModelAttribute("supportoFa") LstSupportoFa lstSupportoFa, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstsupportofa/modificaSupportoFa";
        }

        try{
            lstSupportoFaService.aggiorna(lstSupportoFa);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaSupportoFa";
    }

    @GetMapping("/eliminaSupportoFa")
    public String eliminaSupportoFa(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstSupportoFa lstSupportoFa = lstSupportoFaService.caricaSingoloElemento(id);
            lstSupportoFaService.rimuovi(lstSupportoFa);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaSupportoFa";
    }

}
