package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstAssociazioneCategoria;
import it.sildifesa.sildifesa.services.lst.LstAssociazioneCategoriaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;


@Controller
@RequestMapping("bo/associazioneCategoria")
public class LstAssociazioneCategoriaController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstAssociazioneCategoriaController.class);

    @Autowired
    LstAssociazioneCategoriaService lstAssociazioneCategoriaService;

    @GetMapping("/listaAssociazioneCategoria")
    public String listaAssociazioneCategoria(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstAssociazioneCategoria> listaAssociazioneCategoria = lstAssociazioneCategoriaService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaAssociazioneCategoria, currentPage);

        return "lst/lstassociazionecategoria/listaAssociazioneCategoria";
    }

    @GetMapping("/inserisciAssociazioneCategoria")
    public String inserisciAssociazioneCategoria(@ModelAttribute("associazioneCategoria") LstAssociazioneCategoria lstAssociazioneCategoria, Model model) {
        model.addAttribute("associazioneCategoria", lstAssociazioneCategoria);
        return "lst/lstassociazionecategoria/inserimentoAssociazioneCategoria";
    }

    @PostMapping("/eseguiInserimentoAssociazioneCategoria")
    public String eseguiInserimentoAssociazioneCategoria(@Valid @ModelAttribute("associazioneCategoria") LstAssociazioneCategoria lstAssociazioneCategoria, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstassociazionecategoria/inserimentoAssociazioneCategoria";
        }
        try{
            lstAssociazioneCategoriaService.inserisciNuovo(lstAssociazioneCategoria);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaAssociazioneCategoria";
    }
    @GetMapping("/modificaAssociazioneCategoria")
    public String modificaAssociazioneCategoria( Model model, @RequestParam("id") Long id) {
        LstAssociazioneCategoria lstAssociazioneCategoria = lstAssociazioneCategoriaService.caricaSingoloElemento(id);
        model.addAttribute("associazioneCategoria", lstAssociazioneCategoria);
        return "lst/lstassociazionecategoria/modificaAssociazioneCategoria";
    }

    @PostMapping("/eseguiModificaAssociazioneCategoria")
    public String eseguiModificaAssociazioneCategoria(@Valid @ModelAttribute("associazioneCategoria") LstAssociazioneCategoria lstAssociazioneCategoria, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstassociazionecategoria/modificaAssociazioneCategoria";
        }

        try{
            lstAssociazioneCategoriaService.aggiorna(lstAssociazioneCategoria);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaAssociazioneCategoria";
    }


    @GetMapping("/eliminaAssociazioneCategoria")
    public String eliminaAssociazioneCategoria(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstAssociazioneCategoria lstAssociazioneCategoria = lstAssociazioneCategoriaService.caricaSingoloElemento(id);
            lstAssociazioneCategoriaService.rimuovi(lstAssociazioneCategoria);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaAssociazioneCategoria";
    }
}
