package it.sildifesa.sildifesa.controller.lst;


import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstRuolo;
import it.sildifesa.sildifesa.services.lst.LstRuoloService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "bo/ruolo")
public class LstRuoloController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstRuoloController.class);

    @Autowired
    LstRuoloService lstRuoloService;

    @GetMapping("/listaRuolo")
    public String listaRuolo(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstRuolo> listaRuolo = lstRuoloService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        SildifesaUtility.managePagination(model, listaRuolo, currentPage);
        return "lst/lstruolo/listaRuolo";
    }

    @GetMapping("/inserisciRuolo")
    public String inserisciRuolo(@ModelAttribute("ruolo") LstRuolo lstRuolo, Model model) {
        model.addAttribute("ruolo", lstRuolo);
        return "lst/lstruolo/inserimentoRuolo";
    }

    @PostMapping("/eseguiInserimentoRuolo")
    public String eseguiInserimentoRuolo(@Valid @ModelAttribute("ruolo") LstRuolo lstRuolo, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstruolo/inserimentoRuolo";
        }
        try{
            lstRuoloService.inserisciNuovo(lstRuolo);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaRuolo";
    }
    @GetMapping("/modificaRuolo")
    public String modificaRuolo( Model model, @RequestParam("id") Long id) {
        LstRuolo lstRuolo = lstRuoloService.caricaSingoloElemento(id);
        model.addAttribute("ruolo", lstRuolo);
        return "lst/lstruolo/modificaRuolo";
    }

    @PostMapping("/eseguiModificaRuolo")
    public String eseguiModificaRuolo(@Valid @ModelAttribute("ruolo") LstRuolo lstRuolo, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstruolo/modificaRuolo";
        }

        try{
            lstRuoloService.aggiorna(lstRuolo);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaRuolo";
    }


    @GetMapping("/eliminaRuolo")
    public String eliminaRuolo(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstRuolo lstRuolo = lstRuoloService.caricaSingoloElemento(id);
            lstRuoloService.rimuovi(lstRuolo);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaRuolo";
    }
}
