package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstStatoIter;
import it.sildifesa.sildifesa.services.lst.LstStatoIterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "bo/statoIter")
public class LstStatoIterController extends SildifesaAbstractController {


    private static final Logger LOGGER = LogManager.getLogger(LstStatoIterController.class);

    @Autowired
    LstStatoIterService lstStatoIterService;

    @GetMapping("/listaStatoIter")
    public String listaStatoIter(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstStatoIter> listaStatoIter = lstStatoIterService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaStatoIter, currentPage);

        return "lst/lststatoiter/listaStatoIter";

    }

    @GetMapping("/inserisciStatoIter")
    public String inserisciStatoIter(@ModelAttribute("statoIter") LstStatoIter lstStatoIter, Model model) {
        model.addAttribute("statoIter", lstStatoIter);
        return "lst/lststatoiter/inserimentoStatoIter";
    }

    @GetMapping("/modificaStatoIter")
    public String modificaStatoIter( Model model, @RequestParam("id") Long id) {
        LstStatoIter lstStatoIter = lstStatoIterService.caricaSingoloElemento(id);
        model.addAttribute("statoIter", lstStatoIter);
        return "lst/lststatoiter/modificaStatoIter";
}

    @PostMapping("/eseguiInserimentoStatoIter")
    public String eseguiInserimentoStatoIter(@Valid @ModelAttribute("statoIter") LstStatoIter lstStatoIter, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lststatoiter/inserimentoStatoIter";
        }
        try{
            lstStatoIterService.inserisciNuovo(lstStatoIter);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaStatoIter";
    }



    @PostMapping("/eseguiModificaStatoIter")
    public String eseguiModificaStatoIter(@Valid @ModelAttribute("statoIter") LstStatoIter lstStatoIter, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lststatoiter/modificaStatoIter";
        }

        try{
            lstStatoIterService.aggiorna(lstStatoIter);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaStatoIter";
    }

    @GetMapping("/eliminaStatoIter")
    public String eliminaStatoIter(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstStatoIter lstStatoIter = lstStatoIterService.caricaSingoloElemento(id);
            lstStatoIterService.rimuovi(lstStatoIter);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaStatoIter";
    }
}
