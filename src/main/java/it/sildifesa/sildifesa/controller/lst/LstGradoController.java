package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstGrado;
import it.sildifesa.sildifesa.services.lst.LstGradoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "bo/grado")
public class LstGradoController extends SildifesaAbstractController {


    private static final Logger LOGGER = LogManager.getLogger(LstGradoController.class);

    @Autowired
    LstGradoService lstGradoService;

    @GetMapping("/listaGrado")
    public String listaGrado(
            Model model,
            ModelMap modelMap,
            @ModelAttribute("grado") LstGrado grado,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstGrado> listaGrado = lstGradoService.findPaginated(grado, PageRequest.of(currentPage - 1, pageSize));
        SildifesaUtility.managePagination(model, listaGrado, currentPage);

        modelMap.addAttribute("listaGrado", listaGrado);
        return "lst/lstgrado/listaGrado";
    }


    @GetMapping("/inserisciGrado")
    public String inserisciGrado(@ModelAttribute("grado") LstGrado lstGrado, Model model) {
        model.addAttribute("grado", lstGrado);
        return "lst/lstgrado/inserimentoGrado";
    }

    @PostMapping("/eseguiInserimentoGrado")
    public String eseguiInserimentoGrado(@Valid @ModelAttribute("grado") LstGrado lstGrado, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstgrado/inserimentoGrado";
        }
        try{
            lstGradoService.inserisciNuovo(lstGrado);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaGrado";
    }
    @GetMapping("/modificaGrado")
    public String modificaGrado( Model model, @RequestParam("id") Long id) {
        LstGrado lstGrado = lstGradoService.caricaSingoloElemento(id);
        model.addAttribute("grado", lstGrado);
        return "lst/lstgrado/modificaGrado";
    }

    @PostMapping("/eseguiModificaGrado")
    public String eseguiModificaGrado(@Valid @ModelAttribute("grado") LstGrado lstGrado, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstgrado/modificaGrado";
        }

        try{
            lstGradoService.aggiorna(lstGrado);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaGrado";
    }


    @GetMapping("/eliminaGrado")
    public String eliminaArma(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstGrado lstGrado = lstGradoService.caricaSingoloElemento(id);
            lstGradoService.rimuovi(lstGrado);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaGrado";
    }

}
