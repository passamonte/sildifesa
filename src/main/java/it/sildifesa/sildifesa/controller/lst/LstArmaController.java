package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.Pagination;
import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.services.lst.LstArmaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "bo/arma")
public class LstArmaController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstArmaController.class);

    @Autowired
    LstArmaService lstArmaService;

    @GetMapping("/listaArma")
    public String listaArma(
            Model model,
            @ModelAttribute("arma") LstArma lstArma,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<LstArma> listaFiltrata = lstArmaService.findPaginated(lstArma, PageRequest.of(currentPage - 1, pageSize));
        Pagination<LstArma> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(listaFiltrata.getContent());
        Page<LstArma> listaArma = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaArma, currentPage);
        model.addAttribute("listaArma", listaArma);

        return "lst/lstarma/listaArma";
    }

    @GetMapping("/insertArma")
    public String insertArma(@ModelAttribute("arma") LstArma lstArma, Model model) {
        model.addAttribute("arma", lstArma);
        return "lst/lstarma/inserimentoArma";
    }

    @PostMapping("/executeInsertArma")
    public String executeInsertArma(@Valid @ModelAttribute("arma") LstArma lstArma, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstarma/inserimentoArma";
        }
        try{
            lstArmaService.inserisciNuovo(lstArma);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaArma";
    }


    @GetMapping("/modificaArma")
    public String modificaArma( Model model, @RequestParam("id") Long id) {
        LstArma lstArma = lstArmaService.caricaSingoloElemento(id);
        model.addAttribute("arma", lstArma);
        return "lst/lstarma/modificaArma";
    }

    @PostMapping("/eseguiModificaArma")
    public String eseguiModificaArma(@Valid @ModelAttribute("arma") LstArma lstArma, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstarma/modificaArma";
        }

        try{
            lstArmaService.aggiorna(lstArma);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaArma";
    }


    @GetMapping("/eliminaArma")
    public String eliminaArma(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
        LstArma lstArma = lstArmaService.caricaSingoloElemento(id);
        lstArmaService.rimuovi(lstArma);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaArma";
    }

}
