package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstCategoriaProtetta;
import it.sildifesa.sildifesa.services.lst.LstCategoriaProtettaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("bo/categoriaProtetta")
public class LstCategoriaProtettaController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstCategoriaProtettaController.class);

    @Autowired
    LstCategoriaProtettaService lstCategoriaProtettaService;

    @GetMapping("/listaCategoriaProtetta")
    public String listaCategoriaProtetta(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstCategoriaProtetta> listaCategoriaProtetta = lstCategoriaProtettaService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaCategoriaProtetta, currentPage);

        return "lst/lstcategoriaprotetta/listaCategoriaProtetta";

    }

    @GetMapping("/inserisciCategoriaProtetta")
    public String inserisciCategoriaProtetta(@ModelAttribute("categoriaProtetta") LstCategoriaProtetta lstCategoriaProtetta, Model model) {
        model.addAttribute("categoriaProtetta", lstCategoriaProtetta);
        return "lst/lstcategoriaprotetta/inserimentoCategoriaProtetta";
    }

    @GetMapping("/modificaCategoriaProtetta")
    public String modificaCategoriaProtetta( Model model, @RequestParam("id") Long id) {
        LstCategoriaProtetta lstCategoriaProtetta = lstCategoriaProtettaService.caricaSingoloElemento(id);
        model.addAttribute("categoriaProtetta", lstCategoriaProtetta);
        return "lst/lstcategoriaprotetta/modificaCategoriaProtetta";
    }

    @PostMapping("/eseguiInserimentoCategoriaProtetta")
    public String eseguiInserimentoCategoriaProtetta(@Valid @ModelAttribute("categoriaProtetta") LstCategoriaProtetta lstCategoriaProtetta, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstcategoriaprotetta/inserimentoCategoriaProtetta";
        }
        try{
            lstCategoriaProtettaService.inserisciNuovo(lstCategoriaProtetta);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaCategoriaProtetta";
    }



    @PostMapping("/eseguiModificaCategoriaProtetta")
    public String eseguiModificaCategoriaProtetta(@Valid @ModelAttribute("categoriaProtetta") LstCategoriaProtetta lstCategoriaProtetta, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstcategoriaprotetta/modificaCategoriaProtetta";
        }

        try{
            lstCategoriaProtettaService.aggiorna(lstCategoriaProtetta);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaCategoriaProtetta";
    }

    @GetMapping("/eliminaCategoriaProtetta")
    public String eliminaCategoriaProtetta(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstCategoriaProtetta lstCategoriaProtetta = lstCategoriaProtettaService.caricaSingoloElemento(id);
            lstCategoriaProtettaService.rimuovi(lstCategoriaProtetta);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaCategoriaProtetta";
    }

}
