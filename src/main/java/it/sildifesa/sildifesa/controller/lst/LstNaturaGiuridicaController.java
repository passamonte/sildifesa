package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstNaturaGiuridica;
import it.sildifesa.sildifesa.services.lst.LstNaturaGiuridicaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path="bo/naturaGiuridica")
public class LstNaturaGiuridicaController extends SildifesaAbstractController {
    private static final Logger LOGGER = LogManager.getLogger(LstNaturaGiuridicaController.class);

    @Autowired
    LstNaturaGiuridicaService lstNaturaGiuridicaService;

    @GetMapping("/listaNaturaGiuridica")
    public String listaNaturaGiuridica(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstNaturaGiuridica> listaNaturaGiuridica = lstNaturaGiuridicaService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaNaturaGiuridica, currentPage);

        return "lst/lstnaturagiuridica/listaNaturaGiuridica";

    }

    @GetMapping("/inserisciNaturaGiuridica")
    public String inserisciNaturaGiuridica(@ModelAttribute("naturaGiuridica") LstNaturaGiuridica lstNaturaGiuridica, Model model) {
        model.addAttribute("naturaGiuridica", lstNaturaGiuridica);
        return "lst/lstnaturagiuridica/inserimentoNaturaGiuridica";
    }

    @GetMapping("/modificaNaturaGiuridica")
    public String modificaNaturaGiuridica( Model model, @RequestParam("id") Long id) {
        LstNaturaGiuridica lstNaturaGiuridica = lstNaturaGiuridicaService.caricaSingoloElemento(id);
        model.addAttribute("naturaGiuridica", lstNaturaGiuridica);
        return "lst/lstnaturagiuridica/modificaNaturaGiuridica";
    }

    @PostMapping("/eseguiInserimentoNaturaGiuridica")
    public String eseguiInserimentoNaturaGiuridica(@Valid @ModelAttribute("naturaGiuridica") LstNaturaGiuridica lstNaturaGiuridica, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstnaturagiuridica/inserimentoNaturaGiuridica";
        }
        try{
            lstNaturaGiuridicaService.inserisciNuovo(lstNaturaGiuridica);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaNaturaGiuridica";
    }



    @PostMapping("/eseguiModificaNaturaGiuridica")
    public String eseguiModificaNaturaGiuridica(@Valid @ModelAttribute("naturaGiuridica") LstNaturaGiuridica lstNaturaGiuridica, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstnaturagiuridica/modificaNaturaGiuridica";
        }

        try{
            lstNaturaGiuridicaService.aggiorna(lstNaturaGiuridica);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaNaturaGiuridica";
    }

    @GetMapping("/eliminaNaturaGiuridica")
    public String eliminaNaturaGiuridica(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstNaturaGiuridica lstNaturaGiuridica = lstNaturaGiuridicaService.caricaSingoloElemento(id);
            lstNaturaGiuridicaService.rimuovi(lstNaturaGiuridica);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaNaturaGiuridica";
    }


}
