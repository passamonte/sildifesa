package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstContratto;
import it.sildifesa.sildifesa.services.lst.LstContrattoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("bo/contratto")
public class LstContrattoController extends SildifesaAbstractController {

    private static final Logger LOGGER = LogManager.getLogger(LstContrattoController.class);

    @Autowired
    LstContrattoService lstContrattoService;

    @GetMapping("/listaContratto")
    public String listaContratto(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstContratto> listaContratto = lstContrattoService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaContratto, currentPage);

        return "lst/lstcontratto/listaContratto";

    }

    @GetMapping("/inserisciContratto")
    public String inserisciContratto(@ModelAttribute("contratto") LstContratto lstContratto, Model model) {
        model.addAttribute("contratto", lstContratto);
        return "lst/lstcontratto/inserimentoContratto";
    }

    @GetMapping("/modificaContratto")
    public String modificaContratto( Model model, @RequestParam("id") Long id) {
        LstContratto lstContratto = lstContrattoService.caricaSingoloElemento(id);
        model.addAttribute("categoriaProtetta", lstContratto);
        return "lst/lstcontratto/modificaContratto";
    }

    @PostMapping("/eseguiInserimentoContratto")
    public String eseguiInserimentoContratto(@Valid @ModelAttribute("contratto") LstContratto lstContratto, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstcontratto/inserimentoContratto";
        }
        try{
            lstContrattoService.inserisciNuovo(lstContratto);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaContratto";
    }



    @PostMapping("/eseguiModificaContratto")
    public String eseguiModificaContratto(@Valid @ModelAttribute("categoriaProtetta") LstContratto lstContratto, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstcontratto/modificaContratto";
        }

        try{
            lstContrattoService.aggiorna(lstContratto);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaContratto";
    }

    @GetMapping("/eliminaContratto")
    public String eliminaContratto(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstContratto lstContratto = lstContrattoService.caricaSingoloElemento(id);
            lstContrattoService.rimuovi(lstContratto);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaContratto";
    }

}
