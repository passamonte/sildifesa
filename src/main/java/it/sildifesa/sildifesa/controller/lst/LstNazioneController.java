package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstNazione;
import it.sildifesa.sildifesa.services.lst.LstNazioneService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path = "bo/nazione")
public class LstNazioneController extends SildifesaAbstractController {


    private static final Logger LOGGER = LogManager.getLogger(LstNazioneController.class);

    @Autowired
    LstNazioneService lstNazioneService;

    @GetMapping("/listaNazione")
    public String listaNazione(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstNazione> listaNazione = lstNazioneService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        SildifesaUtility.managePagination(model, listaNazione, currentPage);
        return "lst/lstnazione/listaNazione";
    }

    @GetMapping("/inserisciNazione")
    public String inserisciNazione(@ModelAttribute("nazione") LstNazione lstNazione, Model model) {
        model.addAttribute("nazione", lstNazione);
        return "lst/lstnazione/inserimentoNazione";
    }

    @PostMapping("/eseguiInserimentoNazione")
    public String eseguiInserimentoNazione(@Valid @ModelAttribute("nazione") LstNazione lstNazione, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstnazione/inserimentoNazione";
        }
        try{
            lstNazioneService.inserisciNuovo(lstNazione);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaNazione";
    }
    @GetMapping("/modificaNazione")
    public String modificaNazione( Model model, @RequestParam("id") Long id) {
        LstNazione lstNazione = lstNazioneService.caricaSingoloElemento(id);
        model.addAttribute("nazione", lstNazione);
        return "lst/lstnazione/modificaNazione";
    }

    @PostMapping("/eseguiModificaNazione")
    public String eseguiModificaNazione(@Valid @ModelAttribute("nazione") LstNazione lstNazione, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstnazione/modificaNazione";
        }

        try{
            lstNazioneService.aggiorna(lstNazione);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaNazione";
    }


    @GetMapping("/eliminaNazione")
    public String eliminaNazione(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstNazione lstNazione = lstNazioneService.caricaSingoloElemento(id);
            lstNazioneService.rimuovi(lstNazione);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaNazione";
    }
}
