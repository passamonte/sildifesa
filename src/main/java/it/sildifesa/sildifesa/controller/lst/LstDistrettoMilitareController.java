package it.sildifesa.sildifesa.controller.lst;

import it.sildifesa.sildifesa.controller.SildifesaAbstractController;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import it.sildifesa.sildifesa.services.lst.LstDistrettoMilitareService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(path="bo/distrettoMilitare")
public class LstDistrettoMilitareController extends SildifesaAbstractController {
    private static final Logger LOGGER = LogManager.getLogger(LstDistrettoMilitareController.class);

    @Autowired
    LstDistrettoMilitareService lstDistrettoMilitareService;

    @GetMapping("/listaDistrettoMilitare")
    public String listaDistrettoMilitare(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LstDistrettoMilitare> listaDistrettoMilitare = lstDistrettoMilitareService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        SildifesaUtility.managePagination(model, listaDistrettoMilitare, currentPage);

        return "lst/lstdistrettomilitare/listaDistrettoMilitare";

    }

    @GetMapping("/inserisciDistrettoMilitare")
    public String inserisciDistrettoMilitare(@ModelAttribute("distrettoMilitare") LstDistrettoMilitare lstDistrettoMilitare, Model model) {
        model.addAttribute("distrettoMilitare", lstDistrettoMilitare);
        return "lst/lstdistrettomilitare/inserimentoDistrettoMilitare";
    }

    @GetMapping("/modificaDistrettoMilitare")
    public String modificaDistrettoMilitare( Model model, @RequestParam("id") Long id) {
        LstDistrettoMilitare lstDistrettoMilitare = lstDistrettoMilitareService.caricaSingoloElemento(id);
        model.addAttribute("distrettoMilitare", lstDistrettoMilitare);
        return "lst/lstdistrettomilitare/modificaDistrettoMilitare";
    }

    @PostMapping("/eseguiInserimentoDistrettoMilitare")
    public String eseguiInserimentoDistrettoMilitare(@Valid @ModelAttribute("distrettoMilitare") LstDistrettoMilitare lstDistrettoMilitare, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "lst/lstdistrettomilitare/inserimentoDistrettoMilitare";
        }
        try{
            lstDistrettoMilitareService.inserisciNuovo(lstDistrettoMilitare);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:listaDistrettoMilitare";
    }



    @PostMapping("/eseguiModificaDistrettoMilitare")
    public String eseguiModificaDistrettoMilitare(@Valid @ModelAttribute("distrettoMilitare") LstDistrettoMilitare lstDistrettoMilitare, BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "lst/lstdistrettomilitare/modificaDistrettoMilitare";
        }

        try{
            lstDistrettoMilitareService.aggiorna(lstDistrettoMilitare);
            redirectAttributes.addFlashAttribute("confirm", "Modifica elemento avvenuta con successo");
        } catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile modificare l'elemento");
        }

        return "redirect:listaDistrettoMilitare";
    }

    @GetMapping("/eliminaDistrettoMilitare")
    public String eliminaDistrettoMilitare(@RequestParam("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try{
            LstDistrettoMilitare lstDistrettoMilitare = lstDistrettoMilitareService.caricaSingoloElemento(id);
            lstDistrettoMilitareService.rimuovi(lstDistrettoMilitare);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Exception e){
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:listaDistrettoMilitare";
    }


}
