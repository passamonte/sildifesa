package it.sildifesa.sildifesa.controller.la;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.*;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import it.sildifesa.sildifesa.services.la.LaOffertaService;
import it.sildifesa.sildifesa.services.lst.LstContrattoService;
import it.sildifesa.sildifesa.services.lst.LstTipoContrattoService;
import it.sildifesa.sildifesa.services.rp.RpCodificaLivelloTitoloService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("bo/offerte")
public class LaOffertaController {

    private static final Logger LOGGER = LogManager.getLogger(LaOffertaController.class);

    @Autowired
    LaOffertaService laOffertaService;
    @Autowired
    LstTipoContrattoService lstTipoContrattoService;
    @Autowired
    LstContrattoService lstContrattoService;

    @Autowired
    RpCodificaLivelloTitoloService rpCodificaLivelloTitoloService;


    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @GetMapping("lista")
    public String listaOfferte(Model model, ModelMap modelMap, @ModelAttribute("laOfferta") LaOfferta laOfferta, @RequestParam("page") Optional<Integer> page,
                               @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        Page<LaOfferta> listaFiltrata = laOffertaService.elencaLaOffertePage(laOfferta, PageRequest.of(currentPage - 1, pageSize));
        Pagination<LaOffertaDto> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(listaFiltrata.stream().map(LaOffertaDto::fromModelToDto).collect(Collectors.toList()));
        Page<LaOffertaDto> listaOfferte = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaOfferte, currentPage);
        model.addAttribute("listaOfferte", listaOfferte);
        return "la/listaOfferte";
    }


    @GetMapping("inserimentoOfferta")
    public String inserisciOfferte(@ModelAttribute("offerta") LaOffertaDto offertaDto, Model model) {
        model.addAttribute("offerta", offertaDto)
                .addAttribute("tipoContratti", lstTipoContrattoService.elencaLstTipoContratto())
                .addAttribute("settoreContratti", lstContrattoService.elencaLstContratto())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("errorAzienda", null).addAttribute("errorSettore", null)
                .addAttribute("errorProfessione", null);
        return "la/inserimentoOfferta";
    }

    @PostMapping("executeInsertUpdate")
    public String executeInsertUpdateOfferta(@Valid @ModelAttribute("offerta") LaOffertaDto offerta,
                                             BindingResult result,
                                             @RequestParam(value = "laOffertaContratti", required = false) List<Long> laOffertaContratti,
                                             Model model,
                                             RedirectAttributes redirectAttributes) {
        String errorAzienda = offerta.getAzienda().getId() == null ? "Azienda obbligatoria" : null;
        String errorProfessione = offerta.getProfessione().getId() == null ? "Professione obbligatoria" : null;
//        String errorSettore = offerta.getSettoreContratto().getId() == null ? "Settore Contratto obbligatorio" : null;
        if (result.hasErrors() || errorAzienda != null || errorProfessione != null) {
            String errorSettore = null;
            if (offerta.getSettoreContratto() == null)
                errorSettore = "Settore Contratto obbligatorio";

            model.addAttribute("offerta", offerta)
                    .addAttribute("tipoContratti", lstTipoContrattoService.elencaLstTipoContratto())
                    .addAttribute("settoreContratti", lstContrattoService.elencaLstContratto())
                    .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                    .addAttribute("errorAzienda", errorAzienda).addAttribute("errorSettore", errorSettore)
                    .addAttribute("errorProfessione", errorProfessione);
            LOGGER.warn("Errore Validazione Entità Offerta");
            if (offerta.getId() != null)
                return "la/modificaOfferta";
            else
                return "la/inserimentoOfferta";
        }

        try {

            //GESTIONE MANY TO MANY, CON IL DTO THYMELEAF NON RIESCE A BINDARE LA LISTA/SET
            if (laOffertaContratti != null) {
                List<LstTipoContratto> listaTipoContratti = new ArrayList<>();
                List<LstTipoContrattoDto> listaTipoContrattiDto = new ArrayList<>();
                laOffertaContratti.forEach(item -> listaTipoContratti.add(lstTipoContrattoService.caricaSingoloElemento(item)));
                listaTipoContratti.forEach(item -> listaTipoContrattiDto.add(LstTipoContrattoDto.fromModelToDto(item)));
                offerta.setLaOffertaTipoContratti(new HashSet<>(listaTipoContrattiDto));
            }

            offerta.setAttivo(true);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(offerta.getData());
            offerta.setAnno(calendar.get(Calendar.YEAR));
            LaOfferta modelOfferta = LaOffertaDto.fromDtoToModel(offerta);
            LaOfferta fromDb = null;
            fromDb = laOffertaService.caricaSingoloEager(modelOfferta.getId());

            if (fromDb != null) {
                SildifesaUtility.setObjectFromFront(fromDb, modelOfferta, SildifesaUtility.UPDATE_OFFERTE);
                laOffertaService.inserisciNuovo(fromDb);
            } else {
//                offerta.getLaOffertaTipoContratti().forEach(value->value.setLaOfferta(null));
                laOffertaService.inserisciNuovo(modelOfferta);
            }

            redirectAttributes.addFlashAttribute("confirm", "Salvataggio elemento avvenuto con successo");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile salvare l'elemento");
        }
        return "redirect:lista";
    }

    @GetMapping("modifica")
    public String modificaOfferte(@RequestParam("idOfferta") Long idOfferta, Model model) {
        LaOffertaDto offertaDto = LaOffertaDto.fromModelToDto(laOffertaService.caricaSingoloEager(idOfferta));
        model.addAttribute("offerta", offertaDto)
                .addAttribute("tipoContratti", lstTipoContrattoService.elencaLstTipoContratto())
                .addAttribute("settoreContratti", lstContrattoService.elencaLstContratto())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("errorAzienda", null).addAttribute("errorSettore", null)
                .addAttribute("errorProfessione", null);
        return "la/modificaOfferta";
    }

    @GetMapping("dettaglioOfferte")
    public String dettaglioCorso(Model model, @RequestParam("id") Long id) {
        LaOfferta result = laOffertaService.caricaSingoloEager(id);
        LaOffertaDto offerta = LaOffertaDto.fromModelToDto(result);
        model.addAttribute("offerta", offerta);
        return "la/dettaglioOfferta";
    }

    @GetMapping("elimina")
    public String eliminaOfferta(@RequestParam("idOfferta") Long idOfferta, Model model, RedirectAttributes redirectAttributes) {

        try{
            LaOfferta offerta = laOffertaService.caricaSingoloEager(idOfferta);
            offerta.setAttivo(false);
            laOffertaService.aggiorna(offerta);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Error e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }
        return "redirect:lista";
    }

}
