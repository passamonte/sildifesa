package it.sildifesa.sildifesa.controller.fo;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.FoCorsoDto;
import it.sildifesa.sildifesa.dto.Pagination;
import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.fo.FoDocumentoCorso;
import it.sildifesa.sildifesa.services.fo.FoCorsoService;
import it.sildifesa.sildifesa.services.fo.FoDocumentoCorsoService;
import it.sildifesa.sildifesa.services.rp.RpCodificaLivelloTitoloService;
import it.sildifesa.sildifesa.services.su.SuComuneService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("bo/corsi")
public class FoCorsoController {
    private static final Logger LOGGER = LogManager.getLogger(FoCorsoController.class);

    @Autowired
    private FoCorsoService foCorsoService;

    @Autowired
    private FoDocumentoCorsoService foDocumentoCorsoService;

    @Autowired
    private SuComuneService suComuneService;

    @Autowired
    private RpCodificaLivelloTitoloService rpCodificaLivelloTitoloService;

    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("lista")
    public String listCorsi(Model model, ModelMap modelMap, @ModelAttribute("foCorso") FoCorso foCorso, @RequestParam("page") Optional<Integer> page,
                            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<FoCorso> listaFiltrata = foCorsoService.elencaFoCorsi(foCorso, PageRequest.of(currentPage - 1, pageSize));
        Pagination<FoCorsoDto> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(listaFiltrata.stream().map(FoCorsoDto::fromModelToDtoSenzaDocumento).collect(Collectors.toList()));
        Page<FoCorsoDto> listaCorsi = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaCorsi, currentPage);
        model.addAttribute("listaCorsi", listaCorsi);
        modelMap.addAttribute("corso", new FoCorso());
        return "corso/listaCorsi";
    }

    @GetMapping("insert")
    public String insertAnagrafica(@ModelAttribute("corso") FoCorsoDto corso, Model model) {
        model.addAttribute("corso", corso).addAttribute("comuni", suComuneService.elencaSuComuni())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("errorAzienda", null)
                .addAttribute("flagFile", true);
        return "corso/inserimentoCorso";
    }

    @PostMapping("executeInsertUpdate")
    public String executeInserAnagrafica(@Valid @ModelAttribute("corso") FoCorsoDto corso,
                                         BindingResult result,
                                         Model model,
                                         @RequestParam("file") MultipartFile file,
                                         RedirectAttributes redirectAttributes,
                                         @RequestParam("flagFile") Boolean flagFile) {

        String errorAzienda = corso.getCaAnagraficaAzienda().getId() == null ? "Azienda obbligatoria" : null;
        if (result.hasErrors() || corso.getCaAnagraficaAzienda().getId() == null) {
            model.addAttribute("corso", corso).addAttribute("comuni", suComuneService.elencaSuComuni())
                    .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                    .addAttribute("errorAzienda", errorAzienda)
                    .addAttribute("flagFile", flagFile);
            LOGGER.warn("Errore Validazione Entità Corso");
            if (Objects.nonNull(corso.getId())){
                return "corso/modificaCorso";
            }
            return "corso/inserimentoCorso";
        }
        try {
            corso.setLocalitaSedeCorso(SildifesaUtility.controlloOggettoVuoto(corso.getLocalitaSedeCorso()));
            corso.setCaAnagraficaAzienda(SildifesaUtility.controlloOggettoVuoto(corso.getCaAnagraficaAzienda()));
            corso.setAttivo(true);
            FoCorso modelToInsert = FoCorsoDto.fromDtoToModel(corso);
            FoCorso corsoFromDb = null;
            FoDocumentoCorso doc = null;
            if (!flagFile){
                modelToInsert.setFoDocumentoCorso(null);
            }
            if (!"".equals(file.getOriginalFilename())) {
                modelToInsert.setFoDocumentoCorso(modelToInsert.getFoDocumentoCorso() != null ? modelToInsert.getFoDocumentoCorso() : new FoDocumentoCorso());
                modelToInsert.getFoDocumentoCorso().setNomeDocumentoCorsoFile(file.getOriginalFilename());
                modelToInsert.getFoDocumentoCorso().setEstensioneDocumentoIdentitaFile(file.getContentType());
                modelToInsert.getFoDocumentoCorso().setDocumentoCorsoFile(file.getBytes());
            } else if (Objects.nonNull(modelToInsert.getFoDocumentoCorso()) && Objects.nonNull(modelToInsert.getFoDocumentoCorso().getId())) {
                doc = foDocumentoCorsoService.caricaSingoloElementoEager(modelToInsert.getFoDocumentoCorso().getId());
                doc.setFoCorso(modelToInsert);
            }
            if(Objects.nonNull(modelToInsert.getId())){
                corsoFromDb = foCorsoService.caricaSingoloEager(modelToInsert.getId());
                SildifesaUtility.setObjectFromFront(corsoFromDb, modelToInsert, SildifesaUtility.UPDATE_CORSI);
            }else {
                corsoFromDb = modelToInsert;
            }
            if (Objects.nonNull(doc)){
                corsoFromDb.setFoDocumentoCorso(doc);
            }
            foCorsoService.inserisciNuovo(corsoFromDb);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }
        return "redirect:lista";
    }

    @GetMapping("modificaCorso")
    public String updateCorso(@ModelAttribute("corso") FoCorsoDto corso, Model model, @RequestParam("id") Long id) {
        FoCorso corsoById = foCorsoService.caricaSingoloEager(id);
        corso = FoCorsoDto.fromModelToDto(corsoById);
        model.addAttribute("corso", corso)
                .addAttribute("corso", corso).addAttribute("comuni", suComuneService.elencaSuComuni())
                .addAttribute("livelloStudio", rpCodificaLivelloTitoloService.elencaRpCodificaLivelloTitolo())
                .addAttribute("errorAzienda", null)
                .addAttribute("flagFile", true);
        return "corso/modificaCorso";
    }

    @GetMapping("dettaglioCorso")
    public String dettaglioCorso(Model model, @RequestParam("id") Long id) {
        FoCorso result = foCorsoService.caricaSingoloEager(id);
        FoCorsoDto corso = FoCorsoDto.fromModelToDto(result);
        model.addAttribute("corso", corso);
        return "corso/dettaglioCorso";
    }

    @GetMapping("eliminaCorso")
    public String eliminaAnagrafica(Long id, Model model, RedirectAttributes redirectAttributes) {

        try{
            FoCorso corso = foCorsoService.caricaSingoloEager(id);
            corso.setAttivo(false);
            foCorsoService.aggiorna(corso);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione elemento avvenuta con successo");
        }
        catch (Error e) {
            LOGGER.error(e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile eliminare l'elemento");
        }

        return "redirect:lista";
    }

    @GetMapping("listaIscritti")
    public String listaIscritti(Model model, @RequestParam("id") Long id) {

//        FoCorso result = foCorsoService.caricaSingoloEager(id);
//        FoCorsoDto corso = FoCorsoDto.fromModelToDto(result);
//        model.addAttribute("corso", corso);
        return "corso/listaIscritti";
    }

}
