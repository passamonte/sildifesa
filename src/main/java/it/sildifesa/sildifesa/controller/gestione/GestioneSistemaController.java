package it.sildifesa.sildifesa.controller.gestione;

import it.sildifesa.sildifesa.dao.su.SuComuneRepository;
import it.sildifesa.sildifesa.models.su.SuComune;
import it.sildifesa.sildifesa.services.su.SuComuneService;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

@Controller
@RequestMapping("bo/gestioneSistema")
public class GestioneSistemaController {
    private static final Logger LOGGER = LogManager.getLogger(GestioneSistemaController.class);

    @Value("${app.upload.dir:${user.home}}")
    public String downlDir;

    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @Autowired
    SuComuneService suComuneService;

    @Autowired
    SuComuneRepository suComuneRepository;

    @GetMapping("autoAggiornaComuni")
    public String autoAggiornaComuni(Model model) {
        return "gestione/aggiornaComuni";
    }


    @GetMapping("eseguiAutoAggiornamentoComuni")
    public String eseguiAutoAggiornamentoComuni(Model model) {

        try {
            InputStream is = new URL("https://www.istat.it/storage/codici-unita-amministrative/Elenco-comuni-italiani.xls").openStream();
            Path path = Paths.get(downlDir, "istatDownload.xls");
            File downloaded = new File(path.toString());
            FileUtils.copyInputStreamToFile(is, downloaded);
            FileInputStream file = new FileInputStream(downloaded);


            HSSFWorkbook workbook = new HSSFWorkbook(file);

            HSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();

            while (rowIterator.hasNext()) {

                SuComune suComune = new SuComune();
                Row row = rowIterator.next();

                //For each row, iterate through all the columns
                if (row.getRowNum() > 0) {
                    suComune.setNazione("Italia");
                    SuComune suComuneDb = suComuneRepository.findByNome(row.getCell(6).getStringCellValue());
//              LETTURA COLONNA NOME
                    suComune.setNome(row.getCell(6).getStringCellValue());


//              LETTURA COLONNA PROVINCIA
                    suComune.setProvincia(row.getCell(13).getStringCellValue());

//              LETTURA COLONNA Regione
                    suComune.setRegione(row.getCell(10).getStringCellValue());

//              LETTURA COLONNA Codice Fisco
                    suComune.setCodiceFisco(row.getCell(18).getStringCellValue());


//              LETTURA COLONNA Codice Istat
                    Cell codIstat = row.getCell(4);
                    suComune.setCodiceIstat(row.getCell(4).getStringCellValue());


//              LETTURA COLONNA Codice Regione
                    Cell codRegione = row.getCell(0);
                    suComune.setCodiceRegioneBNL(row.getCell(0).getStringCellValue());

//                    //              LETTURA COLONNA Provincia Estesa
//                    Cell provinciaEstesa = row.getCell(0);s
//                    System.out.print("Provincia Estesa: " + provinciaEstesa.getStringCellValue() + " ");

                    if (suComuneDb==null) {
                        suComuneService.inserisciNuovo(suComune);
                        LOGGER.info("inserito comune: " + suComune.getNome()+ row.getRowNum());
                    } else if(suComuneDb.getNome()!=null &&
                            !(suComuneDb.getNome().equals(suComune.getNome())) && (
                            !(suComuneDb.getProvincia().equals(suComune.getProvincia())) ||
                            !(suComuneDb.getCodiceFisco().equals(suComune.getCodiceFisco())) ||
                            !(suComuneDb.getCodiceIstat().equals(suComune.getCodiceIstat()))
                    )){
                        suComuneService.aggiorna(suComune);
                        LOGGER.info("aggiorato comune: " + suComune.getNome() + row.getRowNum());
                    }



                }
            }
            file.close();
        } catch (Exception e) {
            LOGGER.error("Errore Download dati ISTAT", e);
        }


        return "gestione/aggiornaComuni";
    }
}
