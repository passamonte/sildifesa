package it.sildifesa.sildifesa.controller;

import groovy.lang.IncorrectClosureArgumentsException;
import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.UtUtenteDto;
import it.sildifesa.sildifesa.models.ut.UtUtente;
import it.sildifesa.sildifesa.services.ut.UtUtenteService;
import it.sildifesa.sildifesa.services.securityservice.SecurityService;
import it.sildifesa.sildifesa.services.securityservice.UtUtenteValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class LoginController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UtUtenteService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UtUtenteValidator userValidator;

    @Autowired
    private JavaMailSender javaMailSender;


    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);


    @GetMapping("/home")
    public String welcome(Model model) {
        return "home2";
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public String handleExceptionLogin(InternalAuthenticationServiceException e, Model model) {
        model.addAttribute("erroreAbilitazione", "Attenzione, Utente non abilitato");
        return "login";
    }


    @GetMapping(value = "/login")
    public String login(@RequestParam(value = "error", required = false) Boolean error,
                        @RequestParam(value = "logout", required = false) String logout,
                        Model model) {

        model.addAttribute("error", error);
        model.addAttribute("logout", logout);

        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout=true";
    }


    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public String currentUserNameSimple(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        return principal.getName();
    }


    @GetMapping("registraAccount/registration")
    public String registration(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("utenteVolontario", new UtUtente());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "fo/registrazioneAccountVolontario";
    }

    @PostMapping("registraAccount/execregistration")
    public String registration(@Valid @ModelAttribute("utenteVolontario") UtUtente utenteVolontario, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
//        userValidator.validate(utenteVolontario, bindingResult);
        String path = request.getHeader("host") + request.getContextPath() + "/confermaRegistrazione/";
        try {
            if (userService.controlloEsistenzaUsername(utenteVolontario.getUsername())
                    || userService.controlloEsistenzaUsernameByEmail(utenteVolontario.getEmail())
                    || bindingResult.hasErrors()) {
                if (!utenteVolontario.getPassword().equals(utenteVolontario.getPasswordConfirm()))
                    model.addAttribute("errorePassword", "Attenzione, le password non coincidono");

                if (userService.controlloEsistenzaUsername(utenteVolontario.getUsername()))
                    model.addAttribute("erroreUsername", "Attenzione, utente già esistente");

//                if (userService.controlloEsistenzaUsernameByEmail(utenteVolontario.getEmail()))
//                    model.addAttribute("erroreEmail", "Attenzione, email già registrata");

                try {
                    if (userService.trovaUtentePerMail(utenteVolontario.getEmail()) != null)
                        model.addAttribute("erroreEmail", "Attenzione, mail già registrata");
                } catch (IncorrectClosureArgumentsException e) {
                    model.addAttribute("erroreEmail", "Attenzione, mail già registrata");
                    LOGGER.warn("Errore nel controllo e-mail " + e);
                }

                if (utenteVolontario.getPassword().length() < 8 || utenteVolontario.getPassword().length() > 16)
                    model.addAttribute("erroreLunghezzaPassword", "Attenzione, la lunghezza della password deve essere compresa tra gli 8 e i 16 caratteri");

                return "fo/registrazioneAccountVolontario";
            }
            utenteVolontario.setAbilitato(false);
            String token = SildifesaUtility.generateToken();
            utenteVolontario.setTokenConferma(token);
            userService.insertNewUtente(utenteVolontario);
            MimeMessagePreparator mail = mimeMessage -> {
                MimeMessageHelper message = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");
                message.setSubject("Conferma Registrazione SILDifesa");
                message.setText("\n" +
                        "<html> \n" +
                        "<body>\n" +
                        "\t<h1>SILDifesa </h1> \n" +
                        "\t<div>\n" +
                        "\t\t<p>Per confermare la registrazione, seguire questo link:</p>\n" +
                        "\t\t<a type=\"button\" href=\"http://" + path.concat(utenteVolontario.getUsername() + "/").concat(token) + "\" >Link Conferma E-Mail</a><br> " + path.concat((utenteVolontario.getUsername()).toString() + "/").concat(token) + "</div></body></html>", true);

                message.setTo(utenteVolontario.getEmail());
                message.setFrom("noreply@sild.difesa.it", "NoReply SILD");
                LOGGER.info("Messaggio di conferma inviato a: " + utenteVolontario.getEmail());


            };


            model.addAttribute("confirm", "Registrazione avvenuta con successo. Verificare la mail di conferma per validare la registrazione.");
            javaMailSender.send(mail);


        } catch (Exception e) {
            LOGGER.error("Errore in fase di Registrazione", e);
            redirectAttributes.addFlashAttribute("error", "Errore in fase di registrazione. Riprovare controllando che tutti i campi siano corretti.");
        }
        return "home";
    }

    @GetMapping("/confermaRegistrazione/{username}/{token}")
    public String confermaRegistrazione(@PathVariable("username") String username, @PathVariable("token") String token, Model model, RedirectAttributes redirectAttributes) {
        try {
            UtUtente utenteVolontario = userService.findByUsername(username);

            if (utenteVolontario.getTokenConferma().equals(token) && !utenteVolontario.getAbilitato()) {
                utenteVolontario.setAbilitato(true);
                utenteVolontario.setTokenConferma(null);
                userService.aggiorna(utenteVolontario);
//                securityService.autoLogin(utenteVolontario.getUsername(), utenteVolontario.getPasswordConfirm());

                model.addAttribute("confirm", "Benvenuto " + utenteVolontario.getNome() + ", la registrazione è stata confermata.");
            } else
                model.addAttribute("error", "Attenzione, link di conferma errato oppure si è gia' effettuata la conferma.");
        } catch (Exception e) {
            model.addAttribute("error", "Errore in fase di Conferma. Contattare l'amministratore del servizio");
            LOGGER.error("Errore in fase di Conferma della registrazione", e);
        }
        return "home";
    }

    @GetMapping("passwordReset/enter")
    public String passwordRestet(Model model) {
        return "passwordReset";
    }

    @PostMapping("passwordReset/exec")
    public String passwordResetExec(@RequestParam("email") String email, Model model, HttpServletRequest request) {
        try {
            UtUtente utenteVolontario = userService.trovaUtentePerMail(email);
            if (utenteVolontario != null && utenteVolontario.getAbilitato()) {

                String path = request.getHeader("host") + request.getContextPath() + "/passwordReset/";
                String token = SildifesaUtility.generateToken();
                utenteVolontario.setTokenConferma(token);
                userService.aggiorna(utenteVolontario);
                MimeMessagePreparator mailMessage = mimeMessage -> {
                    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                    message.setSubject("Conferma Recupero Password SILDifesa");
                    message.setText("\n" +
                            "<html> \n" +
                            "<body>\n" +
                            "\t<h1>SILDifesa </h1> \n" +
                            "\t<div>\n" +
                            "\t\t<p>Per recuperare la password, seguire questo link:</p>\n" +
                            "\t\t<a type=\"button\" href=\"http://" + path.concat(utenteVolontario.getUsername() + "/").concat(token) + "\" >Link Recupero Password</a><br> " + path.concat((utenteVolontario.getUsername()).toString() + "/").concat(token) + "</div></body></html>", true);

                    message.setTo(utenteVolontario.getEmail());
                    message.setFrom("noreply@sild.difesa.it", "NoReply SILD");

                };
                LOGGER.info("Messaggio di recupero inviato a: " + utenteVolontario.getEmail());
                javaMailSender.send(mailMessage);

                model.addAttribute("confirm", "Mail per il recupero della password inviata. Controllare la posta dell'indirizzo mail");
            } else {
                model.addAttribute("error", "Attenzione, nessun utente trovato con la mail inserita. Per favore, riprovare.");
            }
        } catch (Exception e) {
            LOGGER.error("Errore in fase di reset password", e);

        }
        return "passwordReset";
    }


    @GetMapping("/passwordReset/{username}/{token}")
    public String passwordResetToken(@PathVariable("username") String username, @PathVariable("token") String token, Model model, RedirectAttributes redirectAttributes) {
        UtUtente utenteVolontario = userService.findByUsername(username);
        if (!(utenteVolontario.getTokenConferma().equals(token))) {
            model.addAttribute("error", "Attenzione, link errato.");
            return "passwordReset";
        }
        model.addAttribute("utente", new UtUtenteDto());
        model.addAttribute("username", username);
        model.addAttribute("token", token);
        return "passwordResetEsegui";
    }

    @PostMapping("/passwordReset/{username}/{token}/esegui")
    public String eseguiReset(@ModelAttribute("utente") UtUtenteDto utente, BindingResult bindingResult, @PathVariable("username") String username, @PathVariable("token") String token, Model model, RedirectAttributes redirectAttributes) {
        try {

            if (!utente.getPassword().equals(utente.getPasswordConfirm())) {
                redirectAttributes.addFlashAttribute("errorePasswordConfirm", "Attenzione, le password non coincidono");
                return "redirect:/passwordReset/" + username + "/" + token;
            }

            if (utente.getPassword().length() < 8 || utente.getPassword().length() > 16) {
                redirectAttributes.addFlashAttribute("erroreLunghezzaPassword", "Attenzione, la lunghezza della password deve essere compresa tra gli 8 e i 16 caratteri");
                return "redirect:/passwordReset/" + username + "/" + token;
            }
            UtUtente utenteVolontario = userService.findByUsername(username);

            if (utenteVolontario.getTokenConferma().equals(token)) {
                userService.resetPassword(utenteVolontario, utente.getPassword());
                model.addAttribute("confirm", "Benvenuto " + utenteVolontario.getNome() + ", il reset della Password è avvenuto correttamente.");
                LOGGER.info("l'utente: " + utenteVolontario.getUsername() + " ha eseguito il reset della password");
            } else
                model.addAttribute("error", "Attenzione, link di reset errato.");
        } catch (Exception e) {
            model.addAttribute("error", "Errore in fase di Reset Password. Contattare l'amministratore del servizio");
            LOGGER.error("Errore in fase di Reset della Passsword", e);
        }
        return "login";
    }


}
