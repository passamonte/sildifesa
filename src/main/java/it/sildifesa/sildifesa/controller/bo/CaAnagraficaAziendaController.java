package it.sildifesa.sildifesa.controller.bo;

import it.sildifesa.sildifesa.controller.utilities.SildifesaUtility;
import it.sildifesa.sildifesa.dto.CaAnagraficaAziendaDto;
import it.sildifesa.sildifesa.dto.Pagination;
import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.ca.CaDocumentoAzienda;
import it.sildifesa.sildifesa.services.ca.CaAnagraficaAziendaService;
import it.sildifesa.sildifesa.services.ca.CaDocumentoAziendaService;
import it.sildifesa.sildifesa.services.ca.CaTipoAziendaService;
import it.sildifesa.sildifesa.services.su.SuComuneService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("bo/anagraficaaziende")
public class CaAnagraficaAziendaController {

    private static final Logger LOGGER = LogManager.getLogger(CaAnagraficaAziendaController.class);

    @Autowired
    CaAnagraficaAziendaService caAnagraficaAziendaService;

    @Autowired
    SuComuneService suComuneService;

    @Autowired
    CaTipoAziendaService caTipoAziendaService;

    @Autowired
    CaDocumentoAziendaService caDocumentoAziendaService;


    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @GetMapping("lista")
    public String listAnagraficaAziende(
            Model model,
            ModelMap modelMap,
            @ModelAttribute("caAnagraficaAzienda") CaAnagraficaAzienda anagAzienda,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<CaAnagraficaAzienda> listaFiltrata = caAnagraficaAziendaService.elencaCaAnagraficaAziende(anagAzienda, PageRequest.of(currentPage - 1, pageSize));
        Pagination<CaAnagraficaAziendaDto> pageToHtml = new Pagination<>();
        pageToHtml.setTotalElements(listaFiltrata.getTotalElements());
        pageToHtml.setTotalPages(listaFiltrata.getTotalPages());
        pageToHtml.setContent(listaFiltrata.stream().map(CaAnagraficaAziendaDto::fromModelToDto).collect(Collectors.toList()));
        Page<CaAnagraficaAziendaDto> listaAnagrafica = new PageImpl<>(pageToHtml.getContent(), PageRequest.of(currentPage - 1, pageSize), pageToHtml.getTotalElements());
        SildifesaUtility.managePagination(model, listaAnagrafica, currentPage);
        model.addAttribute("listaAnagrafica", listaAnagrafica);
        return "anagraficaaziende/listaAnagraficheAziende";
    }


    @GetMapping("insert")
    public String insertAnagrafica(@ModelAttribute("anagrafica") CaAnagraficaAzienda caAnagraficaAzienda, Model model) {
        model.addAttribute("tipologieAzienda", caTipoAziendaService.elencaCaTipoAzienda());
        return "anagraficaaziende/inserimentoAnagraficaAzienda";
    }

    @PostMapping("executeInsert")
    public String executeInsertAnagrafica(@Valid @ModelAttribute("anagrafica") CaAnagraficaAzienda anagrafica,
                                          BindingResult result,
                                          @RequestParam(value = "file", required = false) MultipartFile file,
                                          Model model,
                                          RedirectAttributes redirectAttributes) {


        if (result.hasErrors() || "".equals(file.getOriginalFilename())) {

            if (file.getOriginalFilename() == null || "".equals(file.getOriginalFilename())) {
                model.addAttribute("error", "Documento Camera Commercio obbligatorio");
            }

            model.addAttribute("tipologieAzienda", caTipoAziendaService.elencaCaTipoAzienda());
            LOGGER.warn("Errore Validazione Entità Anagrafica Azienda");
            return "anagraficaaziende/inserimentoAnagraficaAzienda";
        }
        try {
            anagrafica.getCaDocumentoAzienda().setNomeDocumentoAziendaFile(file.getOriginalFilename());
            anagrafica.getCaDocumentoAzienda().setEstensioneDocumentoIdentitaFile(file.getContentType());
            anagrafica.getCaDocumentoAzienda().setDocumentoAziendaFile(file.getBytes());
            anagrafica.setComuneSedeLegale(SildifesaUtility.controlloOggettoVuoto(anagrafica.getComuneSedeLegale()));
            anagrafica.setAttivo(true);
            caAnagraficaAziendaService.inserisciNuovo(anagrafica);
            redirectAttributes.addFlashAttribute("confirm", "Inserimento elemento avvenuta con successo");
        } catch (Exception e) {
            LOGGER.error("Errore in fase di inserimento Anagrafica Azienda", e);
            redirectAttributes.addFlashAttribute("error", "Attenzione, non è stato possibile inserire l'elemento");
        }

        return "redirect:lista";
    }

    @GetMapping("modifica")
    public String modificaAnagrafica(@RequestParam("idAzienda") Long idAzienda, @ModelAttribute("anagrafica") CaAnagraficaAzienda caAnagraficaAzienda, Model model, RedirectAttributes redirectAttributes) {

        CaAnagraficaAzienda anagraficaAzienda = caAnagraficaAziendaService.caricaSingoloElementoCompleto(idAzienda);
        if (anagraficaAzienda == null) {
            redirectAttributes.addFlashAttribute("error", "Attenzione, l' ID selezionato non esistente");
            LOGGER.warn("Id Selezionato non esistente");
            return "redirect:lista";
        }

        model.addAttribute("tipologieAzienda", caTipoAziendaService.elencaCaTipoAzienda()).addAttribute("angraficaAzienda", anagraficaAzienda);
        return "anagraficaaziende/modificaAnagraficaAzienda";

    }

    @PostMapping("eseguiModifica")
    public String eseguiModifica(@Valid @ModelAttribute("anagrafica") CaAnagraficaAzienda caAnagraficaAzienda, BindingResult result, @RequestParam("file") MultipartFile file, Model model, RedirectAttributes redirectAttributes) {

        try {
            if (result.hasErrors() || ("".equals(file.getOriginalFilename()) && caAnagraficaAzienda.getCaDocumentoAzienda() == null)) {
                if ("".equals(file.getOriginalFilename()) && caAnagraficaAzienda.getCaDocumentoAzienda().getId() == null) {
                    model.addAttribute("error", "Documento Camera Commercio obbligatorio");
                }
                model.addAttribute("tipologieAzienda", caTipoAziendaService.elencaCaTipoAzienda())
                        .addAttribute("angraficaAzienda", caAnagraficaAziendaService.caricaSingoloElementoCompleto(caAnagraficaAzienda.getId()));
                LOGGER.warn("Errore Validazione Entità Anagrafica Azienda", result);
                return "anagraficaaziende/modificaAnagraficaAzienda";
            }


            if (!"".equals(file.getOriginalFilename()) || caAnagraficaAzienda.getCaDocumentoAzienda().getId() == null) {
                caAnagraficaAzienda.getCaDocumentoAzienda().setNomeDocumentoAziendaFile(file.getOriginalFilename());
                caAnagraficaAzienda.getCaDocumentoAzienda().setEstensioneDocumentoIdentitaFile(file.getContentType());
                caAnagraficaAzienda.getCaDocumentoAzienda().setDocumentoAziendaFile(file.getBytes());
            } else {
                CaDocumentoAzienda doc = caDocumentoAziendaService.caricaSingoloElemento(caAnagraficaAzienda.getCaDocumentoAzienda().getId());
                caAnagraficaAzienda.setCaDocumentoAzienda(doc);
            }
            CaAnagraficaAzienda anagFromDb = caAnagraficaAziendaService.caricaSingoloElementoCompleto(caAnagraficaAzienda.getId());
            SildifesaUtility.setObjectFromFront(anagFromDb, caAnagraficaAzienda, SildifesaUtility.UPDATE_ANAG_AZIENDA);
            caAnagraficaAziendaService.aggiorna(anagFromDb);
            redirectAttributes.addFlashAttribute("confirm", "Modifica avvenuta con successo");

        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "Attenzione, errore durante la modifica dell'elemento");
            LOGGER.warn("Errore modifica  Anagrafica Azienda", e);
        }

        return "redirect:lista";
    }


    @GetMapping("elimina")
    public String eliminaAnagrafica(@RequestParam("id") Long idAzienda, Model model, RedirectAttributes redirectAttributes) {
        try {
            CaAnagraficaAzienda anagraficaAzienda = caAnagraficaAziendaService.caricaSingoloElementoCompleto(idAzienda);
            if (anagraficaAzienda == null) {
                redirectAttributes.addFlashAttribute("error", "Attenzione, l' ID selezionato non esistente");
                LOGGER.warn("Id Selezionato non esistente");
                return "redirect:lista";
            }
            anagraficaAzienda.setAttivo(false);
            caAnagraficaAziendaService.aggiorna(anagraficaAzienda);
            redirectAttributes.addFlashAttribute("confirm", "Rimozione avvenuta con successo");

        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "Attenzione, errore durante la rimozione dell'elemento");
            LOGGER.warn("Errore modifica  Anagrafica Azienda", e);
        }

        return "redirect:lista";

    }

    @GetMapping("dettaglioAnagraficaAziende")
    public String dettaglioAnagrafica(Model model, @RequestParam("id") Long id) {
        CaAnagraficaAziendaDto anagrafica = CaAnagraficaAziendaDto.fromModelToDto(caAnagraficaAziendaService.caricaSingoloElementoCompleto(id));
        model.addAttribute("tipologieAzienda", caTipoAziendaService.elencaCaTipoAzienda()).addAttribute("anagrafica", anagrafica);
        return "anagraficaaziende/dettaglioAnagraficaAzienda";
    }

}
