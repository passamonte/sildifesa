package it.sildifesa.sildifesa.controller.frontoffice;

import it.sildifesa.sildifesa.models.ut.UtUtente;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("fo/registrazioneVolontario")
public class RegistrazioneAccountVolontarioController {

    private static final Logger LOGGER = LogManager.getLogger(RegistrazioneAccountVolontarioController.class);


    @Autowired
    private JavaMailSender javaMailSender;

    @InitBinder
    public void formattaDate(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @GetMapping("/registra")
    public String regUtente(Model model, @ModelAttribute("utente") UtUtente utUtente) {
        model.addAttribute("utente", utUtente);

        return "fo/registrazioneAccountVolontario";
    }


    @PostMapping("/eseguiRegistrazione")
    public String eseguiRegUtente(Model model, @ModelAttribute("utente") UtUtente utUtente) {


        utUtente.setAbilitato(false);





        try {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(utUtente.getEmail(), "fimaja.9@gmail.com");

            msg.setSubject("Oggetto Prova");
            msg.setText("Messaggio Prova");
            msg.setFrom("a.pass@solvingteam.it");

            MimeMessagePreparator mailMessage = mimeMessage -> {
                MimeMessageHelper message = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");
                message.setSubject("bbbb");
                message.setText("prova", false);

                message.setTo("fimaja.9@gmail.com");
                message.setTo("a.passamonte90@gmail.com");
                message.setFrom("info@sildifesa.it", "SILDifesa");
            };
            javaMailSender.send(mailMessage);
        } catch (Exception e) {
            LOGGER.error("Errore invio e-mail", e);
        }
        return "fo/registrazioneAccountVolontario";
    }


}
