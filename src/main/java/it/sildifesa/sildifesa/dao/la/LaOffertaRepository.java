package it.sildifesa.sildifesa.dao.la;

import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.la.LaOfferta;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface LaOffertaRepository extends CrudRepository<LaOfferta, Long>, QueryByExampleExecutor<LaOfferta>, JpaSpecificationExecutor<LaOfferta> {

    @Query("select la from LaOfferta la left join fetch la.azienda az left join fetch la.localitaSedeOfferta loc left join fetch la.professione pr")
    List<LaOfferta> findAllComplete();

    @Query("select la from LaOfferta la left join fetch la.azienda az left join fetch la.localitaSedeOfferta loc left join fetch la.professione pr where la.id = ?1 ")
    LaOfferta findOneComplete(Long id);


    @Query("select lo from LaOfferta lo" +
            " left join fetch lo.laOffertaTipoContratti" +
            " left join fetch lo.settoreContratto" +
            " left join fetch lo.localitaSedeOfferta" +
            " left join fetch lo.professione" +
            " left join fetch lo.azienda" +
            " where lo.id = ?1")
    LaOfferta caricaSingoloEager(Long id);
    
}
