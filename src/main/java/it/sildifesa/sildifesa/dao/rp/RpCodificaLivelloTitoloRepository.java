package it.sildifesa.sildifesa.dao.rp;

import it.sildifesa.sildifesa.models.rp.RpCodificaLivelloTitolo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface RpCodificaLivelloTitoloRepository extends CrudRepository<RpCodificaLivelloTitolo, Long>, QueryByExampleExecutor<RpCodificaLivelloTitolo> {
}
