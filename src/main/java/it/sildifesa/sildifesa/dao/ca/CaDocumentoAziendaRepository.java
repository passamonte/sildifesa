package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaDocumentoAzienda;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CaDocumentoAziendaRepository extends CrudRepository<CaDocumentoAzienda, Long>, QueryByExampleExecutor<CaDocumentoAzienda> {
}
