package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface CaAnagraficaAziendaRepository  extends JpaRepository<CaAnagraficaAzienda, Long>, CrudRepository<CaAnagraficaAzienda, Long>, QueryByExampleExecutor<CaAnagraficaAzienda>, JpaSpecificationExecutor<CaAnagraficaAzienda> {

    @Query("select ca from CaAnagraficaAzienda ca left join fetch ca.comuneSedeLegale cm left  join ca.tipologiaAzienda tp")
    List<CaAnagraficaAzienda> findAllComplete();

    @Query("select ca from CaAnagraficaAzienda ca left join fetch ca.comuneSedeLegale cm left join fetch ca.tipologiaAzienda tp where ca.id = ?1 ")
    CaAnagraficaAzienda findOneComplete(Long id);

    @EntityGraph(attributePaths = "comuneSedeLegale")
    List<CaAnagraficaAzienda> findAll(Example example);

    List<CaAnagraficaAzienda> findAllByRagioneSocialeContains(String azienda);

}
