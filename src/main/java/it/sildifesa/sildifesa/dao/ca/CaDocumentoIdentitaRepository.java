package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaDocumentoIdentita;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CaDocumentoIdentitaRepository extends CrudRepository<CaDocumentoIdentita, Long>, QueryByExampleExecutor<CaDocumentoIdentita> {
}
