package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaTitoloDiStudio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CaAnagraficaTitoloDiStudioRepository extends CrudRepository<CaAnagraficaTitoloDiStudio, Long>, QueryByExampleExecutor<CaAnagraficaTitoloDiStudio> {

}
