package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaAnagrafica;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CaAnagraficaRepository extends JpaRepository<CaAnagrafica, Long>, QueryByExampleExecutor<CaAnagrafica>, JpaSpecificationExecutor<CaAnagrafica> {
    
    @Query("select ca from CaAnagrafica ca left join fetch ca.suComune1 s1 left join fetch ca.suComune2 s2 left join fetch ca.suComune3 s3" +
            " left join fetch ca.caAnagraficaTitolostudios ats" +
            " left join fetch ats.suTitoloStudio st" +
            " left join fetch ca.suSpecialita sp where ca.id = ?1 ")
    CaAnagrafica caricaSingoloElementoEager(Long input);

}
