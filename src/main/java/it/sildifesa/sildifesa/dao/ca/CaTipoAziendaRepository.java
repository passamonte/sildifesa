package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaTipoAzienda;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CaTipoAziendaRepository extends CrudRepository<CaTipoAzienda, Long>, QueryByExampleExecutor<CaTipoAzienda> {
}
