package it.sildifesa.sildifesa.dao.ca;

import it.sildifesa.sildifesa.models.ca.CaTipoDocumentoIdentita;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CaTipoDocumentoIdentitaRepository extends CrudRepository<CaTipoDocumentoIdentita, Long>, QueryByExampleExecutor<CaTipoDocumentoIdentita> {
}
