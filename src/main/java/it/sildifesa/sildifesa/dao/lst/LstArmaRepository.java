package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstArma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface LstArmaRepository  extends CrudRepository<LstArma, Long>, JpaRepository<LstArma, Long>, JpaSpecificationExecutor<LstArma> {

    @Query("select distinct forzaArmata from LstArma")
    List<String> findDistinctByForzaArmata();

    List<LstArma> findAllByForzaArmata(String forzaArmata);

}
