package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstStatoIter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstStatoIterRepository extends CrudRepository<LstStatoIter, Long>, QueryByExampleExecutor<LstStatoIter> {
}
