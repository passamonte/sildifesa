package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstNazione;
import it.sildifesa.sildifesa.models.lst.LstRuolo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstRuoloRepository extends CrudRepository<LstRuolo, Long>, QueryByExampleExecutor<LstRuolo> {
}
