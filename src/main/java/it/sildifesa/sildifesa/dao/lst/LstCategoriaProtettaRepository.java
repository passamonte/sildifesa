package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstAssociazioneCategoria;
import it.sildifesa.sildifesa.models.lst.LstCategoriaProtetta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstCategoriaProtettaRepository extends CrudRepository<LstCategoriaProtetta, Long>, QueryByExampleExecutor<LstCategoriaProtetta> {
}
