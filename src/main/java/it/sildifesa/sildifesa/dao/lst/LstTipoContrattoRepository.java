package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstTipoContratto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstTipoContrattoRepository extends CrudRepository<LstTipoContratto, Long>, QueryByExampleExecutor<LstTipoContratto> {
}
