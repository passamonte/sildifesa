package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstGrado;
import it.sildifesa.sildifesa.models.lst.LstNaturaGiuridica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstNaturaGiuridicaRepository extends CrudRepository<LstNaturaGiuridica, Long>, QueryByExampleExecutor<LstNaturaGiuridica> {
}
