package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstGrado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface LstGradoRepository extends JpaRepository<LstGrado, Long>, JpaSpecificationExecutor<LstGrado> {

    List<LstGrado> findAllByForzaArmata(String forzaArmata);
}
