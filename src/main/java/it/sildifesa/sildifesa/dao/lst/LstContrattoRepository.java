package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstCategoriaProtetta;
import it.sildifesa.sildifesa.models.lst.LstContratto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstContrattoRepository extends CrudRepository<LstContratto, Long>, QueryByExampleExecutor<LstContratto> {
}
