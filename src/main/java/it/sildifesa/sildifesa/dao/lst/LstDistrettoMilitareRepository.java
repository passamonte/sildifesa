package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstContratto;
import it.sildifesa.sildifesa.models.lst.LstDistrettoMilitare;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstDistrettoMilitareRepository  extends CrudRepository<LstDistrettoMilitare, Long>, QueryByExampleExecutor<LstDistrettoMilitare> {
}