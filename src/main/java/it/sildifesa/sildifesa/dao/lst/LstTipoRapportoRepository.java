package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstTipoRapporto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstTipoRapportoRepository extends CrudRepository<LstTipoRapporto, Long>, QueryByExampleExecutor<LstTipoRapporto> {
}
