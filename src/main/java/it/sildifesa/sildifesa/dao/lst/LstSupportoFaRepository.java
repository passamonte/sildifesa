package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstSupportoFa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstSupportoFaRepository extends CrudRepository<LstSupportoFa, Long>, QueryByExampleExecutor<LstSupportoFa> {
}
