package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstNaturaGiuridica;
import it.sildifesa.sildifesa.models.lst.LstNazione;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstNazioneRepository extends CrudRepository<LstNazione, Long>, QueryByExampleExecutor<LstNazione> {
}
