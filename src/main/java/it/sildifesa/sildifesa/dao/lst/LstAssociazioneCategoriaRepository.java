package it.sildifesa.sildifesa.dao.lst;

import it.sildifesa.sildifesa.models.lst.LstArma;
import it.sildifesa.sildifesa.models.lst.LstAssociazioneCategoria;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface LstAssociazioneCategoriaRepository  extends CrudRepository<LstAssociazioneCategoria, Long>, QueryByExampleExecutor<LstAssociazioneCategoria> {
}
