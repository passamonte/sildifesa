package it.sildifesa.sildifesa.dao.su;

import it.sildifesa.sildifesa.models.su.SuComune;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface SuComuneRepository  extends CrudRepository<SuComune, Long>, QueryByExampleExecutor<SuComune> {

    public SuComune findByNome(String nome);

    public List<SuComune> findAllByNomeContains(String nome);
}
