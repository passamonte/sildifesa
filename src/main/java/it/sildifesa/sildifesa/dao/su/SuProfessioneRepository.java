package it.sildifesa.sildifesa.dao.su;

import it.sildifesa.sildifesa.models.ca.CaAnagraficaAzienda;
import it.sildifesa.sildifesa.models.su.SuProfessione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface SuProfessioneRepository extends CrudRepository<SuProfessione, Long>, JpaRepository<SuProfessione, Long>, QueryByExampleExecutor<SuProfessione> {

//@Query("select p from SuProfessione p left join fetch p.caAnagrafiche ca left join  p.offerte o")
    List<SuProfessione> findAllByNomeContains(String professione);
}
