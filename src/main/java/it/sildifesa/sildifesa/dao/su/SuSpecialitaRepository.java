package it.sildifesa.sildifesa.dao.su;

import it.sildifesa.sildifesa.models.su.SuSpecialita;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface SuSpecialitaRepository extends CrudRepository<SuSpecialita, Long>, QueryByExampleExecutor<SuSpecialita> {

    List<SuSpecialita> findAllByNomeContainsAndForzaArmata(String nome, String forzaArmata);

}
