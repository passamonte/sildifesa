package it.sildifesa.sildifesa.dao.su;

import it.sildifesa.sildifesa.models.su.SuTitoloStudio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface SuTitoloStudioRepository extends CrudRepository<SuTitoloStudio, Long>, QueryByExampleExecutor<SuTitoloStudio> {

    @Query("SELECT t FROM SuTitoloStudio t WHERE t.livello between 30 and 60 ")
    public List<SuTitoloStudio> getTitoloSuLivelloBetween();

    @Query("SELECT t FROM SuTitoloStudio t WHERE t.livello >= 70 ")
    public List<SuTitoloStudio> getTitoloSuLivelloLaurea();

    @Query("SELECT t FROM SuTitoloStudio t WHERE t.livello = ?1 ")
    public List<SuTitoloStudio> getTitoloSuLivelloLower(Integer livelloInput);
}
