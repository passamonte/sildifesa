package it.sildifesa.sildifesa.dao.ut;

import it.sildifesa.sildifesa.models.fo.FoCorso;
import it.sildifesa.sildifesa.models.ut.UtUtente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface UtUtenteRepository extends JpaRepository<UtUtente, Long>, CrudRepository<UtUtente, Long>, JpaSpecificationExecutor<UtUtente>, QueryByExampleExecutor<UtUtente> {
    UtUtente findByUsername(String username);

    @Query("select count(ut) from UtUtente ut where ut.username = ?1")
    Integer countByUsernameEqual(String username);

    @Query("select count(ut) from UtUtente ut where ut.email = ?1")
    Integer countByEmailEqual(String username);

    @Query("select ut from UtUtente ut where ut.email = ?1")
    UtUtente findByEmailEqual(String email);
}
