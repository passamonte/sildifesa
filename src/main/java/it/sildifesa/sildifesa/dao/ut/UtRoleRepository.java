package it.sildifesa.sildifesa.dao.ut;

import it.sildifesa.sildifesa.models.ut.UtRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface UtRoleRepository extends CrudRepository<UtRole, Long>, JpaRepository<UtRole, Long> , JpaSpecificationExecutor<UtRole> {
}
