package it.sildifesa.sildifesa.dao.fo;

import it.sildifesa.sildifesa.models.fo.FoDocumentoCorso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface FoDocumentoCorsoRepository extends CrudRepository<FoDocumentoCorso, Long>, QueryByExampleExecutor<FoDocumentoCorso> {

    @Query("SELECT dc from FoDocumentoCorso dc left join fetch dc.foCorso where dc.id = ?1")
    FoDocumentoCorso caricaSingoloElementoEager(Long input);
}
