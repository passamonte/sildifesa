package it.sildifesa.sildifesa.dao.fo;

import it.sildifesa.sildifesa.models.fo.FoCorso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FoCorsoRepository extends JpaRepository<FoCorso, Long>, CrudRepository<FoCorso, Long>, JpaSpecificationExecutor<FoCorso> {
    @Query("select c from FoCorso c left join fetch c.localitaSedeCorso s1 left join fetch c.caAnagraficaAzienda s2 " +
            " left join fetch c.foCorsoAnagraficaList s4 " +
            " left join fetch c.foDocumentoCorso s3 where c.id = ?1 ")
    FoCorso caricaSingoloElementoEager(Long input);
}
