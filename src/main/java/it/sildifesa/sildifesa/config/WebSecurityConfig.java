package it.sildifesa.sildifesa.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Value("${ldap.urls}")
//    private String ldapUrls;
//    @Value("${ldap.base.dn}")
//    private String ldapBaseDn;
//    @Value("${ldap.username}")
//    private String ldapSecurityPrincipal;
//    @Value("${ldap.password}")
//    private String ldapPrincipalPassword;
//    @Value("${ldap.user.dn.pattern}")
//    private String ldapUserDnPattern;
//    @Value("${ldap.groupSearchBase}")
//    private String groupSearchBase;
//    @Value("${ldap.userSearchBase}")
//    private String userSearchBase;
//
//    @Value("${spring_ldap.authentication.enabled}")
//    private Boolean ldapEnabled;
//
//
//    @Autowired
//    private DataSource dataSource;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/login", "/css/**", "/js/**", "/img/**", "/fonts/**", "/svg/**")
//                .permitAll()
//                //.antMatchers("/**").fullyAuthenticated()
//                .antMatchers("/**")
//                .hasAnyRole("SILDIFESA_sildifesa", "SILDIFESA_guest")
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .defaultSuccessUrl("/home")
//                .failureUrl("/login?error=true")
//                .permitAll()
//                .and()
//                .logout()
//                .logoutSuccessUrl("/login?logout=true")
//                .invalidateHttpSession(true)
//                .permitAll()
//                .and()
//                .csrf()
//                .disable();
//
//    }
//
//    @Override
//    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
//        if (!ldapEnabled) {
//            auth.inMemoryAuthentication()
//                    .withUser("sildifesa").password(passwordEncoder().encode("sildifesa")).roles("SILDIFESA_sildifesa")
//                    .and()
//                    .withUser("guest").password(passwordEncoder().encode("guest")).roles("SILDIFESA_guest");
//        } else {
//            auth
//                    .ldapAuthentication()
//                    .userDnPatterns(ldapUserDnPattern)
//                    .userSearchBase(userSearchBase)
//                    .groupSearchBase(groupSearchBase)
//                    //.rolePrefix("")
//                    .contextSource()
//                    .url(ldapUrls + ldapBaseDn)
//                    .managerDn(ldapSecurityPrincipal)
//                    .managerPassword(ldapPrincipalPassword);
//        }
//
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }


    @Qualifier("utUtenteServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login", "/css/**", "/js/**", "/img/**", "/fonts/**", "/svg/**", "/registraAccount/**", "/confermaRegistrazione/**", "/passwordReset/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?error=true")
                .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/login?logout=true")
                .invalidateHttpSession(true)
                .permitAll()
                .and()
                .csrf()
                .disable();

    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

}