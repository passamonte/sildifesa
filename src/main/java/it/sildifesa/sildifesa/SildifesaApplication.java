package it.sildifesa.sildifesa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SildifesaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SildifesaApplication.class, args);
	}

}
